/* eslint-disable import/no-extraneous-dependencies */
// FIXME: this shouldn't be in the global scope, especially
// since ES6 imports happen before the current script is run,
// so anything else that's imported from this script that
// runs code in the global scope will see global.__basedir as
// undefined.
import 'dd-trace/init';
import cluster from 'cluster';
import express, { Request } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import fs from 'fs';
import helmet from 'helmet';
import compression from 'compression';
import session from 'express-session';
import http from 'http';
import https from 'https';
import limits from 'limits';
import os from 'os';
import sharp from 'sharp';
import { Server, Socket } from 'socket.io';

import 'reflect-metadata';
import { getMetadataArgsStorage, useExpressServer } from 'routing-controllers';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import { validationMetadatasToSchemas } from 'class-validator-jsonschema';

import { createAdapter } from '@socket.io/redis-adapter';
import v2ErrorHandler from './src/middleware/v2ErrorHandler';
import redis from './src/services/redisClient';

import ipInfo from './src/middleware/ip';
import ipBan from './src/middleware/ipBan';
import legacyRoute from './src/routes';
import validOrigins from './config/validOrigins';

import { JWT_SECRET } from './config/server';

import registerThreadHandler from './src/handlers/threadHandler';
import registerConversationHandler from './src/handlers/conversationHandler';
import registerEventLogHandler from './src/handlers/eventLogHandler';
import registerThreadPostHandler from './src/handlers/threadPostHandler';
import registerNotificationHandler from './src/handlers/notificationHandler';
import { authentication } from './src/middleware/auth';
import { initializeQueues, initializeScheduledJobs } from './src/jobs';

import errorHandler from './src/services/errorHandler';
import stripeWebhookMiddleware from './src/middleware/stripeWebhookMiddleware';
import rateLimiterMiddleware from './src/middleware/rateLimit';
import initializeAssociations from './src/models/associations';

const { defaultMetadataStorage } = require('class-transformer/cjs/storage');

// eslint-disable-next-line no-underscore-dangle
global.__basedir = process.cwd();

// eslint-disable-next-line no-underscore-dangle
global.__basedir = process.cwd();

const coreCount = os.cpus().length;
const RedisStore = require('connect-redis')(session);
const morgan = require('morgan');
const eiows = require('eiows');
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const timestamp = process.env.NODE_ENV !== 'development' ? require('log-timestamp') : undefined;

console.log(process.env.MODERATION_WEBHOOK);

let clusterWorkers = Number(process.env.CLUSTER_WORKERS);
// eslint-disable-next-line no-restricted-globals
if (!isFinite(clusterWorkers)) {
  // default value
  clusterWorkers = coreCount;
}

sharp.concurrency(1);

const limitsConfig = {
  enable: true,
  file_uploads: true,
  post_max_size: 8000000,
};

const app = express();

morgan.token('user-id', (req: Request) => req.user?.id);
morgan.token('ip-info', (req: Request) => req.ipInfo);
morgan.token('url', (req: Request) => {
  if (req.query.key) {
    const params = new URLSearchParams(req.originalUrl.split('?')[1]);
    if (params.has('key')) {
      params.set('key', params.get('key')!.split('.').slice(0, 2).join('.'));
    }
    return `${req.originalUrl.split('?')[0]}?${params.toString()}`;
  }
  return req.originalUrl;
});

app.use(
  morgan(
    '[:date[iso]] :ip-info - :user-id ":method :url" :status :res[content-length] :response-time'
  )
);

const helmetOptions = {
  contentSecurityPolicy: {
    useDefaults: true,
  },
  crossOriginOpenerPolicy: {
    policy: 'unsafe-none',
  },
  crossOriginResourcePolicy: {
    policy: process.env.NODE_ENV === 'development' ? 'cross-origin' : 'same-origin',
  },
};

app.use(helmet(helmetOptions));

const sessionOptions = session({
  store: new RedisStore({ client: redis }),
  secret: JWT_SECRET,
  resave: false,
  saveUninitialized: false,
});

app.use(compression());
app.use(sessionOptions);

if (process.env.NODE_ENV !== 'production') {
  validOrigins.push('http://localhost:8080');
  validOrigins.push('http://localhost:8081');
}

const corsOptions = {
  origin: validOrigins,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
  exposedHeaders: 'Content-Range,X-Content-Range,X-Total-Count',
  credentials: true,
};

app.use(cors(corsOptions));

app.use(rateLimiterMiddleware);
app.use(stripeWebhookMiddleware);
app.use(ipInfo);
app.use(ipBan);
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cookieParser());
app.use(limits(limitsConfig));

useExpressServer(app, {
  routePrefix: '/v2',
  controllers: [`${__dirname}/src/v2/*Controller.*`],
  middlewares: [v2ErrorHandler],
  cors: corsOptions,
  defaultErrorHandler: false,
});

app.use('/', legacyRoute);

const schemas = validationMetadatasToSchemas({
  refPointerPrefix: '#/components/schemas/',
  classTransformerMetadataStorage: defaultMetadataStorage,
});

const spec = routingControllersToSpec(
  getMetadataArgsStorage(),
  { routePrefix: '/v2' },
  { components: { schemas }, info: { title: 'Knockout! API', version: '2.0.0' } }
);

app.get('/schema', (req, res) => {
  const output = spec;
  // This path only takes multipart/form-data
  output.paths['/v2/users/{id}/profile/background'].put.requestBody.content['application/json'] =
    undefined;
  // Fix for multiple types
  if (output.components?.schemas) {
    output.components.schemas['Notification']['properties']['data'].$ref = undefined;
    output.components.schemas['Event']['properties']['data'].$ref = undefined;
  }
  return res.json(spec);
});

if (clusterWorkers > 0 && cluster.isMaster) {
  console.log(`Starting ${clusterWorkers} Workers...`);
  // log events
  cluster.on('online', (worker) => {
    console.log(`Worker ${worker.process.pid} is online.`);
  });
  cluster.on('exit', (worker) => {
    console.log(`Worker ${worker.process.pid} died.`);
    cluster.fork();
  });
  // initialize scheduled jobs (we dont want our cluster workers to add the same cron jobs to the queue)
  initializeScheduledJobs();
  // fork processes
  for (let i = 0; i < clusterWorkers; i++) {
    cluster.fork();
  }
} else {
  initializeQueues();

  const setupSockets = (httpServer) => {
    const io: Server = new Server(httpServer, {
      adapter: createAdapter(redis, redis.duplicate()),
      transports: ['websocket'],
      cors: corsOptions,
      wsEngine: eiows.Server,
    });
    io.engine.use(sessionOptions);
    const wrap = (middleware) => (socket, next) => middleware(socket.request, {}, next);
    io.use(wrap(authentication.socket));
    io.on('connection', (socket: Socket) => {
      registerConversationHandler(io, socket);
      registerThreadHandler(io, socket);
      registerEventLogHandler(socket);
      registerThreadPostHandler(socket);
      registerNotificationHandler(socket);
    });
    app.set('io', io);
  };

  initializeAssociations();

  if (process.env.NODE_ENV === 'production') {
    const httpServer = http.createServer(app);
    setupSockets(httpServer);
    httpServer.listen(8002, () => {
      console.log('🚀 HTTP Server running on port 8002');
    });
  }
  if (process.env.NODE_ENV === 'qa') {
    const privateKey = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/privkey.pem`,
      'utf8'
    );
    const certificate = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/cert.pem`,
      'utf8'
    );
    const ca = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/fullchain.pem`,
      'utf8'
    );
    const credentials = {
      key: privateKey,
      cert: certificate,
      ca,
    };
    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(3000, () => {
      console.log('🚀 HTTPS Server running on port 3000');
    });
  }
  if (process.env.NODE_ENV === 'development') {
    const httpServer = http.createServer(app);
    setupSockets(httpServer);

    httpServer.listen(3000, () => {
      console.log('🚀 HTTP Server running on port 3000');
    });
  }
}

errorHandler.catchHandler(app);

export default app;
