import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import PostEdit from '../../src/models/postEdit';

interface CreatePostEditData {
  userId: number;
  postId: number;
  editReason?: string;
  content?: string;
  createdAt?: Date;
}

const data = (props: CreatePostEditData) => {
  const defaultData: CreationAttributes<PostEdit> = {
    userId: props.userId,
    postId: props.postId,
    editReason: faker.lorem.sentence(),
    content: faker.lorem.sentence(),
    createdAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreatePostEditData) => PostEdit.create(data(props));
