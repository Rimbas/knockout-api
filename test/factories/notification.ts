import { NotificationType } from 'knockout-schema';
import { CreationAttributes } from 'sequelize';
import Notification from '../../src/models/notification';

interface CreateNotificationData {
  type?: NotificationType;
  dataId: number;
  userId: number;
}

const data = (props: CreateNotificationData) => {
  const defaultData: CreationAttributes<Notification> = {
    dataId: props.dataId,
    userId: props.userId,
    type: NotificationType.POST_REPLY,
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateNotificationData) => Notification.create(data(props));
