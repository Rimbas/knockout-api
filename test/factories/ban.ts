import Ban from '../../src/models/ban';

interface CreateBanData {
  user_id: number;
  banned_by: number;
  post_id?: number;
  ban_reason?: string;
  expires_at: Date;
  appealThreadCreated?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateBanData) => {
  const defaultData: Required<Omit<CreateBanData, 'post_id'>> &
    Partial<Pick<CreateBanData, 'post_id'>> = {
    user_id: props.user_id,
    banned_by: props.banned_by,
    expires_at: props.expires_at,
    post_id: props.post_id,
    ban_reason: 'Test ban reason',
    appealThreadCreated: props.appealThreadCreated || false,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateBanData) => Ban.create(await data(props));
