import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Mention from '../../src/models/mention';

interface CreateMentionData {
  postId: number;
  mentionsUser: number;
  content?: string;
  inactive?: boolean;
  threadId: number;
  threadTitle?: string;
  page: number;
  mentionedBy: number;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateMentionData) => {
  const defaultData: CreationAttributes<Mention> = {
    postId: props.postId,
    mentionsUser: props.mentionsUser,
    content: faker.lorem.sentence(),
    inactive: false,
    threadId: props.threadId,
    threadTitle: faker.lorem.sentence(),
    page: props.page,
    mentionedBy: props.mentionedBy,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateMentionData) => Mention.create(await data(props));
