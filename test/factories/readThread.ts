import { CreationAttributes } from 'sequelize';
import ReadThread from '../../src/models/readThread';

interface CreateReadThreadData {
  user_id: number;
  thread_id: number;
  lastPostNumber?: number;
  isSubscription?: boolean;
  lastSeen?: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateReadThreadData) => {
  const defaultData: CreationAttributes<ReadThread> = {
    user_id: props.user_id,
    thread_id: props.thread_id,
    lastPostNumber: props.lastPostNumber || 1,
    isSubscription: false,
    lastSeen: new Date(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateReadThreadData) => ReadThread.create(await data(props));
