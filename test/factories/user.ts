import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import User from '../../src/models/user';

type UserAttributes = CreationAttributes<User>;
const data = (props: UserAttributes) => {
  const defaultData: UserAttributes = {
    username: faker.internet.userName(),
    avatar_url: 'none.webp',
    background_url: '',
    disableIncomingMessages: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: UserAttributes = {}) => User.create(data(props));
