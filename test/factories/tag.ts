import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Tag from '../../src/models/tag';

interface CreateTagData {
  name?: string;
  createdAt?: Date;
  deletedAt?: Date;
}

const data = async (props: CreateTagData) => {
  const defaultData: CreationAttributes<Tag> = {
    name: faker.lorem.words(3),
    createdAt: new Date(),
    deletedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateTagData = {}) => Tag.create(await data(props));
