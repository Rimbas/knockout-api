import { lorem } from 'faker';
import { CreationAttributes } from 'sequelize';
import Report from '../../src/models/report';

interface CreateReportData {
  postId: number;
  reportedBy: number;
  reportReason?: string;
  solvedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateReportData) => {
  const defaultData: CreationAttributes<Report> = {
    postId: props.postId,
    reportedBy: props.reportedBy,
    reportReason: lorem.sentence(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateReportData) => Report.create(await data(props));
