import PreviousUserRole from '../../src/models/previousUserRole';

interface CreatePreviousUserRoleData {
  user_id: number;
  role_id: number;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = (props: CreatePreviousUserRoleData) => {
  const defaultData: CreatePreviousUserRoleData = {
    user_id: props.user_id,
    role_id: props.role_id,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreatePreviousUserRoleData) => PreviousUserRole.create(data(props));
