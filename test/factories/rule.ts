import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Rule from '../../src/models/rule';

interface CreateRuleData {
  rulableType?: 'Subforum' | 'Thread';
  rulableId?: number;
  category?: string;
  title?: string;
  cardinality?: number;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateRuleData) => {
  const defaultData: CreationAttributes<Rule> = {
    category: faker.hacker.noun(),
    title: faker.hacker.noun(),
    cardinality: 1,
    description: faker.hacker.noun(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateRuleData = {}) => Rule.create(await data(props));
