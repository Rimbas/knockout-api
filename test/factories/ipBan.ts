import { IP_BAN_CACHE_KEY } from '../../src/middleware/ipBan';
import IpBan from '../../src/models/ipBan';

import redis from '../../src/services/redisClient';

interface CreateIpBanData {
  address?: string;
  range?: string;
  createdBy: number;
  updated_by?: number;
  created_at?: Date;
  updated_at?: Date;
}

const data = async (props: CreateIpBanData) => {
  if (props.address) {
    await redis.del(`${IP_BAN_CACHE_KEY}:${props.address}`);
  }

  const defaultData: Partial<CreateIpBanData> = {
    created_at: new Date(),
    updated_at: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateIpBanData) => IpBan.create(await data(props));
