import { EventType } from 'knockout-schema';
import Event from '../../src/models/event';

interface CreateEventData {
  type?: EventType;
  content?: string;
  dataId: number;
  creator: number;
}

const data = (props: CreateEventData) => {
  const defaultData: Required<CreateEventData> = {
    dataId: props.dataId,
    creator: props.creator,
    type: EventType.THREAD_LOCKED,
    content: '[]',
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateEventData) => Event.create(data(props));
