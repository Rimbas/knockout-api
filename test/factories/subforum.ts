import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Subforum from '../../src/models/subforum';

interface CreateSubforumData {
  name?: string;
  icon?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateSubforumData) => {
  const defaultData: CreationAttributes<Subforum> = {
    name: faker.lorem.words(3),
    icon: faker.internet.url(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateSubforumData = {}) => Subforum.create(await data(props));
