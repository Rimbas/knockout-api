import PostResponse from '../../src/models/postResponse';

interface CreatePostResponseData {
  originalPostId: number;
  responsePostId: number;
  createdAt?: Date;
}

const data = (props: CreatePostResponseData) => {
  const defaultData: CreatePostResponseData = {
    originalPostId: props.originalPostId,
    responsePostId: props.responsePostId,
    createdAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreatePostResponseData) => PostResponse.create(data(props));
