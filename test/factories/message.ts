import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Message from '../../src/models/message';

interface CreateMessageData {
  conversation_id: number;
  user_id: number;
  content?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateMessageData) => {
  const defaultData: CreationAttributes<Message> = {
    conversation_id: props.conversation_id,
    user_id: props.user_id,
    content: faker.lorem.sentence(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateMessageData) => Message.create(await data(props));
