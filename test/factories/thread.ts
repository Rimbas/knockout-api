import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Thread from '../../src/models/thread';

type ThreadCreationInput = Pick<Thread, 'user_id' | 'subforum_id'> & Partial<ThreadAttributes>;
type ThreadAttributes = CreationAttributes<Thread>;
const data = (props: ThreadCreationInput): ThreadAttributes => {
  const defaultData: ThreadAttributes = {
    user_id: props.user_id,
    subforum_id: props.subforum_id,
    title: faker.random.words(5),
    icon_id: 1,
    locked: false,
    pinned: false,
    background_url: '',
    background_type: '',
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: ThreadCreationInput) => Thread.create(data(props));
