import { ThreadSearchRequest } from 'knockout-schema';
import { createRole, createUser, createSubforum, createThread, createPost } from '../factories';
import { addPermissionCodesToRole, apiPost } from '../helper/testHelper';
import User from '../../src/models/user';
import Thread from '../../src/models/thread';

describe('/v2/threadsearch endpoint', () => {
  let role;
  let user: User;
  let subforum;
  let thread: Thread;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      title: 'abcdefgh',
    });
    await createPost({ user_id: user.id, thread_id: thread.id });
  });

  describe('POST /', () => {
    test('search threads', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);

      const data: ThreadSearchRequest = {
        title: thread.title,
        page: 1,
      };

      const expectedThread = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        createdAt: null,
        updatedAt: null,
        deletedAt: null,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: null,
        backgroundUrl: '',
        backgroundType: '',
        user: null,
        postCount: 1,
        recentPostCount: 0,
        subforum: null,
        tags: [],
      };

      const res = await apiPost('/threadsearch', data, user.id);

      expectedThread.createdAt = res.body.threads[0].createdAt;
      expectedThread.updatedAt = res.body.threads[0].updatedAt;
      expectedThread.subforum = res.body.threads[0].subforum;
      expectedThread.lastPost = res.body.threads[0].lastPost;
      expectedThread.user = res.body.threads[0].user;
      expect(res.body.threads).toEqual(
        expect.arrayContaining([expect.objectContaining(expectedThread)])
      );
    });
  });

  test('hides deleted threads', async () => {
    await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);

    const data: ThreadSearchRequest = {
      title: thread.title,
      page: 1,
    };

    thread.deletedAt = new Date();
    await thread.save();
    const res = await apiPost('/threadsearch', data, user.id);

    expect(res.body.threads).toHaveLength(0);
  });

  describe('with locked threads', () => {
    beforeEach(async () => {
      thread.locked = true;
      await thread.save();
    });

    test('hides locked threads by default', async () => {
      const data: ThreadSearchRequest = {
        title: thread.title,
        page: 1,
      };

      const res = await apiPost('/threadsearch', data, user.id);
      expect(res.body.threads).toHaveLength(0);
    });

    test('includes locked threads when locked param is true', async () => {
      const data: ThreadSearchRequest = {
        title: thread.title,
        page: 1,
        locked: true,
      };
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);

      const res = await apiPost('/threadsearch', data, user.id);
      expect(res.body.threads[0].id).toEqual(thread.id);
    });
  });

  test('hides locked threads', async () => {
    thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      title: 'abcdefgh',
      locked: true,
    });
  });
});
