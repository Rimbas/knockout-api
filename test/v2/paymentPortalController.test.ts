import httpStatus from 'http-status';
import { PaymentPortalRequest } from 'knockout-schema';
import { createUser } from '../factories';
import deleteStripeCustomer from '../helper/deleteStripeCustomer';

import { apiPost } from '../helper/testHelper';
import User from '../../src/models/user';

describe('/v2/payment-portal endpoint', () => {
  let user: User;

  beforeEach(async () => {
    user = await createUser();
  });

  // delete any stripe test customers we have created for our test user
  afterEach(async () => {
    await deleteStripeCustomer(user.id);
  });

  describe('POST /', () => {
    const data: PaymentPortalRequest = {
      returnUrl: 'http://localhost:3000/',
    };

    test('rejects logged out users', async () => {
      await apiPost(`/payment-portal`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves payment portal info for the user', async () => {
      const response = await apiPost(`/payment-portal`, data, user.id).expect(httpStatus.CREATED);

      expect(response.body.url).not.toBeNull();
    });
  });
});
