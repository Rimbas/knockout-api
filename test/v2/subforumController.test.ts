import httpStatus from 'http-status';
import { GetSubforumThreadsResponse, Subforum as SubforumSchema } from 'knockout-schema';

import {
  addPermissionCodesToRole,
  apiGet,
  removePermissionCodesFromRole,
} from '../helper/testHelper';

import { getGuestUserRoleId } from '../../src/helpers/role';
import {
  createRole,
  createUser,
  createSubforum,
  createThread,
  createPost,
  createReadThread,
} from '../factories';
import nsfwTagName from '../../src/constants/nsfwTagName';
import redis from '../../src/services/redisClient';
import Thread from '../../src/models/thread';
import Tag from '../../src/models/tag';
import ThreadTag from '../../src/models/threadTag';
import HiddenThread from '../../src/models/hiddenThread';
import User from '../../src/models/user';
import ReadThread from '../../src/models/readThread';
import knex from '../../src/services/knex';

describe('/v2/subforums endpoint', () => {
  let user: User;
  let role;
  let subforum: { id: number; name: string };
  let subforum1: { id: number; name: string };
  let thread: Thread;
  let thread1: Thread;
  let thread2: Thread;
  let thread3: Thread;
  let post: { id: number };
  let post1: { id: number };
  let post2: { id: number };
  let guestRoleId: number;
  let readThread: ReadThread;

  beforeAll(async () => {
    role = await createRole();
    guestRoleId = await getGuestUserRoleId();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread3 = await createThread({ user_id: user.id, subforum_id: subforum.id });
    await createPost({ user_id: user.id, thread_id: thread3.id });
    await new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    post = await createPost({ user_id: user.id, thread_id: thread.id });
    await new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
    thread2 = await createThread({ user_id: user.id, subforum_id: subforum.id });
    await createPost({ user_id: user.id, thread_id: thread2.id });
    readThread = await createReadThread({
      user_id: user.id,
      thread_id: thread2.id,
      lastPostNumber: 1,
    });
    post2 = await createPost({ user_id: user.id, thread_id: thread2.id });
    subforum1 = await createSubforum();
    thread1 = await createThread({ user_id: user.id, subforum_id: subforum1.id });
    post1 = await createPost({ user_id: user.id, thread_id: thread1.id });

    const [tag] = await Tag.findOrCreate({
      attributes: ['id'],
      where: { name: nsfwTagName },
    });

    await ThreadTag.create({ tag_id: tag.id, thread_id: thread2.id });

    await addPermissionCodesToRole(role.id, [
      `subforum-${subforum.id}-view`,
      `subforum-${subforum1.id}-view`,
    ]);

    await addPermissionCodesToRole(guestRoleId, [
      `subforum-${subforum.id}-view`,
      `subforum-${subforum1.id}-view`,
    ]);
  });

  describe('GET /', () => {
    describe('for a non logged-in user', () => {
      test('retrieves all subforums', async () => {
        const { body: response }: { body: SubforumSchema[] } = await apiGet('/subforums').expect(
          httpStatus.OK
        );
        expect(response.length).toBeGreaterThan(1);
        expect(response).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ id: subforum.id }),
            expect.objectContaining({ id: subforum1.id }),
          ])
        );
      });

      test('retrieves viewable subforums', async () => {
        await removePermissionCodesFromRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
        const { body: response }: { body: SubforumSchema[] } = await apiGet('/subforums').expect(
          httpStatus.OK
        );
        expect(response).not.toEqual(
          expect.arrayContaining([expect.objectContaining({ id: subforum1.id })])
        );
        await addPermissionCodesToRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
      });

      test('hides nsfw threads', async () => {
        await addPermissionCodesToRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
        const { body: response }: { body: SubforumSchema[] } = await apiGet(
          '/subforums?hideNsfw=1'
        ).expect(httpStatus.OK);
        expect(response).toEqual(
          expect.arrayContaining([expect.objectContaining({ id: subforum.id })])
        );
        expect(response.find((value) => value.id === subforum.id)?.lastPost?.id).toEqual(post.id);
      });
    });

    describe('for a logged-in user', () => {
      test('retrieves all subforums', async () => {
        const { body: response }: { body: SubforumSchema[] } = await apiGet(
          '/subforums',
          user.id
        ).expect(httpStatus.OK);
        expect(response.length).toBeGreaterThan(1);
        expect(response).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ id: subforum.id }),
            expect.objectContaining({ id: subforum1.id }),
          ])
        );
        expect(response.find((value) => value.id === subforum.id)?.lastPost?.id).toEqual(post2.id);
        expect(response.find((value) => value.id === subforum1.id)?.lastPost?.id).toEqual(post1.id);
      });

      test('retrieves viewable subforums', async () => {
        await removePermissionCodesFromRole(role.id, [`subforum-${subforum1.id}-view`]);
        const { body: response }: { body: SubforumSchema[] } = await apiGet(
          '/subforums',
          user.id
        ).expect(httpStatus.OK);
        expect(response.length).toEqual(1);
        expect(response).toEqual(
          expect.arrayContaining([expect.objectContaining({ id: subforum.id })])
        );
        expect(response.find((value) => value.id === subforum.id)?.lastPost?.id).toEqual(post2.id);
        expect(response.find((value) => value.id === subforum.id)?.totalThreads).toEqual(3);
        await addPermissionCodesToRole(role.id, [`subforum-${subforum1.id}-view`]);
      });

      test('hides nsfw threads', async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum1.id}-view`]);
        const { body: response }: { body: SubforumSchema[] } = await apiGet(
          '/subforums?hideNsfw=1',
          user.id
        ).expect(httpStatus.OK);
        expect(response).toEqual(
          expect.arrayContaining([expect.objectContaining({ id: subforum.id })])
        );
        expect(response.find((value) => value.id === subforum.id)?.lastPost?.id).toEqual(post.id);
      });

      test('hides last posts of threads the user cannot see', async () => {
        const bannedRole = await createRole();
        const bannedUser = await createUser({ roleId: bannedRole.id });
        await addPermissionCodesToRole(bannedRole.id, [
          `subforum-${subforum.id}-view-own-threads`,
          `subforum-${subforum1.id}-view-own-threads`,
        ]);

        const { body: response }: { body: SubforumSchema[] } = await apiGet(
          '/subforums',
          bannedUser.id
        ).expect(httpStatus.OK);

        expect(response.length).toBeGreaterThan(1);
        expect(response).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ id: subforum.id }),
            expect.objectContaining({ id: subforum1.id }),
          ])
        );
        expect(response.find((value) => value.id === subforum.id)?.lastPost).toEqual({});
        expect(response.find((value) => value.id === subforum1.id)?.lastPost).toEqual({});
        expect(response).toHaveLength(2);
      });
    });
  });

  describe('GET /:id/:page?', () => {
    describe('for a non logged-in user', () => {
      test('retrieves a page of threads', async () => {
        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum.id}/1`
        ).expect(httpStatus.OK);
        expect(response.page).toEqual(1);
        expect(response.subforum.id).toEqual(subforum.id);
        expect(response.threads).toHaveLength(3);
        expect(response.threads).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ id: thread.id }),
            expect.objectContaining({ id: thread2.id }),
            expect.objectContaining({ id: thread3.id }),
          ])
        );
      });

      test('blocks users without permisssions', async () => {
        await removePermissionCodesFromRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
        await apiGet(`/subforums/${subforum1.id}/1`).expect(httpStatus.FORBIDDEN);
        await addPermissionCodesToRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
      });

      test('hides total thread count from users without permissions', async () => {
        await removePermissionCodesFromRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
        await addPermissionCodesToRole(guestRoleId, [`subforum-${subforum1.id}-view-own-threads`]);

        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum1.id}/1`
        ).expect(httpStatus.OK);
        expect(response.subforum.totalThreads).toEqual(0);
        expect(response.threads).toHaveLength(0);

        await removePermissionCodesFromRole(guestRoleId, [
          `subforum-${subforum1.id}-view-own-threads`,
        ]);
        await addPermissionCodesToRole(guestRoleId, [`subforum-${subforum1.id}-view`]);
      });

      test('hides nsfw threads', async () => {
        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum.id}/1?hideNsfw=1`
        ).expect(httpStatus.OK);
        expect(response.page).toEqual(1);
        expect(response.subforum.id).toEqual(subforum.id);
        expect(response.threads).toHaveLength(2);
        expect(response.threads[0].id).toEqual(thread.id);
        expect(response.threads[1].id).toEqual(thread3.id);
      });
    });

    describe('for a logged-in user', () => {
      test('retrieves a page of threads', async () => {
        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum.id}/1`,
          user.id
        ).expect(httpStatus.OK);
        expect(response.page).toEqual(1);
        expect(response.subforum.id).toEqual(subforum.id);
        expect(response.threads).toHaveLength(3);
        expect(response.threads[0].id).toEqual(thread2.id);
        expect(response.threads[0].readThread).toBeDefined();
        expect(response.threads[1].id).toEqual(thread.id);
        expect(response.threads[2].id).toEqual(thread3.id);
      });

      test('retrieves a page of threads with a ReadThread missing a last_post_number', async () => {
        await knex('ReadThreads')
          .where({ user_id: readThread.user_id, thread_id: readThread.thread_id })
          .update({ last_post_number: null });
        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum.id}/1`,
          user.id
        ).expect(httpStatus.OK);
        expect(response.page).toEqual(1);
        expect(response.subforum.id).toEqual(subforum.id);
        expect(response.threads).toHaveLength(3);
        expect(response.threads[0].id).toEqual(thread2.id);
        expect(response.threads[0].readThread?.lastPostNumber).toBeDefined();
        expect(response.threads[1].id).toEqual(thread.id);
        expect(response.threads[2].id).toEqual(thread3.id);
        await knex('ReadThreads')
          .where({ user_id: readThread.user_id, thread_id: readThread.thread_id })
          .update({ last_post_number: 1 });
      });

      test('blocks users without permisssions', async () => {
        await removePermissionCodesFromRole(role.id, [`subforum-${subforum1.id}-view`]);
        await apiGet(`/subforums/${subforum1.id}/1`, user.id).expect(httpStatus.FORBIDDEN);
        await addPermissionCodesToRole(role.id, [`subforum-${subforum1.id}-view`]);
      });

      test('hides total thread count from users without permissions', async () => {
        await removePermissionCodesFromRole(role.id, [`subforum-${subforum1.id}-view`]);
        await addPermissionCodesToRole(role.id, [`subforum-${subforum1.id}-view-own-threads`]);

        const user1 = await createUser({ roleId: role.id });

        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum1.id}/1 `,
          user1.id
        ).expect(httpStatus.OK);
        expect(response.subforum.totalThreads).toEqual(0);
        expect(response.threads).toHaveLength(0);

        await removePermissionCodesFromRole(role.id, [`subforum-${subforum1.id}-view-own-threads`]);
        await addPermissionCodesToRole(role.id, [`subforum-${subforum1.id}-view`]);
      });

      test('hides nsfw threads', async () => {
        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum.id}/1?hideNsfw=1`,
          user.id
        ).expect(httpStatus.OK);
        expect(response.page).toEqual(1);
        expect(response.subforum.id).toEqual(subforum.id);
        expect(response.threads).toHaveLength(2);
        expect(response.threads[0].id).toEqual(thread.id);
        expect(response.threads[1].id).toEqual(thread3.id);
      });

      test('hides hidden threads', async () => {
        await HiddenThread.create({ userId: user.id, threadId: thread.id });
        redis.flushall();

        const { body: response }: { body: GetSubforumThreadsResponse } = await apiGet(
          `/subforums/${subforum.id}/1`,
          user.id
        ).expect(httpStatus.OK);
        expect(response.page).toEqual(1);
        expect(response.subforum.id).toEqual(subforum.id);
        expect(response.threads).toHaveLength(2);
        expect(response.threads[0].id).toEqual(thread2.id);
        expect(response.threads[1].id).toEqual(thread3.id);

        await HiddenThread.destroy({ where: { userId: user.id, threadId: thread.id } });
      });
    });
  });
});
