import httpStatus from 'http-status';
import { CreateAlertRequest, AlertsResponse } from 'knockout-schema';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import createUser from '../factories/user';
import { addPermissionCodesToRole, apiDelete, apiGet, apiPost } from '../helper/testHelper';
import nsfwTagName from '../../src/constants/nsfwTagName';
import { createReadThread, createRole } from '../factories';
import ReadThread from '../../src/models/readThread';
import Tag from '../../src/models/tag';
import ThreadTag from '../../src/models/threadTag';
import User from '../../src/models/user';
import Thread from '../../src/models/thread';

describe('/v2/read-threads', () => {
  let user: User;
  let subforum;
  let thread: Thread;

  beforeEach(async () => {
    subforum = await createSubforum();

    const role = await createRole();
    await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
    user = await createUser({ roleId: role.id });
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
  });

  describe('POST /', () => {
    test('rejects logged out users', async () => {
      await apiPost(`/read-threads`, {}).expect(httpStatus.UNAUTHORIZED);
    });

    test('creates a read thread for the user', async () => {
      const request: CreateAlertRequest = {
        threadId: thread.id,
        lastPostNumber: 1,
      };
      await apiPost(`/read-threads`, request, user.id).expect(httpStatus.CREATED);

      await new Promise((resolve) => {
        setTimeout(resolve, 1000);
      });
      const readThread = await ReadThread.findOne({
        attributes: ['thread_id'],
        where: { user_id: user.id },
      });
      expect(readThread).not.toBeNull();
    });

    test('with an existing matching alert, updates the last post number', async () => {
      const request: CreateAlertRequest = {
        threadId: thread.id,
        lastPostNumber: 2,
      };
      const readThread = await createReadThread({
        thread_id: thread.id,
        user_id: user.id,
        lastPostNumber: 1,
        isSubscription: true,
      });

      await apiPost(`/read-threads`, request, user.id).expect(httpStatus.CREATED);
      await readThread.reload();
      expect(readThread.lastPostNumber).toEqual(2);
      expect(readThread.isSubscription).toEqual(true);
    });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/read-threads/1`).expect(httpStatus.UNAUTHORIZED);
    });

    test("retrieves users' read threads", async () => {
      const thread2 = await createThread({
        user_id: user.id,
        subforum_id: subforum.id,
      });

      await ReadThread.create({
        thread_id: thread.id,
        user_id: user.id,
        lastPostNumber: 0,
        isSubscription: false,
      });

      await ReadThread.create({
        thread_id: thread2.id,
        user_id: user.id,
        lastPostNumber: 0,
        isSubscription: false,
      });

      const { body }: { body: AlertsResponse } = await apiGet('/read-threads/1', user.id).expect(
        httpStatus.OK
      );

      expect(body.totalAlerts).toEqual(2);
      expect(body.alerts[0].id).toEqual(thread.id);
      expect(body.alerts[1].id).toEqual(thread2.id);
    });

    test('hides nsfw read threads', async () => {
      const thread2 = await createThread({
        user_id: user.id,
        subforum_id: subforum.id,
      });

      await ReadThread.create({
        thread_id: thread.id,
        user_id: user.id,
        lastPostNumber: 0,
        isSubscription: false,
      });

      await ReadThread.create({
        thread_id: thread2.id,
        user_id: user.id,
        lastPostNumber: 0,
        isSubscription: false,
      });

      const [tag] = await Tag.findOrCreate({
        attributes: ['id'],
        where: { name: nsfwTagName },
      });

      await ThreadTag.create({ tag_id: tag.id, thread_id: thread2.id });
      const { body }: { body: AlertsResponse } = await apiGet(
        '/read-threads/1?hideNsfw=1',
        user.id
      ).expect(httpStatus.OK);

      expect(body.totalAlerts).toEqual(1);
      expect(body.alerts[0].id).toEqual(thread.id);
    });
  });

  describe('DELETE /:id', () => {
    test('rejects logged out users', async () => {
      await apiDelete(`/read-threads/${thread.id}`).expect(httpStatus.UNAUTHORIZED);
    });

    test('deletes a read thread for the user', async () => {
      await ReadThread.create({
        thread_id: thread.id,
        user_id: user.id,
        lastPostNumber: 1,
        isSubscription: false,
      });

      await apiDelete(`/read-threads/${thread.id}`, user.id).expect(httpStatus.OK);

      const result = await ReadThread.findAll({
        attributes: ['thread_id'],
        where: { user_id: user.id },
      });
      expect(result.length).toEqual(0);
    });

    test('deletes an existing alert for the user', async () => {
      await ReadThread.create({
        thread_id: thread.id,
        user_id: user.id,
        lastPostNumber: 1,
        isSubscription: true,
      });

      await apiDelete(`/read-threads/${thread.id}`, user.id).expect(httpStatus.OK);

      const result = await ReadThread.findAll({
        attributes: ['thread_id'],
        where: { user_id: user.id },
      });
      expect(result.length).toEqual(0);
    });
  });
});
