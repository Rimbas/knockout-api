import httpStatus from 'http-status';
import createSubforum from '../factories/subforum';
import createThread from '../factories/thread';
import createUser from '../factories/user';
import { apiDelete, apiGet, apiPost } from '../helper/testHelper';
import HiddenThread from '../../src/models/hiddenThread';
import User from '../../src/models/user';

describe('/v2/hidden-threads', () => {
  let user: User;
  let subforum;
  let thread;

  beforeEach(async () => {
    user = await createUser();
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
  });

  describe('POST /:id', () => {
    test('rejects logged out users', async () => {
      await apiPost(`/hidden-threads/${thread.id}`, {}).expect(httpStatus.UNAUTHORIZED);
    });

    test('creates a hidden thread for the user', async () => {
      await apiPost(`/hidden-threads/${thread.id}`, {}, user.id).expect(httpStatus.CREATED);

      const hiddenThread = (
        await HiddenThread.findAll({
          attributes: ['threadId'],
          where: { userId: user.id },
        })
      ).find((ht) => ht.threadId === thread.id);
      expect(hiddenThread).not.toBeNull();
    });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/hidden-threads`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves users hidden threads', async () => {
      await HiddenThread.create({
        threadId: thread.id,
        userId: user.id,
      });

      const res = await apiGet('/hidden-threads', user.id).expect(httpStatus.OK);

      const hiddenThread = res.body[0];
      expect(hiddenThread.id).toEqual(thread.id);
    });
  });

  describe('DELETE /:id', () => {
    test('rejects logged out users', async () => {
      await apiDelete(`/hidden-threads/${thread.id}`).expect(httpStatus.UNAUTHORIZED);
    });

    test('deletes a hidden thread for the user', async () => {
      await HiddenThread.create({
        threadId: thread.id,
        userId: user.id,
      });

      await apiDelete(`/hidden-threads/${thread.id}`, user.id).expect(httpStatus.OK);

      const ht = await HiddenThread.findAll({
        attributes: ['threadId'],
        where: { userId: user.id },
      });
      expect(ht.length).toEqual(0);
    });
  });
});
