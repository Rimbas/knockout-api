import httpStatus from 'http-status';
import { Alert, CreateAlertRequest } from 'knockout-schema';
import { apiDelete, apiGet, apiPost } from '../helper/testHelper';
import {
  createRole,
  createUser,
  createSubforum,
  createThread,
  createPost,
  createReadThread,
} from '../factories';
import User from '../../src/models/user';
import ReadThread from '../../src/models/readThread';

describe('/v2/alerts endpoint', () => {
  let user: User;
  let role;
  let subforum: { id: number };
  let thread;
  let alert: ReadThread;
  let request: CreateAlertRequest;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    alert = await createReadThread({
      user_id: user.id,
      thread_id: thread.id,
      isSubscription: true,
    });
    request = { threadId: thread.id, lastPostNumber: 2 };
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/alerts/1`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieve alerts for the user', async () => {
      const res = await apiGet(`/alerts/1`, user.id).expect(httpStatus.OK);
      expect(res.body.totalAlerts).toEqual(1);

      const receivedAlert: Alert = res.body.alerts[0];
      expect(receivedAlert.id).toEqual(alert.thread_id);
    });

    test('adds last_post_number to alerts missing the attribute', async () => {
      await createPost({ user_id: user.id, thread_id: thread.id });
      await (alert as any).update({ last_post_number: null });
      await alert.save();
      const res = await apiGet(`/alerts/1`, user.id).expect(httpStatus.OK);
      expect(res.body.totalAlerts).toEqual(1);

      const receivedAlert: Alert = res.body.alerts[0];
      expect(receivedAlert.id).toEqual(alert.thread_id);

      const newAlert = await ReadThread.findOne({
        where: { user_id: user.id, thread_id: thread.id, isSubscription: true },
      });
      expect(newAlert!.lastPostNumber).toEqual(1);
    });

    test('does not retrieve alerts in subforums the user cant see', async () => {
      const otherUser = await createUser();
      await thread.update({ user_id: otherUser.id });
      await thread.save();
      const res = await apiGet(`/alerts/1`, user.id).expect(httpStatus.OK);
      expect(res.body.totalAlerts).toEqual(0);
    });
  });

  describe('POST /', () => {
    test('rejects logged out users', async () => {
      await apiPost('/alerts', request).expect(httpStatus.UNAUTHORIZED);
    });

    test('creates an alert for the user', async () => {
      await apiPost('/alerts', request, user.id).expect(httpStatus.CREATED);
    });

    test('with an existing matching read thread, creates an alert for the user', async () => {
      await alert.update({ isSubscription: false, lastPostNumber: 1 });
      await apiPost('/alerts', request, user.id).expect(httpStatus.CREATED);
      await alert.reload();
      expect(alert.lastPostNumber).toEqual(2);
      expect(alert.isSubscription).toEqual(true);
    });

    test('updates an alert for the user', async () => {
      await apiPost('/alerts', { ...request, lastPostNumber: 4 }, user.id).expect(
        httpStatus.CREATED
      );

      const newAlert = await ReadThread.findOne({
        where: { user_id: user.id, thread_id: thread.id, isSubscription: true },
      });
      expect(newAlert!.lastPostNumber).toEqual(4);
    });
  });

  describe('DELETE /:threadId', () => {
    test('rejects logged out users', async () => {
      await apiDelete(`/alerts/${thread.id}`).expect(httpStatus.UNAUTHORIZED);
    });

    test('deletes the alert', async () => {
      await apiDelete(`/alerts/${thread.id}`, user.id).expect(httpStatus.OK);

      const alerts = await ReadThread.findAll({
        where: { user_id: user.id, isSubscription: true },
      });
      expect(alerts.length).toEqual(0);
    });

    test('with an existing matching read thread, deletes the alert but keeps the read thread', async () => {
      await alert.update({
        lastPostNumber: 2,
        isSubscription: false,
      });
      await apiDelete(`/alerts/${thread.id}`, user.id).expect(httpStatus.OK);
      await alert.reload();
      expect(alert.lastPostNumber).toEqual(2);
      expect(alert.isSubscription).toEqual(false);
    });
  });

  describe('POST /batchDelete', () => {
    test('rejects logged out users', async () => {
      await apiPost('/alerts/batchDelete', { threadIds: [thread.id] }).expect(
        httpStatus.UNAUTHORIZED
      );
    });

    test('deletes the alerts', async () => {
      await apiPost('/alerts/batchDelete', { threadIds: [thread.id] }, user.id).expect(
        httpStatus.OK
      );

      const alerts = await ReadThread.findAll({
        where: { user_id: user.id, isSubscription: true },
      });
      expect(alerts.length).toEqual(0);
    });
  });
});
