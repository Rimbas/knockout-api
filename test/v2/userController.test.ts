import httpStatus from 'http-status';
import {
  UserProfile,
  UpdateUserProfileRequest,
  UserProfileCommentPage,
  UserProfileComment,
  WipeAccountRequest,
  UpdateUserRequest,
  PreviousUsername,
} from 'knockout-schema';
import { addPermissionCodesToRole, apiDelete, apiGet, apiPost, apiPut } from '../helper/testHelper';

import knex from '../../src/services/knex';
import UserRetriever from '../../src/retriever/user';
import {
  getBannedUserRoleId,
  getBasicUserRoleId,
  getLimitedUserRoleId,
} from '../../src/helpers/role';
import {
  createRole,
  createUser,
  createUserProfile,
  createProfileComment,
  createSubforum,
  createThread,
  createPost,
  createRating,
} from '../factories';
import User from '../../src/models/user';
import { DELETED_USER_NAME } from '../../src/constants/deletedUser';

describe('/v2/users endpoint', () => {
  let user: User;
  let userRole;
  let user1: User;
  let userProfile;
  let profileComment;

  beforeEach(async () => {
    userRole = await createRole();
    user = await createUser({ roleId: userRole.id });
    user1 = await createUser({ roleId: await getLimitedUserRoleId() });
    userProfile = await createUserProfile({ user_id: user.id });
    profileComment = await createProfileComment({ user_profile: user.id, author: user1.id });
    await createProfileComment({ user_profile: user.id, author: user1.id });
  });

  describe('GET /:id/profile', () => {
    test('retrieves a user profile', async () => {
      const expectedResponse: UserProfile = {
        id: userProfile.user_id,
        bio: userProfile.heading_text,
        social: {
          website: userProfile.personal_site,
          steam: {
            url: userProfile.steam,
          },
          discord: userProfile.discord,
          github: userProfile.github,
          youtube: userProfile.youtube,
          twitter: userProfile.twitter,
          fediverse: userProfile.fediverse,
          twitch: userProfile.twitch,
          gitlab: userProfile.gitlab,
          tumblr: userProfile.tumblr,
          bluesky: userProfile.bluesky,
        },
        background: {
          url: userProfile.background_url,
          type: userProfile.background_type,
        },
        header: userProfile.header,
        disableComments: false,
      };

      await apiGet(`/users/${user.id}/profile`).expect(httpStatus.OK, expectedResponse);
    });
  });

  describe('PUT /:id/profile', () => {
    test('updates a user profile', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
        useBioForTitle: true,
      };

      await apiPut(`/users/${user.id}/profile`, data, user.id).expect(httpStatus.OK);

      const expectedResponse: UserProfile = {
        id: userProfile.user_id,
        bio: data.bio,
        social: {
          website: data.social.website,
          steam: {
            url: userProfile.steam,
          },
          discord: userProfile.discord,
          github: userProfile.github,
          youtube: userProfile.youtube,
          twitter: data.social.twitter,
          fediverse: userProfile.fediverse,
          twitch: userProfile.twitch,
          gitlab: userProfile.gitlab,
          tumblr: userProfile.tumblr,
          bluesky: userProfile.bluesky,
        },
        background: {
          url: userProfile.background_url,
          type: userProfile.background_type,
        },
        header: userProfile.header,
        disableComments: data.disableComments,
      };

      await apiGet(`/users/${user.id}/profile`).expect(httpStatus.OK, expectedResponse);

      const newUser = await new UserRetriever(userProfile.user_id).getSingleObject();
      expect(newUser.title).toEqual('many things');
    });

    test('rejects a logged out user', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
      };

      await apiPut(`/users/${user.id}/profile`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects a user who is not the profile owner', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
      };

      await apiPut(`/users/${user.id + 1}/profile`, data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('rejects a user who is banned', async () => {
      const bannedUser = await createUser({ roleId: await getBannedUserRoleId() });
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        disableComments: true,
      };

      await apiPut(`/users/${bannedUser.id}/profile`, data, bannedUser.id).expect(
        httpStatus.FORBIDDEN
      );
    });
  });

  describe('GET /:id/profile/comments', () => {
    test("retrieves a user's profile comments", async () => {
      const response: { body: UserProfileCommentPage } = await apiGet(
        `/users/${user.id}/profile/comments`
      ).expect(httpStatus.OK);

      expect(response.body.comments.length).toBe(2);
      expect(response.body.comments[0].author.id).toBe(user1.id);
      expect(response.body.comments[1].author.id).toBe(user1.id);
    });

    test('throws a bad request error if the user id is invalid', async () => {
      await apiGet(`/users/0/profile/comments`).expect(httpStatus.BAD_REQUEST);
    });
  });

  describe('POST /:id/profile/comments', () => {
    test("creates a comment on a user's profile with the correct permission", async () => {
      await addPermissionCodesToRole(userRole.id, ['user-profile-comment-create']);
      const response: { body: UserProfileComment } = await apiPost(
        `/users/${user.id}/profile/comments`,
        { content: 'stuff' },
        user.id
      ).expect(httpStatus.CREATED);
      expect(response.body.content).toBe('stuff');
    });

    test('rejects logged out users', async () => {
      await apiPost(`/users/${user.id}/profile/comments`, {
        content: 'stuff',
      }).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects users without the correct permission', async () => {
      await apiPost(`/users/${user.id}/profile/comments`, { content: 'stuff' }, user.id).expect(
        httpStatus.FORBIDDEN
      );
    });

    test('rejects all comments when profile commenting is disabled', async () => {
      await addPermissionCodesToRole(userRole.id, ['user-profile-comment-create']);
      userProfile.disable_comments = true;
      await userProfile.save();
      await apiPost(`/users/${user.id}/profile/comments`, { content: 'stuff' }, user.id).expect(
        httpStatus.FORBIDDEN
      );
    });
  });

  describe('DELETE /:id/profile/comments/:commentId', () => {
    test("deletes a comment on the user's own profile", async () => {
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, user.id).expect(
        httpStatus.OK
      );
    });

    test('deletes a comment the user created', async () => {
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, user1.id).expect(
        httpStatus.OK
      );
    });

    test('allows user with the correct permission to delete comments', async () => {
      await addPermissionCodesToRole(userRole.id, ['user-profile-comment-archive']);
      const otherUser = await createUser();
      profileComment.update({ user_profile: otherUser.id, author: otherUser.id });
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, user.id).expect(
        httpStatus.OK
      );
    });

    test('rejects users without the correct permission to delete other users comments', async () => {
      const newUser = await createUser();
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`, newUser.id).expect(
        httpStatus.FORBIDDEN
      );
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/users/${user.id}/profile/comments/${profileComment.id}`).expect(
        httpStatus.UNAUTHORIZED
      );
    });
  });

  describe('DELETE /:id/wipe', () => {
    const body: WipeAccountRequest = {
      reason: 'Reason',
    };

    test('rejects users wiping their own account', async () => {
      await apiDelete(`/users/${user.id}/wipe`, user.id, body).expect(httpStatus.FORBIDDEN);
    });

    test('allows users with the correct permission to wipe accounts', async () => {
      await addPermissionCodesToRole(userRole.id, ['user-archive']);
      const subforum = createSubforum();
      const thread = await createThread({
        user_id: user1.id,
        subforum_id: (await subforum).id,
      });
      const post = await createPost({ user_id: user1.id, thread_id: thread.id });
      const post1 = await createPost({ user_id: user1.id, thread_id: thread.id });
      const rating = await createRating({ user_id: user1.id, post_id: post.id });
      const newProfileComment = await createProfileComment({
        user_profile: user.id,
        author: user1.id,
      });
      await createUserProfile({ user_id: user1.id });
      await apiDelete(`/users/${user1.id}/wipe`, user.id, body).expect(httpStatus.OK);
      await new Promise((r) => {
        setTimeout(r, 2000);
      });

      const updatedPost = await knex('Posts').select('content').where({ id: post1.id }).first();
      const updatedRating = await knex('Ratings')
        .select('*')
        .where({ user_id: rating.user_id, post_id: rating.post_id })
        .first();
      const updatedUser = await knex('Users').select('username').where({ id: user1.id }).first();

      const updatedProfileComment = await knex('ProfileComments')
        .select('content')
        .where('id', newProfileComment.id)
        .first();
      const updatedUserProfile = await knex('UserProfiles')
        .select('header')
        .where('user_id', user1.id)
        .first();
      expect(updatedPost.content).toBe("This post's content has been removed by a moderator.");
      expect(updatedRating).toBeFalsy();
      expect(updatedUser.username).toBe(DELETED_USER_NAME);
      expect(updatedProfileComment.content).toBe(
        'This profile comment is from a deleted user and is no longer directly available.'
      );
      expect(updatedUserProfile.header).toBeNull();
    });

    test('rejects attempts to wipe non-limited accounts', async () => {
      const newUser = await createUser({ roleId: await getBasicUserRoleId() });
      await addPermissionCodesToRole(userRole.id, ['user-archive']);
      await apiDelete(`/users/${newUser.id}/wipe`, user.id, body).expect(httpStatus.FORBIDDEN);
    });

    test('rejects users without the required permissions to wipe an account', async () => {
      await apiDelete(`/users/${user1.id}/wipe`, user.id, body).expect(httpStatus.FORBIDDEN);
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/users/${user.id}/wipe`).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('DELETE /:id', () => {
    test('rejects users deleting a different account', async () => {
      await apiDelete(`/users/${user1.id}`, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('allows users with the correct permission to delete their own accounts', async () => {
      const newUser = await createUser({ roleId: userRole.id, createdAt: new Date(2020, 1, 1) });
      const subforum = createSubforum();
      const thread = await createThread({
        user_id: newUser.id,
        subforum_id: (await subforum).id,
      });

      const post1 = await createPost({ user_id: newUser.id, thread_id: thread.id });
      const newProfileComment = await createProfileComment({
        user_profile: user1.id,
        author: newUser.id,
      });
      await createUserProfile({ user_id: newUser.id });
      await apiDelete(`/users/${newUser.id}`, newUser.id).expect(httpStatus.OK);
      await new Promise((r) => {
        setTimeout(r, 2000);
      });

      const updatedPost = await knex('Posts').select('content').where({ id: post1.id }).first();
      const updatedUser = await knex('Users').select('username').where({ id: newUser.id }).first();

      const updatedProfileComment = await knex('ProfileComments')
        .select('content')
        .where('id', newProfileComment.id)
        .first();
      const updatedUserProfile = await knex('UserProfiles')
        .select('header')
        .where('user_id', newUser.id)
        .first();
      expect(updatedPost.content).toBe(post1.content);
      expect(updatedUser.username).toBe(DELETED_USER_NAME);
      expect(updatedProfileComment.content).toBe(
        'This profile comment is from a deleted user and is no longer directly available.'
      );
      expect(updatedUserProfile.header).toBeNull();
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/users/${user.id}`).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('PUT /:id', () => {
    test('updates a username', async () => {
      const data: UpdateUserRequest = {
        username: 'newUsername',
      };

      const response: { body: User } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.username).toBe('newUsername');
    });

    test('rejects a logged out user', async () => {
      const data: UpdateUserRequest = {
        username: 'newUsername',
      };

      await apiPut(`/users/${user.id}/`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects username change if the username was already changed recently', async () => {
      const data: UpdateUserRequest = {
        username: 'newerUsername',
      };

      const response: { body: User } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.username).toBe('newerUsername');

      await apiPut(`/users/${user.id}/`, { username: 'secondUsername' }, user.id).expect(
        httpStatus.FORBIDDEN
      );
    });

    test('rejects invalid usernames', async () => {
      const data: UpdateUserRequest = {
        username: 'ne',
      };

      await apiPut(`/users/${user.id}/`, data, user.id).expect(httpStatus.BAD_REQUEST);

      await apiPut(`/users/${user.id}/`, { username: 'sdf<' }, user.id).expect(
        httpStatus.BAD_REQUEST
      );
    });

    test('rejects username change if the username is already in use', async () => {
      const user2 = await createUser({ username: 'newUsername' });
      const data: UpdateUserRequest = {
        username: user2.username,
      };

      await apiPut(`/users/${user.id}/`, data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('updates pronouns', async () => {
      const data: UpdateUserRequest = {
        pronouns: 'he / him',
      };

      const response: { body: User } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.pronouns).toBe('he / him');
    });

    test('rejects banned users', async () => {
      const bannedUser = await createUser({ roleId: await getBannedUserRoleId() });

      const data: UpdateUserRequest = {
        username: 'newUsername',
        pronouns: 'he / him',
      };

      await apiPut(`/users/${bannedUser.id}/`, data, bannedUser.id).expect(httpStatus.FORBIDDEN);
    });

    test('updates online status toggle', async () => {
      const data: UpdateUserRequest = {
        showOnlineStatus: true,
      };

      const response: { body: any } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.showOnlineStatus).toBe(1);
    });

    test('updates disable incoming messages toggle', async () => {
      const data: UpdateUserRequest = {
        disableIncomingMessages: true,
      };

      const response: { body: any } = await apiPut(`/users/${user.id}/`, data, user.id).expect(
        httpStatus.OK
      );

      expect(response.body.disableIncomingMessages).toBe(1);
    });
  });

  describe('GET /:id/previous-usernames', () => {
    test("retrieves a user's previous usernames", async () => {
      await addPermissionCodesToRole(userRole.id, ['full-user-info-view']);

      const data: UpdateUserRequest = {
        username: 'newUsername1',
      };

      await apiPut(`/users/${user1.id}/`, data, user1.id).expect(httpStatus.OK);

      const response: { body: PreviousUsername[] } = await apiGet(
        `/users/${user1.id}/previous-usernames`,
        user.id
      ).expect(httpStatus.OK);

      expect(response.body).toHaveLength(1);
      expect(response.body[0].username).toEqual(user1.username);
    });

    test('rejects logeed out users', async () => {
      await apiGet(`/users/${user.id}/previous-usernames`).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects users without the required permission', async () => {
      await apiGet(`/users/${user1.id}/previous-usernames`, user.id).expect(httpStatus.FORBIDDEN);
    });
  });
});
