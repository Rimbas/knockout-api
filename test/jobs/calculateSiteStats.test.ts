import {
  createBan,
  createPost,
  createReport,
  createSiteStat,
  createSubforum,
  createThread,
  createUser,
} from '../factories';
import calculateSiteStats from '../../src/jobs/calculateSiteStats';
import SiteStat from '../../src/models/siteStat';

describe('calculateSiteStats', () => {
  beforeEach(async () => {
    await createSiteStat();
  });

  test('a new site stat is created with a starting date of the last stat', async () => {
    const statCountBefore = await SiteStat.count();

    const user = await createUser();
    const onlineDate = new Date();
    onlineDate.setMilliseconds(onlineDate.getMilliseconds() + 100);
    const activeUser = await createUser({ lastOnlineAt: onlineDate });
    const subforum = await createSubforum();
    const thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      createdAt: new Date(),
    });
    const post = await createPost({ thread_id: thread.id, user_id: user.id });
    await createBan({ user_id: user.id, banned_by: activeUser.id, expires_at: new Date() });
    await createReport({ postId: post.id, reportedBy: user.id, solvedBy: user.id });

    await new Promise((r) => {
      setTimeout(r, 500);
    });

    await calculateSiteStats();

    const statCountAfter = await SiteStat.count();

    expect(statCountAfter - statCountBefore).toEqual(1);

    // the below specs are flaky until we find a way for test data to not leak between tests
    // const newSiteStat = await SiteStat.findOne({
    //   attributes: [
    //     'totalActiveUsers',
    //     'totalBans',
    //     'totalNewUsers',
    //     'totalPosts',
    //     'totalReports',
    //     'totalThreads',
    //   ],
    //   order: [['createdAt', 'desc']],
    // });

    // expect(newSiteStat.totalActiveUsers).toEqual(1);
    // expect(newSiteStat.totalBans).toEqual(1);
    // expect(newSiteStat.totalNewUsers).toEqual(2);
    // expect(newSiteStat.totalPosts).toEqual(1);
    // expect(newSiteStat.totalReports).toEqual(1);
    // expect(newSiteStat.totalThreads).toEqual(1);
  });
});
