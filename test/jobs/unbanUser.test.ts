import { getBannedUserRoleId } from '../../src/helpers/role';
import unBanUser from '../../src/jobs/unBanUser';
import UserRetriever from '../../src/retriever/user';
import { createBan, createUser } from '../factories';

describe('unbanUser', () => {
  let bannedUser;
  let banningUser;

  beforeEach(async () => {
    bannedUser = await createUser();
    banningUser = await createUser();
  });

  test('users with no active bans who still have the banned-user role are unbanned', async () => {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    const bannedRoleId = await getBannedUserRoleId();

    await createBan({ user_id: bannedUser.id, banned_by: banningUser.id, expires_at: yesterday });

    await bannedUser.update({ roleId: bannedRoleId });
    await bannedUser.save();
    await new UserRetriever(bannedUser.id).invalidate();

    await unBanUser({ userId: bannedUser.id });

    await bannedUser.reload();
    expect(bannedUser.roleId).not.toEqual(bannedRoleId);
  });

  test('users with unexpired bans who still have the banned-user role are still banned', async () => {
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    await createBan({ user_id: bannedUser.id, banned_by: banningUser.id, expires_at: tomorrow });

    const bannedRoleId = await getBannedUserRoleId();

    await bannedUser.update({ roleId: bannedRoleId });
    await bannedUser.save();
    await new UserRetriever(bannedUser.id).invalidate();

    await unBanUser({ userId: bannedUser.id });

    await bannedUser.reload();
    expect(bannedUser.roleId).toEqual(bannedRoleId);
  });
});
