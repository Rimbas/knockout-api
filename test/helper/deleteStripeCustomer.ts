import User from '../../src/models/user';
import UserRetriever, { UserFlag } from '../../src/retriever/user';
import stripeClient from '../../src/services/stripeClient';

/**
 * Deletes the Stripe Customer associated with a User. Does nothing if the user
 * has no associated Stripe Customer ID
 *
 * @param userId the user we want to delete the Stripe Customer for
 */
export default async (userId: number) => {
  const userRetriever = new UserRetriever(userId, [UserFlag.INCLUDE_STRIPE_CUSTOMER_ID]);
  const user = await userRetriever.getSingleObject();
  const { stripeCustomerId } = user;

  // if the user does not have an associated stripe customer, return
  if (!stripeCustomerId) {
    return;
  }

  try {
    const deletionResponse = await stripeClient.customers.del(stripeCustomerId);

    if (deletionResponse.deleted) {
      // update user and bust user cache

      const userRecord = await User.findOne({ where: { id: userId } });
      await userRecord!.update({ stripeCustomerId });
      await userRecord!.save();

      await userRetriever.invalidate();
    }
  } catch (exception) {
    console.error(`[stripe-api] Error deleting Stripe Customer for user with ID ${userId}`);
    console.error(exception.message);
  }
};
