import { RoleCode } from '../../src/helpers/permissions';
import PreviousUserRole from '../../src/models/previousUserRole';
import Role from '../../src/models/role';
import User from '../../src/models/user';
import { createRole, createUser } from '../factories';

describe('User', () => {
  let user: User;

  beforeEach(async () => {
    user = await createUser();
  });

  test('user without a role is created with limited user role', async () => {
    const role = await Role.findOne({ where: { id: user.roleId } });
    expect(role!.code).toEqual(RoleCode.LIMITED_USER);
  });

  test('users previous role is persisted', async () => {
    const previousUserRoleId = user.roleId;
    const newRole = await createRole({});
    user.roleId = newRole.id;
    await user.save();
    const previousRole = await PreviousUserRole.findOne({ where: { user_id: user.id } });
    expect(previousRole!.role_id).toEqual(previousUserRoleId);
  });
});
