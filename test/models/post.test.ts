import { THREAD_POST_LIMIT } from 'knockout-schema';
import { createPost, createSubforum, createThread, createUser } from '../factories';

describe('Post', () => {
  test('when the post is the first post on a thread', async () => {
    const user = await createUser();
    const subforum = await createSubforum();
    let thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    const post = await createPost({ user_id: user.id, thread_id: thread.id });

    expect(post.thread_post_number).toEqual(1);

    thread = await thread.reload();
    expect(thread.postCount).toEqual(1);
  });

  test('when the post is the second post on a thread', async () => {
    const user = await createUser();
    const subforum = await createSubforum();
    let thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    await createPost({ user_id: user.id, thread_id: thread.id });
    const post = await createPost({ user_id: user.id, thread_id: thread.id });

    expect(post.thread_post_number).toEqual(2);

    thread = await thread.reload();
    expect(thread.postCount).toEqual(2);
  });

  test('when the post is the THREAD_POST_LIMT post on a thread', async () => {
    const user = await createUser();
    const subforum = await createSubforum();
    let thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      postCount: THREAD_POST_LIMIT - 1,
    });

    const post = await createPost({ user_id: user.id, thread_id: thread.id });

    expect(post.thread_post_number).toEqual(THREAD_POST_LIMIT);

    thread = await thread.reload();
    expect(thread.postCount).toEqual(THREAD_POST_LIMIT);
    expect(thread.locked).toEqual(true);
  });
});
