import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import {
  getPostsAndCount,
  store,
  update,
  updateTags,
} from '../../src/policies/threadControllerPolicy';
import { addPermissionCodesToRole } from '../helper/testHelper';
import { getBannedUserRoleId } from '../../src/helpers/role';
import { createSubforum, createRole, createUser, createThread, createBan } from '../factories';
import User from '../../src/models/user';

describe('threadControllerPolicy', () => {
  let subforum: { id: number };
  let thread;
  let role;
  let user: User;
  let mockRequest;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    subforum = await createSubforum();
    role = await createRole();
    user = await createUser({ roleId: role.id });
    const threadUser = await createUser();
    thread = await createThread({ user_id: threadUser.id, subforum_id: subforum.id });
    mockNext = jest.fn();
  });

  describe('getPostsAndCount', () => {
    beforeEach(async () => {
      mockRequest = {
        params: {
          id: thread.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with subforum view permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      await getPostsAndCount(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user viewing their own thread is authorized', async () => {
      const userThread = await createThread({ user_id: user.id, subforum_id: subforum.id });
      mockRequest.params.id = userThread.id;
      await getPostsAndCount(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('is not authorized without the subforum view permission', async () => {
      await expect(getPostsAndCount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('viewing a deleted thread', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
        await thread.update({ deletedAt: new Date() });
      });

      test('is authorized with the subforum view-deleted-threads permission', async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view-deleted-threads`]);
        await getPostsAndCount(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test('is not authorized without the subforum view-deleted-threads permission', async () => {
        await expect(getPostsAndCount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError('Insufficient user permissions.')
        );
      });
    });
  });

  describe('store', () => {
    beforeEach(() => {
      mockRequest = {
        body: {
          subforum_id: subforum.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user with subforum thread-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);
      await store(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without subforum thread-create permission is not authorized', async () => {
      await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('user uploading a background url with the subforum thread-create permission', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);
        mockRequest.body.background_url = 'test';
      });

      test('is authorized with the subforum-thread-background-update permission', async () => {
        await addPermissionCodesToRole(role.id, [
          `subforum-${subforum.id}-thread-background-update`,
        ]);
        await store(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test('is not authorized without the correct permissions', async () => {
        await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError('Insufficient user permissions.')
        );
      });
    });

    describe('when the user is banned', () => {
      let bannedUser: User;
      let bannedUserRoleId;

      beforeAll(async () => {
        bannedUserRoleId = await getBannedUserRoleId();
      });

      beforeEach(async () => {
        bannedUser = await createUser({ roleId: bannedUserRoleId });
        mockRequest.user.id = bannedUser.id;
      });

      test('banned user with subforum thread-create permission has ban appeal set', async () => {
        const banningUser = await createUser();
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        const ban = await createBan({
          user_id: bannedUser.id,
          banned_by: banningUser.id,
          expires_at: tomorrow,
        });
        await addPermissionCodesToRole(bannedUserRoleId, [`subforum-${subforum.id}-thread-create`]);
        await store(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
        await ban.reload();
        expect(ban.appealThreadCreated).toBe(true);
      });

      test('banned user with subforum thread-create permission and ban is appealed', async () => {
        const banningUser = await createUser();
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        await createBan({
          user_id: bannedUser.id,
          banned_by: banningUser.id,
          expires_at: tomorrow,
          appealThreadCreated: true,
        });
        await addPermissionCodesToRole(bannedUserRoleId, [`subforum-${subforum.id}-thread-create`]);
        await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError('You can only make one appeal thread per active mute.')
        );
      });
    });
  });

  describe('update', () => {
    beforeEach(async () => {
      mockRequest = {
        body: {
          id: thread.id,
          title: 'Updated thread title',
        },
        user: {
          id: user.id,
        },
      };
    });

    describe('as a user with the subforum-thread-create permission', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);
      });

      test('updating their own thread is authorized', async () => {
        const userThread = await createThread({ user_id: user.id, subforum_id: subforum.id });
        mockRequest.body.id = userThread.id;
        await update(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test('updating another users thread in the same subforum is not authorized', async () => {
        const otherUser = await createUser();
        const otherThread = await createThread({ user_id: otherUser.id, subforum_id: subforum.id });
        mockRequest.body.id = otherThread.id;

        await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError('Insufficient user permissions.')
        );
      });

      describe('with a background url', () => {
        beforeEach(() => {
          mockRequest.body.background_url = 'test';
        });

        describe('user updating their own thread', () => {
          beforeEach(async () => {
            const userThread = await createThread({ user_id: user.id, subforum_id: subforum.id });
            mockRequest.body.id = userThread.id;
          });

          test('user with thread-background-update permission is authorized', async () => {
            await addPermissionCodesToRole(role.id, [
              `subforum-${subforum.id}-thread-background-update`,
            ]);
            await update(mockRequest, mockResponse, mockNext);
            expect(mockNext).toHaveBeenCalled();
          });

          test('user without thread-background-update permission is not authorized', async () => {
            await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
              new ForbiddenError('Insufficient user permissions.')
            );
          });
        });
      });

      describe('with a new subforum id', () => {
        beforeEach(async () => {
          const userThread = await createThread({ user_id: user.id, subforum_id: subforum.id });
          mockRequest.body.id = userThread.id;
          mockRequest.body.subforum_id = '1';
        });

        test('user with subforum thread-move permission is authorized', async () => {
          await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-move`]);
          await update(mockRequest, mockResponse, mockNext);
          expect(mockNext).toHaveBeenCalled();
        });

        test('user without thread-move permission is not authorized', async () => {
          await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
            new ForbiddenError('Insufficient user permissions.')
          );
        });
      });
    });
  });

  describe('updateTags', () => {
    beforeEach(() => {
      mockRequest = {
        body: {
          threadId: thread.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    beforeEach(async () => {
      const otherUser = await createUser();
      const otherThread = await createThread({ user_id: otherUser.id, subforum_id: subforum.id });
      mockRequest.body.threadId = otherThread.id;
    });

    test('user with subforum thread-update permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-update`]);
      await updateTags(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without subforum thread-update permission is not authorized', async () => {
      await expect(updateTags(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
