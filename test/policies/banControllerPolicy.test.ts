import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { store } from '../../src/policies/banControllerPolicy';
import { createRole, createUser } from '../factories';
import { addPermissionCodesToRole } from '../helper/testHelper';
import User from '../../src/models/user';

describe('banControllerPolicy', () => {
  let mockRequest;
  let user: User;
  let userRole;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    userRole = await createRole();
    user = await createUser({ roleId: userRole.id });
    mockNext = jest.fn();
    mockRequest = {
      user: {
        id: user.id,
      },
    };
  });

  describe('store', () => {
    test('user with ban-create permission is authorized', async () => {
      await addPermissionCodesToRole(userRole.id, ['ban-create']);
      await store(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user without ban-create permission is not authorized', async () => {
      await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
      expect(mockNext).not.toHaveBeenCalled();
    });
  });
});
