import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { deleteAccount, update, wipeAccount } from '../../src/policies/userControllerPolicy';
import { createPreviousUserRole, createRole, createUser } from '../factories';
import { addPermissionCodesToRole } from '../helper/testHelper';
import {
  getBannedUserRoleId,
  getBasicUserRoleId,
  getLimitedUserRoleId,
} from '../../src/helpers/role';
import { banUser } from '../../src/controllers/banController';
import User from '../../src/models/user';

describe('userControllerPolicy', () => {
  let mockRequest;
  let role;
  let user: User;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('update', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is authorized to update themselves', async () => {
      await update(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user is not authorized to update another user', async () => {
      const otherUser = await createUser();
      mockRequest.user.id = otherUser.id;
      await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });
  });

  describe('deleteAccount', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user,
      };
    });

    test('new users are not authorized to delete their own accounts', async () => {
      await expect(deleteAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });

    test('permabanned users are not authorized to delete their own accounts', async () => {
      const newUser = await createUser({ createdAt: new Date(2020, 1, 1) });

      await banUser(-1, newUser.id, 'sdfds', user.id);
      newUser.roleId = await getBannedUserRoleId();
      mockRequest.params.id = newUser.id;
      mockRequest.user = { ...newUser, role_id: newUser.roleId };
      await expect(deleteAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        ForbiddenError
      );
    });

    test('user is authorized to delete their own account', async () => {
      const newUser = await createUser({ roleId: role.id, createdAt: new Date(2020, 1, 1) });
      mockRequest.params.id = newUser.id;
      mockRequest.user = newUser;
      await deleteAccount(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test("user is not authorized to delete another user's account", async () => {
      const otherUser = await createUser();
      mockRequest.params.id = otherUser.id;
      await expect(deleteAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError()
      );
    });
  });

  describe('wipeAccount', () => {
    beforeEach(() => {
      mockRequest = {
        params: {
          id: user.id,
        },
        user: {
          id: user.id,
        },
      };
    });

    test('user is not authorized to wipe their own account', async () => {
      await expect(wipeAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        ForbiddenError
      );
    });

    test("user is not authorized to wipe another user's account without correct permission", async () => {
      const otherUser = await createUser({ roleId: await getLimitedUserRoleId() });
      mockRequest.params.id = otherUser.id;
      await expect(wipeAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('when the current user has the user-archive permission', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, ['user-archive']);
      });

      test("user is authorized to wipe another limited user's account", async () => {
        const otherUser = await createUser({ roleId: await getLimitedUserRoleId() });
        mockRequest.params.id = otherUser.id;
        await wipeAccount(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test("user is authorized to wipe a banned user's account whose previous role is limited", async () => {
        const otherUser = await createUser({ roleId: await getBannedUserRoleId() });
        await createPreviousUserRole({
          user_id: otherUser.id,
          role_id: await getLimitedUserRoleId(),
        });
        mockRequest.params.id = otherUser.id;
        await wipeAccount(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });

      test("user is not authorized to wipe a non-limited user's account with user-archive permission", async () => {
        const otherUser = await createUser({ roleId: await getBasicUserRoleId() });
        mockRequest.params.id = otherUser.id;
        await expect(wipeAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          ForbiddenError
        );
      });

      test("user is not authorized to wipe a banned user's account whose previous role is not limited", async () => {
        const otherUser = await createUser({ roleId: await getBannedUserRoleId() });
        await createPreviousUserRole({
          user_id: otherUser.id,
          role_id: await getBasicUserRoleId(),
        });
        mockRequest.params.id = otherUser.id;
        await expect(wipeAccount(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          ForbiddenError
        );
      });
    });
  });
});
