import User from '../../src/models/user';
import { validateThreadStatus } from '../../src/validations/post';
import { createSubforum, createUser, createThread } from '../factories';

describe('Test thread status validation', () => {
  let subforum;
  let user: User;

  beforeEach(async () => {
    subforum = await createSubforum();
    user = await createUser();
  });

  test('validation passes for a valid thread', async () => {
    const thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    const threadStatus = await validateThreadStatus(thread.id);
    expect(threadStatus).toBe(true);
  });

  test('validation fails for a locked thread', async () => {
    const thread = await createThread({ user_id: user.id, subforum_id: subforum.id, locked: true });
    const threadStatus = await validateThreadStatus(thread.id);
    expect(threadStatus).toBe(false);
  });

  test('validation fails for a deleted thread', async () => {
    const thread = await createThread({
      user_id: user.id,
      subforum_id: subforum.id,
      deletedAt: new Date(),
    });
    const threadStatus = await validateThreadStatus(thread.id);
    expect(threadStatus).toBe(false);
  });
});
