import initializeAssociations from '../src/models/associations';

export default async () => {
  try {
    initializeAssociations();
  } catch (error) {
    console.log(error);
    throw error;
  }
};
