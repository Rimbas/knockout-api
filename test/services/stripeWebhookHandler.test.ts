import { v4 as uuidv4 } from 'uuid';
import { STRIPE_GOLD_MEMBER_PRICE_ID } from '../../config/server';
import {
  getBannedUserRoleId,
  getBasicUserRoleId,
  getPaidGoldUserRoleId,
} from '../../src/helpers/role';
import StripeWebhookHandler from '../../src/services/stripeWebhookHandler';
import * as stripeHelper from '../../src/helpers/stripe';
import knex from '../../src/services/knex';
import { createUser } from '../factories';
import PreviousUserRole from '../../src/models/previousUserRole';
import User from '../../src/models/user';

describe('stripeWebhookHandler', () => {
  let user: User;
  let basicUserRoleId: number;

  beforeAll(async () => {
    basicUserRoleId = await getBasicUserRoleId();
  });

  beforeEach(async () => {
    user = await createUser({ roleId: basicUserRoleId, stripeCustomerId: uuidv4() });
  });

  describe('handleWebhookEvent', () => {
    describe('handles invoice.payment_succeeded events', () => {
      let spy;

      beforeEach(async () => {
        spy = jest.spyOn(stripeHelper, 'getWebhookEvent').mockImplementation(() =>
          Promise.resolve({
            id: 'evt_1LmZLyCfV5w4tJXMBWwUf0hW',
            object: 'event',
            api_version: '2020-08-27',
            created: 1664268082,
            data: {
              object: {
                id: 'in_1LlTtJCfV5w4tJXMXdDyxTLa',
                object: 'invoice',
                account_country: 'GB',
                account_name: 'knockout.chat',
                account_tax_ids: null,
                amount_due: 350,
                amount_paid: 350,
                amount_remaining: 0,
                application: null,
                application_fee_amount: null,
                attempt_count: 1,
                attempted: true,
                auto_advance: false,
                automatic_tax: {
                  enabled: false,
                  status: null,
                },
                billing_reason: 'subscription_cycle',
                charge: 'ch_3LmZLuCfV5w4tJXM1s7rhXsQ',
                collection_method: 'charge_automatically',
                created: 1664008757,
                currency: 'gbp',
                custom_fields: null,
                customer: user.stripeCustomerId,
                customer_address: null,
                customer_email: 'test@test.com',
                customer_name: null,
                customer_phone: null,
                customer_shipping: null,
                customer_tax_exempt: 'none',
                customer_tax_ids: [],
                default_payment_method: null,
                default_source: null,
                default_tax_rates: [],
                description: null,
                discount: null,
                discounts: [],
                due_date: null,
                ending_balance: 0,
                footer: null,
                from_invoice: null,
                hosted_invoice_url: 'url',
                invoice_pdf: 'url',
                last_finalization_error: null,
                latest_revision: null,
                lines: {
                  object: 'list',
                  data: [
                    {
                      id: 'il_1LlTtJCfV5w4tJXMtmmtG2dI',
                      object: 'line_item',
                      amount: 350,
                      amount_excluding_tax: 350,
                      currency: 'gbp',
                      description: '1 × Gold Membership (at £3.50 / month)',
                      discount_amounts: [],
                      discountable: true,
                      discounts: [],
                      livemode: true,
                      metadata: {},
                      period: {
                        end: 1666600715,
                        start: 1664008715,
                      },
                      plan: {
                        id: 'test_price',
                        object: 'plan',
                        active: true,
                        aggregate_usage: null,
                        amount: 350,
                        amount_decimal: '350',
                        billing_scheme: 'per_unit',
                        created: 1610990626,
                        currency: 'gbp',
                        interval: 'month',
                        interval_count: 1,
                        livemode: true,
                        metadata: {},
                        nickname: null,
                        product: 'test_prod',
                        tiers_mode: null,
                        transform_usage: null,
                        trial_period_days: null,
                        usage_type: 'licensed',
                      },
                      price: {
                        id: STRIPE_GOLD_MEMBER_PRICE_ID,
                        object: 'price',
                        active: true,
                        billing_scheme: 'per_unit',
                        created: 1610990626,
                        currency: 'gbp',
                        custom_unit_amount: null,
                        livemode: true,
                        lookup_key: null,
                        metadata: {},
                        nickname: null,
                        product: 'test_prod',
                        recurring: {
                          aggregate_usage: null,
                          interval: 'month',
                          interval_count: 1,
                          trial_period_days: null,
                          usage_type: 'licensed',
                        },
                        tax_behavior: 'unspecified',
                        tiers_mode: null,
                        transform_quantity: null,
                        type: 'recurring',
                        unit_amount: 350,
                        unit_amount_decimal: '350',
                      },
                      proration: false,
                      proration_details: {
                        credited_items: null,
                      },
                      quantity: 1,
                      subscription: 'test_sub',
                      subscription_item: 'si_MIqoAnrUX69lhU',
                      tax_amounts: [],
                      tax_rates: [],
                      type: 'subscription',
                      unit_amount_excluding_tax: '350',
                    },
                  ],
                  has_more: false,
                  total_count: 1,
                  url: '/v1/invoices/in_1LlTtJCfV5w4tJXMXdDyxTLa/lines',
                },
                livemode: true,
                metadata: {},
                next_payment_attempt: null,
                number: 'CDDD531E-0323',
                on_behalf_of: null,
                paid: true,
                paid_out_of_band: false,
                payment_intent: 'pi_3LmZLuCfV5w4tJXM1BOUf6A1',
                payment_settings: {
                  default_mandate: null,
                  payment_method_options: null,
                  payment_method_types: null,
                },
                period_end: 1664008715,
                period_start: 1661330315,
                post_payment_credit_notes_amount: 0,
                pre_payment_credit_notes_amount: 0,
                quote: null,
                receipt_number: null,
                rendering_options: null,
                starting_balance: 0,
                statement_descriptor: null,
                status: 'paid',
                status_transitions: {
                  finalized_at: 1664268078,
                  marked_uncollectible_at: null,
                  paid_at: 1664268078,
                  voided_at: null,
                },
                subscription: 'sub_1LaF6dCfV5w4tJXM2LPlMrp5',
                subtotal: 350,
                subtotal_excluding_tax: 350,
                tax: null,
                test_clock: null,
                total: 350,
                total_discount_amounts: [],
                total_excluding_tax: 350,
                total_tax_amounts: [],
                transfer_data: null,
                webhooks_delivered_at: null,
              },
            },
            livemode: true,
            pending_webhooks: 1,
            request: {
              id: null,
              idempotency_key: null,
            },
            type: 'invoice.payment_succeeded',
          })
        );
      });

      it('promotes the user to gold', async () => {
        expect(user.roleId).toEqual(basicUserRoleId);
        await new StripeWebhookHandler().handleWebhookEvent('test', 'test', undefined);

        const paidGoldUserRoleId = await getPaidGoldUserRoleId();
        await user.reload();
        expect(user.roleId).toEqual(paidGoldUserRoleId);
        spy.mockRestore();
      });

      describe('when the user is banned', () => {
        let bannedUserRoleId;

        beforeEach(async () => {
          bannedUserRoleId = await getBannedUserRoleId();
          await knex('Users').update({ role_id: bannedUserRoleId }).where({ id: user.id });
          await user.reload();
        });

        it('does not unban the user and makes their previous role a gold user', async () => {
          expect(user.roleId).toEqual(bannedUserRoleId);
          await new StripeWebhookHandler().handleWebhookEvent('test', 'test', undefined);
          await user.reload();
          expect(user.roleId).toEqual(bannedUserRoleId);

          const userPreviousRole = await PreviousUserRole.findOne({
            attributes: ['role_id'],
            where: { user_id: user.id },
            order: [['created_at', 'desc']],
          });
          const paidGoldUserRoleId = await getPaidGoldUserRoleId();

          expect(userPreviousRole!.role_id).toEqual(paidGoldUserRoleId);
        });
      });
    });

    describe('handles checkout.session.completed (donation) events', () => {
      let spy;

      beforeEach(async () => {
        spy = jest.spyOn(stripeHelper, 'getWebhookEvent').mockImplementation(() =>
          Promise.resolve({
            id: 'evt_1LlTgGCfV5w4tJXMoWaAC6MM',
            object: 'event',
            api_version: '2020-08-27',
            created: 1664007948,
            data: {
              object: {
                id: 'cs_live_a1wwKftwIxSQqewW1p6sY5GcxYWbKEdC7j6CdMD0XDTIUWUXfNFsyTlbml',
                object: 'checkout.session',
                after_expiration: null,
                allow_promotion_codes: null,
                amount_subtotal: 350,
                amount_total: 350,
                automatic_tax: {
                  enabled: false,
                  status: null,
                },
                billing_address_collection: null,
                cancel_url: 'https://knockout.chat/usersettings?knockoutGoldPurchaseSuccess=0',
                client_reference_id: null,
                consent: null,
                consent_collection: null,
                currency: 'gbp',
                customer: user.stripeCustomerId,
                customer_creation: null,
                customer_details: {
                  address: {
                    city: null,
                    country: 'PL',
                    line1: null,
                    line2: null,
                    postal_code: null,
                    state: null,
                  },
                  email: 'test@test.com',
                  name: 'Test Test',
                  phone: null,
                  tax_exempt: 'none',
                  tax_ids: [],
                },
                customer_email: null,
                expires_at: 1664094195,
                livemode: true,
                locale: null,
                metadata: {
                  name: 'Knockout Gold checkout session for User 2655',
                  type: 'donation',
                },
                mode: 'donation',
                payment_intent: null,
                payment_link: null,
                payment_method_collection: 'always',
                payment_method_options: null,
                payment_method_types: ['card'],
                payment_status: 'paid',
                phone_number_collection: {
                  enabled: false,
                },
                recovered_from: null,
                setup_intent: null,
                shipping: null,
                shipping_address_collection: null,
                shipping_options: [],
                shipping_rate: null,
                status: 'complete',
                submit_type: null,
                subscription: 'sub_1LlTfoCfV5w4tJXMWx8FMG7I',
                success_url: 'https://knockout.chat/usersettings?knockoutGoldPurchaseSuccess=1',
                total_details: {
                  amount_discount: 0,
                  amount_shipping: 0,
                  amount_tax: 0,
                },
                url: null,
              },
            },
            livemode: true,
            pending_webhooks: 1,
            request: {
              id: null,
              idempotency_key: null,
            },
            type: 'checkout.session.completed',
          })
        );
      });

      it('promotes the user to gold', async () => {
        expect(user.roleId).toEqual(basicUserRoleId);
        await new StripeWebhookHandler().handleWebhookEvent('test', 'test', undefined);

        const paidGoldUserRoleId = await getPaidGoldUserRoleId();
        await user.reload();
        expect(user.roleId).toEqual(paidGoldUserRoleId);
        spy.mockRestore();
      });

      describe('when the user is banned', () => {
        let bannedUserRoleId;
        let paidGoldUserRoleId;

        beforeEach(async () => {
          bannedUserRoleId = await getBannedUserRoleId();
          paidGoldUserRoleId = await getPaidGoldUserRoleId();
          await knex('Users').update({ role_id: bannedUserRoleId }).where({ id: user.id });
          await user.reload();
        });

        it('does not unban the user and makes their previous role a gold user', async () => {
          expect(user.roleId).toEqual(bannedUserRoleId);
          await new StripeWebhookHandler().handleWebhookEvent('test', 'test', undefined);
          await user.reload();
          expect(user.roleId).toEqual(bannedUserRoleId);

          const userPreviousRole = await PreviousUserRole.findOne({
            attributes: ['role_id'],
            where: { user_id: user.id },
            order: [['created_at', 'desc']],
          });

          console.log(
            await PreviousUserRole.findAll({
              where: { user_id: user.id },
              order: [['created_at', 'desc']],
            }),
            userPreviousRole,
            paidGoldUserRoleId
          );

          expect(userPreviousRole!.role_id).toEqual(paidGoldUserRoleId);
        });
      });
    });

    describe('handles customer.subscription.deleted events', () => {
      let paidGoldUserId;
      let spy;

      beforeEach(async () => {
        paidGoldUserId = await getPaidGoldUserRoleId();
        await user.update({ roleId: paidGoldUserId });
        await user.reload();
        spy = jest.spyOn(stripeHelper, 'getWebhookEvent').mockImplementation(() =>
          Promise.resolve({
            id: 'evt_1LmPNWCfV5w4tJXMVQ53gkNA',
            object: 'event',
            api_version: '2020-08-27',
            created: 1664229738,
            data: {
              object: {
                id: 'sub_1LF3VsCfV5w4tJXMIwJxhHS1',
                object: 'subscription',
                application: null,
                application_fee_percent: null,
                automatic_tax: {
                  enabled: false,
                },
                billing_cycle_anchor: 1656280864,
                billing_thresholds: null,
                cancel_at: null,
                cancel_at_period_end: false,
                canceled_at: null,
                collection_method: 'charge_automatically',
                created: 1656280864,
                currency: 'gbp',
                current_period_end: 1666821664,
                current_period_start: 1664229664,
                customer: user.stripeCustomerId,
                days_until_due: null,
                default_payment_method: 'pm_1LF3VrCfV5w4tJXMSgdPwViO',
                default_source: null,
                default_tax_rates: [],
                description: null,
                discount: null,
                ended_at: null,
                items: {
                  object: 'list',
                  data: [
                    {
                      id: 'si_LwxQIn2E1Hk8J8',
                      object: 'subscription_item',
                      billing_thresholds: null,
                      created: 1656280864,
                      metadata: {},
                      plan: {
                        id: 'price_1IB1SACfV5w4tJXMIkPtLUQv',
                        object: 'plan',
                        active: true,
                        aggregate_usage: null,
                        amount: 350,
                        amount_decimal: '350',
                        billing_scheme: 'per_unit',
                        created: 1610990626,
                        currency: 'gbp',
                        interval: 'month',
                        interval_count: 1,
                        livemode: true,
                        metadata: {},
                        nickname: null,
                        product: 'prod_ImadaxFQ9cnG5t',
                        tiers_mode: null,
                        transform_usage: null,
                        trial_period_days: null,
                        usage_type: 'licensed',
                      },
                      price: {
                        id: 'price_1IB1SACfV5w4tJXMIkPtLUQv',
                        object: 'price',
                        active: true,
                        billing_scheme: 'per_unit',
                        created: 1610990626,
                        currency: 'gbp',
                        custom_unit_amount: null,
                        livemode: true,
                        lookup_key: null,
                        metadata: {},
                        nickname: null,
                        product: 'prod_ImadaxFQ9cnG5t',
                        recurring: {
                          aggregate_usage: null,
                          interval: 'month',
                          interval_count: 1,
                          trial_period_days: null,
                          usage_type: 'licensed',
                        },
                        tax_behavior: 'unspecified',
                        tiers_mode: null,
                        transform_quantity: null,
                        type: 'recurring',
                        unit_amount: 350,
                        unit_amount_decimal: '350',
                      },
                      quantity: 1,
                      subscription: 'sub_1LF3VsCfV5w4tJXMIwJxhHS1',
                      tax_rates: [],
                    },
                  ],
                  has_more: false,
                  total_count: 1,
                  url: '/v1/subscription_items?subscription=sub_1LF3VsCfV5w4tJXMIwJxhHS1',
                },
                latest_invoice: 'in_1LmPNWCfV5w4tJXMclDEiBi9',
                livemode: true,
                metadata: {},
                next_pending_invoice_item_invoice: null,
                pause_collection: null,
                payment_settings: {
                  payment_method_options: null,
                  payment_method_types: null,
                  save_default_payment_method: 'off',
                },
                pending_invoice_item_interval: null,
                pending_setup_intent: null,
                pending_update: null,
                plan: {
                  id: 'price_1IB1SACfV5w4tJXMIkPtLUQv',
                  object: 'plan',
                  active: true,
                  aggregate_usage: null,
                  amount: 350,
                  amount_decimal: '350',
                  billing_scheme: 'per_unit',
                  created: 1610990626,
                  currency: 'gbp',
                  interval: 'month',
                  interval_count: 1,
                  livemode: true,
                  metadata: {},
                  nickname: null,
                  product: 'prod_ImadaxFQ9cnG5t',
                  tiers_mode: null,
                  transform_usage: null,
                  trial_period_days: null,
                  usage_type: 'licensed',
                },
                quantity: 1,
                schedule: null,
                start_date: 1656280864,
                status: 'active',
                test_clock: null,
                transfer_data: null,
                trial_end: null,
                trial_start: null,
              },
              previous_attributes: {
                current_period_end: 1664229664,
                current_period_start: 1661551264,
                latest_invoice: 'in_1LbAbRCfV5w4tJXMnHaySH11',
              },
            },
            livemode: true,
            pending_webhooks: 1,
            request: {
              id: null,
              idempotency_key: null,
            },
            type: 'customer.subscription.deleted',
          })
        );
      });

      afterEach(() => {
        spy.mockRestore();
      });

      it('demotes the user from gold', async () => {
        expect(user.roleId).toEqual(paidGoldUserId);
        await new StripeWebhookHandler().handleWebhookEvent('test', 'test', undefined);
        await user.reload();
        expect(user.roleId).not.toEqual(paidGoldUserId);
      });

      describe('when the user is banned', () => {
        let bannedUserRoleId;

        beforeEach(async () => {
          bannedUserRoleId = await getBannedUserRoleId();
          await user.update({ roleId: bannedUserRoleId });
          await user.reload();
        });

        it('does not unban / demote the user from gold and makes their previous role a basic user', async () => {
          expect(user.roleId).toEqual(bannedUserRoleId);
          await new Promise((resolve) => {
            setTimeout(resolve, 1000);
          });
          await new StripeWebhookHandler().handleWebhookEvent('test', 'test', undefined);
          await user.reload();
          expect(user.roleId).toEqual(bannedUserRoleId);

          const userPreviousRole = await PreviousUserRole.findOne({
            attributes: ['role_id'],
            where: { user_id: user.id },
            order: [['created_at', 'desc']],
          });

          expect(userPreviousRole!.role_id).toEqual(basicUserRoleId);
        });
      });
    });
  });
});
