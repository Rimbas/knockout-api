import scrubUser from '../../../src/services/userDeletion/scrubUser';
import { RoleCode } from '../../../src/helpers/permissions';
import { createUser } from '../../factories';
import User from '../../../src/models/user';
import Role from '../../../src/models/role';
import initializeAssociations from '../../../src/models/associations';
import { DELETED_USER_NAME } from '../../../src/constants/deletedUser';

describe('scrubUser', () => {
  let user: User;

  beforeAll(async () => {
    initializeAssociations();
  });

  beforeEach(async () => {
    user = await createUser();
  });

  test('the users info is scrubbed', async () => {
    await scrubUser(user.id);

    const scrubbedUser = (await User.findOne({
      where: { id: user.id },
      include: {
        model: Role,
        attributes: ['code'],
      },
    }))!;
    expect(scrubbedUser.username).toEqual(DELETED_USER_NAME);
    expect(scrubbedUser.avatar_url).toEqual('');
    expect(scrubbedUser.background_url).toEqual('');
    expect(scrubbedUser.title).toBeNull();
    expect((scrubbedUser as any).Role.code).toEqual(RoleCode.BANNED_USER);
  });
});
