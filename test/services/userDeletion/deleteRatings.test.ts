import Rating from '../../../src/models/rating';
import User from '../../../src/models/user';
import deleteRatings from '../../../src/services/userDeletion/deleteRatings';
import {
  createUser,
  createSubforum,
  createThread,
  createPost,
  createRating,
} from '../../factories';

describe('deleteRatings', () => {
  let user: User;

  beforeEach(async () => {
    user = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    const post = await createPost({ user_id: user.id, thread_id: thread.id });
    await createRating({ user_id: user.id, post_id: post.id });
  });

  test('the users ratings are deleted', async () => {
    const ratingsCountBefore = (
      await Rating.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(ratingsCountBefore).toEqual(1);

    await deleteRatings(user.id);

    const ratingsCountAfter = (
      await Rating.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(ratingsCountAfter).toEqual(0);
  });
});
