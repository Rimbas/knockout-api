import Post from '../../../src/models/post';
import PostResponse from '../../../src/models/postResponse';
import User from '../../../src/models/user';
import { wipePosts } from '../../../src/services/userDeletion/scrubPosts';
import {
  createUser,
  createSubforum,
  createThread,
  createPost,
  createPostResponse,
} from '../../factories';

describe('wipePosts', () => {
  let user: User;
  let post;

  beforeEach(async () => {
    user = await createUser();
    const userTwo = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    post = await createPost({ thread_id: thread.id, user_id: user.id });
    const postTwo = await createPost({ thread_id: thread.id, user_id: userTwo.id });
    await createPostResponse({
      originalPostId: postTwo.id,
      responsePostId: post.id,
    });
  });

  test('the users posts are scrubbed and post responses are deleted', async () => {
    const postResponsesCountBefore = (
      await PostResponse.findAll({
        where: { responsePostId: post.id },
      })
    ).length;

    expect(postResponsesCountBefore).toEqual(1);

    await wipePosts(user.id);

    const postResponsesCountAfter = (
      await PostResponse.findAll({
        where: { responsePostId: post.id },
      })
    ).length;

    expect(postResponsesCountAfter).toEqual(0);

    const newPost = await Post.findOne({ where: { id: post.id } });
    expect(newPost!.content).toEqual("This post's content has been removed by a moderator.");
  });
});
