import { Polly, PollyConfig } from '@pollyjs/core';
import { Context, setupPolly } from 'setup-polly-jest';
import NodeHttpAdapter from '@pollyjs/adapter-node-http';
import FSPersister from '@pollyjs/persister-fs';

Polly.register(NodeHttpAdapter);
Polly.register(FSPersister);

export const pollySetup = (pollyOptionOverrides: PollyConfig = {}) =>
  setupPolly({
    adapters: ['node-http'],
    mode: 'replay',
    persister: 'fs',
    recordFailedRequests: true,
    flushRequestsOnStop: true,
    persisterOptions: {
      fs: { recordingsDir: 'test/__recordings__' },
    },
    matchRequestsBy: {
      method: true,
      headers: true,
      body: true,
      order: true,
      url: {
        protocol: true,
        username: true,
        password: true,
        hostname: true,
        port: true,
        pathname: true,
        query: true,
        hash: true,
      },
    },
    ...pollyOptionOverrides,
  });

export const passthroughLocalRequests = (pollyContext: Context) =>
  pollyContext.polly.server
    .any()
    .filter((req) => /^(127.0.0.1|docker):[0-9]+$/.test(String(req.headers.host)))
    .passthrough();
