import { Alert } from 'knockout-schema';
import {
  createRole,
  createUser,
  createSubforum,
  createThread,
  createReadThread,
  createPost,
} from '../factories';
import initializeAssociations from '../../src/models/associations';
import User from '../../src/models/user';
import Thread from '../../src/models/thread';
import ReadThread from '../../src/models/readThread';
import * as readThreadController from '../../src/controllers/readThreadController';

describe('alertController', () => {
  let user: User;
  let role;
  let subforum: { id: number };
  let thread: Thread;

  beforeAll(async () => {
    initializeAssociations();
  });

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
  });

  describe('store', () => {
    it('creates an alert if it doesnt exist for the user and thread', async () => {
      const alertCountBefore = await ReadThread.count({
        where: { user_id: user.id, isSubscription: true },
      });

      await readThreadController.store(user.id, thread.id, 4, new Date(), true);

      const alertCountAfter = await ReadThread.count({
        where: { user_id: user.id, isSubscription: true },
      });
      expect(alertCountAfter - alertCountBefore).toEqual(1);

      const newAlert = await ReadThread.findOne({
        where: { user_id: user.id, thread_id: thread.id, isSubscription: true },
      });
      expect(newAlert!.dataValues.lastPostNumber).toEqual(4);
    });

    it('updates an alert if it exists for the user and thread', async () => {
      await createReadThread({
        user_id: user.id,
        thread_id: thread.id,
        lastPostNumber: 4,
        isSubscription: true,
      });

      const alertCountBefore = await ReadThread.count({
        where: { user_id: user.id, isSubscription: true },
      });

      await readThreadController.store(user.id, thread.id, 5, new Date(), true);

      const alertCountAfter = await ReadThread.count({
        where: { user_id: user.id, isSubscription: true },
      });
      expect(alertCountAfter - alertCountBefore).toEqual(0);

      const newAlert = await ReadThread.findOne({
        where: { user_id: user.id, thread_id: thread.id, isSubscription: true },
      });
      expect(newAlert!.dataValues.lastPostNumber).toEqual(5);
    });
  });

  describe('get', () => {
    it('returns a list of alerts for the user', async () => {
      let alert = await createReadThread({
        user_id: user.id,
        thread_id: thread.id,
        isSubscription: true,
      });

      await createPost({ user_id: user.id, thread_id: thread.id });
      await (alert as any).update({ lastPostNumber: null });
      await alert.save();

      const alerts = await readThreadController.get(user.id, undefined, undefined, true);
      expect(alerts.totalAlerts).toEqual(1);

      const receivedAlert: Alert = alerts.alerts[0];
      expect(receivedAlert.id).toEqual(alert.thread_id);

      alert = await alert.reload();
      expect(alert.lastPostNumber).toEqual(1);
    });
  });

  describe('destroy', () => {
    it('destroys alerts for the user', async () => {
      await createReadThread({
        user_id: user.id,
        thread_id: thread.id,
        lastPostNumber: 4,
        isSubscription: true,
      });

      await readThreadController.destroy(user.id, thread.id);

      const alerts = await ReadThread.findAll({
        where: { user_id: user.id, isSubscription: true },
      });
      expect(alerts.length).toEqual(0);
    });
  });
});
