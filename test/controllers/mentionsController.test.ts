import { getMentions, markMentionInactive } from '../../src/controllers/mentionsController';
import User from '../../src/models/user';
import { createMention, createPost, createSubforum, createThread, createUser } from '../factories';

describe('mentionsController', () => {
  let mention;
  let post;
  let mentionsUser: User;

  beforeEach(async () => {
    mentionsUser = await createUser();
    const mentionedByUser = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ user_id: mentionedByUser.id, subforum_id: subforum.id });
    post = await createPost({ user_id: mentionedByUser.id, thread_id: thread.id });
    mention = await createMention({
      postId: post.id,
      mentionsUser: mentionsUser.id,
      mentionedBy: mentionedByUser.id,
      threadId: thread.id,
      page: 1,
    });
  });

  describe('markMentionInactive', () => {
    it('marks mentions as inactive', async () => {
      expect(mention.inactive).toBe(false);
      await markMentionInactive([post.id], mentionsUser.id);
      await mention.reload();
      expect(mention.inactive).toBe(true);
    });
  });

  describe('getMentions', () => {
    it('returns mentions for a user', async () => {
      const mentions = await getMentions(mentionsUser.id);
      expect(mentions.length).toBe(1);
      expect(mentions[0].id).toBe(mention.id);
    });
  });
});
