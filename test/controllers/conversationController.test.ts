import * as conversationController from '../../src/controllers/conversationController';
import initializeAssociations from '../../src/models/associations';
import User from '../../src/models/user';
import UserArchivedConversation from '../../src/models/userArchivedConversation';
import { createConversation, createUser } from '../factories';

describe('conversationController', () => {
  let userOne: User;
  let userTwo: User;
  let conversation;

  beforeAll(async () => {
    initializeAssociations();
  });

  beforeEach(async () => {
    userOne = await createUser();
    userTwo = await createUser();
    conversation = await createConversation([userOne.id, userTwo.id]);
  });

  describe('isUserInConversation', () => {
    test('when a user is part of a conversation', async () => {
      expect(await conversationController.isUserInConversation(conversation.id, userOne.id)).toBe(
        true
      );
    });

    test('when a user is not part of a conversation', async () => {
      const newUser = await createUser();

      expect(await conversationController.isUserInConversation(conversation.id, newUser.id)).toBe(
        false
      );
    });
  });

  describe('getConversations', () => {
    test('returns all conversations a user is a part of', async () => {
      const conversations = await conversationController.getConversations(userOne.id);
      expect(conversations.length).toEqual(1);
      expect(conversations[0].id).toEqual(conversation.id);
    });

    describe('when the user has archived a conversation', () => {
      beforeEach(async () => {
        await UserArchivedConversation.create({
          userId: userOne.id,
          conversationId: conversation.id,
        });
      });

      test('does not return the archived conversation for the user who archived it', async () => {
        const conversations = await conversationController.getConversations(userOne.id);
        expect(conversations.length).toEqual(0);
      });

      test('returns the archived conversation for a different user', async () => {
        const conversations = await conversationController.getConversations(userTwo.id);
        expect(conversations.length).toEqual(1);
        expect(conversations[0].id).toEqual(conversation.id);
      });
    });
  });

  describe('archive', () => {
    test('archives a conversation', async () => {
      await conversationController.archiveConversation(userOne.id, conversation.id);
      const conversations = await conversationController.getConversations(userOne.id);
      expect(conversations.length).toEqual(0);
    });
  });
});
