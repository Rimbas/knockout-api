'use strict';

const READ_THREADS_PER_USER_MAX = 100;

const randomInt = (max) => {
  return Math.floor(Math.random() * (max - 1) + 1);
}

const randomReadThread = (userId, threadId, createdAt) => {
  return {
    user_id: userId,
    thread_id: threadId,
    last_seen: createdAt,
    created_at: createdAt,
    updated_at: createdAt,
  }
}

const randomReadThreads = (userIds, threads) => {
  const readThreads = userIds.map((userId) => {
    const threadCount = randomInt(READ_THREADS_PER_USER_MAX);
    const threadSet = new Set();
    return Array.from({ length: threadCount }).map(() => {
      let randomIndex;
      do {
       randomIndex = randomInt(threads.length);
      } while (threadSet.has(randomIndex));
      threadSet.add(randomIndex);
      const thread = threads[randomIndex];
      const createdAt = new Date(thread.createdAt);
      return randomReadThread(userId, thread.id, createdAt);
    })
  })

  return [].concat(...readThreads);
}

export async function up(queryInterface, Sequelize) {
  const userIds = (await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  )).map((record) => record.id);

  const threads = (await queryInterface.sequelize.query(
    `SELECT id, created_at FROM Threads`,
    { type: Sequelize.QueryTypes.SELECT }
  )).map((record) => {
    return {
      id: record.id, 
      createdAt: record.created_at
    }
  });

  return queryInterface.bulkInsert('ReadThreads', randomReadThreads(userIds, threads), {});
}

export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('ReadThreads', null, {});
}
