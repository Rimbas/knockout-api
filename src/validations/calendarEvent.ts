import { CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS } from 'knockout-schema';
import ms from 'ms';

const calendarEventDateRangeError = (startDateTimestamp: Date, endDateTimestamp: Date) => {
  if (startDateTimestamp > endDateTimestamp) {
    return 'Start date must not be after end date';
  }

  const dateRange = endDateTimestamp.getTime() - startDateTimestamp.getTime();
  if (dateRange > ms(`${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} days`)) {
    return `Range bewtween startDate and endDate must be less than ${CALENDAR_EVENT_DATE_RANGE_LIMIT_DAYS} days`;
  }
  return null;
};

export default calendarEventDateRangeError;
