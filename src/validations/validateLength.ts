import { NEWLINE_MULTIPLIER } from 'knockout-schema';

export default (content, characterLimit) => {
  const postContent = content.trim();
  if (postContent.length < 1) {
    return false;
  }

  const lineBreakAdjustment = (postContent.match(/\n/g) || '').length * NEWLINE_MULTIPLIER;
  if (postContent.length + lineBreakAdjustment > characterLimit) {
    return false;
  }

  return true;
};
