/* eslint-disable class-methods-use-this */
import { INTERNAL_SERVER_ERROR } from 'http-status';
import { Get, JsonController, Params, Res } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { Response } from 'express';
import { KnockoutCommit, OpenAPIParam } from 'knockout-schema';
import { getCommitsForAllProjects } from '../helpers/gitlab';

@OpenAPI({ tags: ['Changelog'] })
@JsonController('/changelog')
export default class ChangelogController {
  @Get('/:page?')
  @OpenAPI({
    summary: 'View all commits for the Knockout frontend and backend',
  })
  @OpenAPIParam('page', {
    description: 'The page of commits to load',
    required: false,
  })
  @ResponseSchema(KnockoutCommit, { isArray: true })
  async getChangelog(@Params() { page = 1 }, @Res() response: Response) {
    const commits = await getCommitsForAllProjects(page);

    if (commits.length > 0) {
      return commits;
    }
    return response.status(INTERNAL_SERVER_ERROR).json({ error: 'Could not fetch commits' });
  }
}
