/* eslint-disable class-methods-use-this */
import { Request } from 'express';
import httpStatus from 'http-status';
import { PaymentPortal, PaymentPortalRequest } from 'knockout-schema';
import { Body, HttpCode, JsonController, Post, Req, UseBefore } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { authentication } from '../middleware/auth';
import { paymentPortalSessionUrl } from '../helpers/stripe';

@OpenAPI({ tags: ['Payment Portal'] })
@JsonController('/payment-portal')
export default class PaymentPortalController {
  @Post('/')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary: `Create and return a Stripe Payment Portal session URL for the current user.`,
  })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(PaymentPortal)
  async getPaymentPortalSession(
    @Req() request: Request,
    @Body() body: PaymentPortalRequest
  ): Promise<PaymentPortal> {
    const url = await paymentPortalSessionUrl(request.user!.id, body.returnUrl);
    return { url };
  }
}
