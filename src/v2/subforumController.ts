/* eslint-disable class-methods-use-this */
import { Get, JsonController, UseBefore, Param, Req, QueryParam } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { Request } from 'express';
import { GetSubforumThreadsResponse, OpenAPIParam, Subforum } from 'knockout-schema';
import { authentication } from '../middleware/auth';
import { subforumGetAllController, subforumGetThreadsController } from '../controllers';
import errorHandler from '../services/errorHandler';
import { subforumGetThreadsPolicy } from '../policies';

const { catchErrors } = errorHandler;

@OpenAPI({ tags: ['Subforums'] })
@JsonController('/subforums')
export default class SubforumController {
  @Get('/')
  @UseBefore(authentication.optional)
  @OpenAPI({ summary: 'Get all subforums and their latest posts' })
  @OpenAPIParam('hideNsfw', { description: 'Whether or not to hide NSFW threads.', example: '1' })
  @ResponseSchema(Subforum, { isArray: true })
  async getSubforums(
    @QueryParam('hideNsfw') hideNsfw: string,
    @Req() request: Request
  ): Promise<Subforum[]> {
    return subforumGetAllController.getSubforums(hideNsfw, request.user?.id);
  }

  @Get('/:id/:page?')
  @UseBefore(...[authentication.optional, catchErrors(subforumGetThreadsPolicy.getThreads)])
  @OpenAPI({ summary: 'Get the most recently updated threads from a subforum' })
  @OpenAPIParam('hideNsfw', { description: 'Whether or not to hide NSFW threads.', example: '1' })
  @OpenAPIParam('id', { description: 'The id of the subforum.' })
  @OpenAPIParam('page', { description: 'The page of the subforum.' })
  @ResponseSchema(GetSubforumThreadsResponse)
  async getSubforum(
    @Param('id') id: number,
    @Param('page') page: number,
    @QueryParam('hideNsfw') hideNsfw: string,
    @Req() request: Request
  ): Promise<GetSubforumThreadsResponse> {
    return subforumGetThreadsController.getSubformThreads(
      id,
      Boolean(Number(hideNsfw)),
      request.user?.id,
      page
    );
  }
}
