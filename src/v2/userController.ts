/* eslint-disable class-methods-use-this */
import {
  Get,
  JsonController,
  UseBefore,
  Param,
  Put,
  Body,
  UploadedFile,
  Req,
  Delete,
  Post,
  HttpCode,
  NotFoundError,
  QueryParam,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import multer from 'multer';
import { Request } from 'express';
import httpStatus from 'http-status';
import {
  OpenAPIParam,
  UserProfile,
  UpdateUserProfileRequest,
  UserProfileBackgroundRequest,
  UserProfileHeaderResponse,
  UserProfileComment,
  UserProfileCommentPage,
  CreateUserProfileCommentRequest,
  WipeAccountRequest,
  User,
  UpdateUserRequest,
  CheckUsernameRequest,
  PreviousUsername as PreviousUsernameSchema,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import { userController, userProfileController } from '../controllers';
import { rateLimiterUserMiddleware } from '../middleware/rateLimit';
import ProfileCommentRetriever from '../retriever/profileComment';
import {
  moderationControllerPolicy,
  userControllerPolicy,
  userProfileControllerPolicy,
} from '../policies';
import errorHandler from '../services/errorHandler';
import { userQueue } from '../jobs';
import { UserJob } from '../jobs/processUserJobs';
import { jobInfo } from '../log';
import { banUser } from '../controllers/banController';
import PreviousUsername from '../models/previousUsername';

const { catchErrors } = errorHandler;

@OpenAPI({ tags: ['Users'] })
@JsonController('/users')
export default class UserController {
  @Put('/:id')
  @UseBefore(...[authentication.required, catchErrors(userControllerPolicy.update)])
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @ResponseSchema(User)
  updateeUser(@Body() body: UpdateUserRequest, @Param('id') id: number): Promise<User> {
    return userController.update(id, body);
  }

  @Put('/:id/check-username')
  @UseBefore(...[authentication.required, catchErrors(userControllerPolicy.update)])
  @OpenAPIParam('id', { description: 'The id of the user.' })
  checkUsername(@Body() body: CheckUsernameRequest, @Req() request: Request) {
    return userController.checkUsername(request.user, body.username);
  }

  @Delete('/:id/wipe')
  @UseBefore(...[authentication.required, catchErrors(userControllerPolicy.wipeAccount)])
  @OpenAPIParam('id', { description: 'The id of the user.' })
  async wipeUser(
    @Body() body: WipeAccountRequest,
    @Param('id') id: number,
    @Req() request: Request
  ) {
    await banUser(0, id, body.reason, request.user!.id, request.app.get('io'), undefined, true);

    jobInfo(`Enqueuing user wipe job`);
    await userQueue.add(
      { type: UserJob.WIPE_USER, userId: id },
      { attempts: 3, backoff: { type: 'exponential', delay: 30000 }, jobId: `user-wipe-${id}` }
    );
    return { status: 'User wipe in progress' };
  }

  @Delete('/:id')
  @UseBefore(...[authentication.required, catchErrors(userControllerPolicy.deleteAccount)])
  @OpenAPIParam('id', { description: 'The id of the user.' })
  async deleteUser(@Param('id') id: number) {
    return userController.archive(id);
  }

  @Get('/:id/previous-usernames')
  @UseBefore(...[authentication.required, catchErrors(moderationControllerPolicy.getFullUserInfo)])
  @OpenAPI({ summary: "Get a user's previous usernames" })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @ResponseSchema(PreviousUsernameSchema, { isArray: true })
  async getPreviousUsernames(@Param('id') id: number): Promise<PreviousUsernameSchema[]> {
    const previousUsernames = await PreviousUsername.findAll({
      attributes: ['username', 'createdAt'],
      where: { user_id: id },
      order: [['created_at', 'DESC']],
    });

    return previousUsernames.map((previousUsername) => ({
      ...previousUsername.toJSON(),
      createdAt: previousUsername.createdAt.toString(),
    })) as PreviousUsernameSchema[];
  }

  @Get('/:id/profile')
  @UseBefore(authentication.optional)
  @OpenAPI({ summary: 'Get a user profile' })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @ResponseSchema(UserProfile)
  getUserProfile(@Param('id') id: number): Promise<UserProfile> {
    return userProfileController.get(id);
  }

  @Put('/:id/profile')
  @UseBefore(...[authentication.required, catchErrors(userProfileControllerPolicy.update)])
  @OpenAPI({ summary: 'Update a user profile' })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  updateUserProfile(@Param('id') id: number, @Body() body: UpdateUserProfileRequest) {
    return userProfileController.update(id, body);
  }

  @Put('/:id/profile/background')
  @UseBefore(
    ...[authentication.required, catchErrors(userProfileControllerPolicy.updateBackground)]
  )
  @OpenAPI({
    summary: "Update a user profile's background",
    requestBody: {
      content: {
        'multipart/form-data': {
          schema: { $ref: '#/components/schemas/UserProfileBackgroundRequest' },
        },
      },
    },
  })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  updateUserProfileBackground(
    @Req() request: Request,
    @UploadedFile('image', { options: { storage: multer.memoryStorage() } }) file: any,
    @Body() body: UserProfileBackgroundRequest
  ) {
    return userProfileController.updateBackground(file, request.user, body.type);
  }

  @Put('/:id/profile/header')
  @UseBefore(...[authentication.required, catchErrors(userProfileControllerPolicy.updateHeader)])
  @OpenAPI({
    summary: "Update a user profile's header",
    requestBody: {
      content: {
        'multipart/form-data': {
          schema: {
            properties: {
              image: {
                format: 'base64',
                type: 'string',
              },
            },
            type: 'object',
            required: ['image'],
          },
        },
      },
    },
  })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @ResponseSchema(UserProfileHeaderResponse)
  updateUserProfileHeader(
    @Param('id') id: number,
    @UploadedFile('image', { options: { storage: multer.memoryStorage() } }) file: any
  ) {
    return userProfileController.updateHeader(file, id);
  }

  @Delete('/:id/profile/header')
  @UseBefore(...[authentication.required, catchErrors(userProfileControllerPolicy.removeHeader)])
  @OpenAPI({
    summary: "Remove a user profile's header",
  })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  deleteUserProfileHeader(@Param('id') id: number) {
    return userProfileController.removeHeader(id);
  }

  @Get('/:id/profile/comments/')
  @UseBefore(authentication.optional)
  @OpenAPI({ summary: "Get a user profile's comments" })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @OpenAPIParam('page', { description: 'The page of the thread.' })
  @ResponseSchema(UserProfileComment, { isArray: true })
  getUserProfileComments(
    @Param('id') id: number,
    @QueryParam('page') page: number
  ): Promise<UserProfileCommentPage> {
    return userProfileController.getComments(id, page);
  }

  @Post('/:id/profile/comments')
  @UseBefore(
    ...[
      authentication.required,
      rateLimiterUserMiddleware,
      catchErrors(userProfileControllerPolicy.createComment),
    ]
  )
  @OpenAPI({ summary: "Create a comment on a user's profile" })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(UserProfileComment)
  async createUserProfileComment(
    @Param('id') id: number,
    @Req() request: Request,
    @Body() body: CreateUserProfileCommentRequest
  ): Promise<UserProfileComment> {
    return userProfileController.createComment(id, body, request.user!.id, request.app.get('io'));
  }

  @Delete('/:id/profile/comments/:commentId')
  @UseBefore(...[authentication.required, catchErrors(userProfileControllerPolicy.deleteComment)])
  @OpenAPI({ summary: "Delete a comment on a user's profile" })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @OpenAPIParam('commentId', { description: 'The id of the profile comment.' })
  async deleteUserProfileComment(@Param('id') id: number, @Param('commentId') commentId: number) {
    const comment = await new ProfileCommentRetriever(commentId).getSingleObject();

    if (!comment?.id) throw new NotFoundError();
    return userProfileController.deleteComment(id, commentId);
  }
}
