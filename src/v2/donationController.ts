/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import { Request } from 'express';
import { JsonController, UseBefore, Post, Body, HttpCode, Req, Get } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import {
  DonationCheckoutSessionResponse,
  DonationCheckoutSessionRequest,
  DonationGetUpgradeExpirationResponse,
} from 'knockout-schema';
import { authentication } from '../middleware/auth';
import UserRetriever, { UserFlag } from '../retriever/user';
import { donationCheckoutSession } from '../helpers/stripe';

@OpenAPI({ tags: ['Donations'] })
@JsonController('/donations')
export default class DonationController {
  @Post('/checkout-session')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary: `Create and return a Knockout donation Stripe checkout session for the current user. The Stripe Session ID is available in the URL parameters on successful checkout redirection in the URL parameters.`,
  })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(DonationCheckoutSessionResponse)
  async checkout(@Req() request: Request, @Body() body: DonationCheckoutSessionRequest) {
    const session = await donationCheckoutSession(
      request.user!.id,
      body.successUrl,
      body.cancelUrl
    );
    return { sessionId: session?.id };
  }

  @Get('/upgrade-expiration')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Get the expiration date of the current users donation upgrades' })
  @HttpCode(httpStatus.OK)
  @ResponseSchema(DonationGetUpgradeExpirationResponse)
  async getUpgradeExpiration(
    @Req() request: Request
  ): Promise<DonationGetUpgradeExpirationResponse> {
    const { donationUpgradeExpiresAt } = await new UserRetriever(request.user!.id, [
      UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
    ]).getSingleObject();
    return { expiresAt: donationUpgradeExpiresAt };
  }
}
