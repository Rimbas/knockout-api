/* eslint-disable class-methods-use-this */
import { Request } from 'express';
import {
  JsonController,
  UseBefore,
  Req,
  Post,
  Param,
  Delete,
  Get,
  HttpCode,
  Body,
  QueryParam,
  OnUndefined,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { AlertsResponse, CreateAlertRequest, OpenAPIParam } from 'knockout-schema';
import httpStatus from 'http-status';
import { authentication } from '../middleware/auth';
import { destroy, get, storeReadThread } from '../controllers/readThreadController';

@OpenAPI({ tags: ['Read Threads'] })
@JsonController('/read-threads')
export default class ReadThreadController {
  @Post('/')
  @HttpCode(httpStatus.CREATED)
  @OnUndefined(httpStatus.CREATED)
  @UseBefore(authentication.required)
  async createReadThread(@Body() body: CreateAlertRequest, @Req() request: Request) {
    return storeReadThread(request.user!.id, body.threadId, body.lastPostNumber);
  }

  @Get('/:page')
  @UseBefore(authentication.required)
  @OpenAPIParam('page', { description: 'The page of read threads to retreive' })
  @OpenAPIParam('hideNsfw', { description: 'Whether to hide NSFW threads' })
  @HttpCode(httpStatus.OK)
  @ResponseSchema(AlertsResponse)
  async getReadThreads(
    @QueryParam('hideNsfw') hideNsfw: number,
    @Req() request: Request,
    @Param('page') page: number = 1
  ) {
    return get(request.user!.id, page, Boolean(hideNsfw));
  }

  @Delete('/:id')
  @UseBefore(authentication.required)
  @OpenAPIParam('id', { description: 'The id of the read thread.' })
  async unreadThread(@Param('id') id: number, @Req() request: Request) {
    return destroy(request.user!.id, id);
  }
}
