/* eslint-disable class-methods-use-this */
import { Get, JsonController } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { User } from 'knockout-schema';
import { getModeratorLevelRoleIds } from '../helpers/role';
import UserRetriever from '../retriever/user';
import UserModel from '../models/user';

@OpenAPI({ tags: ['Community Team'] })
@JsonController('/community-team')
export default class CommunityTeamController {
  @Get('/')
  @OpenAPI({
    summary: 'View all users on the Community Team',
  })
  @ResponseSchema(User, { isArray: true })
  async getCommunityTeam() {
    const communityTeamRoleIds = await getModeratorLevelRoleIds();

    const userIds = (
      await UserModel.findAll({
        attributes: ['id'],
        raw: true,
        where: { roleId: communityTeamRoleIds },
      })
    ).map((user) => user.id);

    return new UserRetriever(userIds).getObjectArray();
  }
}
