/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import { ForbiddenError, UnauthorizedError } from 'routing-controllers';
import { isUserInConversation } from '../controllers/conversationController';

export const show = async (req: Request, res: Response, next: NextFunction) => {
  if (!req.user) {
    throw new UnauthorizedError('Insufficient user permissions.');
  }

  const userInConversation = await isUserInConversation(Number(req.params.id), req.user.id);

  if (!userInConversation) {
    throw new ForbiddenError('Insufficient user permissions.');
  }
  return next();
};
