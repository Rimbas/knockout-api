import { NextFunction, Request, Response } from 'express';
import { ForbiddenError, UnauthorizedError } from 'routing-controllers';
import { isUserInConversation } from '../controllers/conversationController';
import authorize from '../helpers/authorize';
import MessageRetriever from '../retriever/message';
import UserRetriever from '../retriever/user';

export const store = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  if (!req.user) {
    throw new UnauthorizedError('Insufficient user permissions.');
  }
  if (req.body.conversationId) {
    const { conversationId } = req.body;
    const userInConversation = await isUserInConversation(conversationId, req.user?.id);
    if (!userInConversation) {
      throw new ForbiddenError('Insufficient user permissions.');
    }
  } else {
    await authorize(req.user?.id, ['message-create']);
  }

  // receiving user must not have incoming messages disabled
  // if they do, the sending user must have the message-create-override permission
  const receivingUser = await new UserRetriever(req.body?.receivingUserId)?.getSingleObject();
  if (receivingUser?.disableIncomingMessages) {
    await authorize(req.user.id, ['message-create-override']);
  }

  return next();
};

export const update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  if (!req.user) {
    throw new UnauthorizedError('Insufficient user permissions.');
  }
  const message = await new MessageRetriever(req.params.id).getSingleObject();
  const userInConversation = await isUserInConversation(message.conversationId, req.user?.id);
  if (!userInConversation) {
    throw new ForbiddenError('Insufficient user permissions.');
  }
  return next();
};
