/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import { ForbiddenError, UnauthorizedError } from 'routing-controllers';
import ms from 'ms';
import { PermissionCode } from 'knockout-schema';
import { Attributes } from 'sequelize';
import authorize from '../helpers/authorize';
import PostRetriever, { PostFlag } from '../retriever/post';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import { isLimitedUser } from '../validations/user';
import redis from '../services/redisClient';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import LogEvent from '../services/logEvent';
import Scamalytics from '../services/scamalytics';
import Ripe from '../services/ripe';
import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';
import Post from '../models/post';

const getThread = async (threadId: number) =>
  new ThreadRetriever(threadId, [ThreadFlag.RETRIEVE_SHALLOW]).getSingleObject();

export const get = async (req: Request, res: Response, next: NextFunction) => {
  const post = new PostRetriever(req.params.id, [PostFlag.INCLUDE_THREAD]).getSingleObject();

  const permissionCodes = getUserPermissionCodes(req.user?.id);
  if (!userCanViewThread(await permissionCodes, (await post).thread!, req.user?.id)) {
    throw new ForbiddenError('Insufficient user permissions.');
  }

  return next();
};

export const postStore = async (req: Request, res: Response, next: NextFunction) => {
  if (!req.user) {
    throw new UnauthorizedError('User is unauthorized.');
  }
  const thread = await getThread(req.body.thread_id);
  const limitedUser = await isLimitedUser(req.user.id);

  const logEvent = new LogEvent(LogEvent.ENTITY_POST, LogEvent.ACTION_CREATE)
    .setUserId(req.user.id)
    .setProperties({ ip: req.ipInfo })
    .setSuccess(false);

  // if the user is limited:
  // - check IP
  // - if raid mode, prevent from posting
  // - if last post made within 5 mins, prevent from posting
  if (limitedUser) {
    if (await new Scamalytics(req.ipInfo).isRisky()) {
      logEvent.setMessage('VPN detected').log();
      throw new ForbiddenError('You appear to be using a proxy/VPN.');
    }

    // Do we have a ban in place for their netname or parent ASN?
    if ((await new Ripe(req.ipInfo).isBanned()) || (await new Ripe(req.ipInfo).isParentBanned())) {
      logEvent.setMessage('Banned ISP detected').log();
      throw new ForbiddenError('You appear to be using an ISP which we have banned.');
    }

    const raidModeEnabled = await redis.get(RAID_MODE_KEY);

    if (raidModeEnabled) {
      throw new ForbiddenError('Our servers are under heavy load, try posting again later.');
    }

    const recentPost = await Post.findOne({
      attributes: ['createdAt'] as (keyof Attributes<Post>)[],
      where: { user_id: req.user.id },
      order: [['createdAt', 'DESC']],
    });

    if (recentPost) {
      const now = new Date();
      const { createdAt } = recentPost;
      const diff = now.getTime() - createdAt.getTime();

      if (diff < ms('5 minutes')) {
        throw new ForbiddenError(`Please wait ${ms(ms('5 minutes') - diff)} before posting again.`);
      }
    }
  }
  const userPermissionCodes = getUserPermissionCodes(req.user.id);
  await authorize(req.user.id, [`subforum-${thread.subforumId}-post-create`]);

  if (!userCanViewThread(await userPermissionCodes, thread, req.user.id)) {
    throw new ForbiddenError('You do not have permission to post in this thread.');
  }
  logEvent.setSuccess(true).log();
  return next();
};

export const postUpdate = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  // post must belong to user, or the user must have the
  // subforum-wide post-edit permission
  const post = await new PostRetriever(req.params.id, [PostFlag.INCLUDE_THREAD]).getSingleObject();

  // user must still be able to post in the subforum (e.g. banned users editing their posts)
  const requiredCodes: PermissionCode[] = [`subforum-${post.thread!.subforumId}-post-create`];

  // if the post does not belong to the user, see if user has subforum
  // post edit permission
  if (post.user!.id !== req.user?.id) {
    requiredCodes.push(`subforum-${post.thread!.subforumId}-post-update`);
  }
  await authorize(req.user?.id, requiredCodes);
  return next();
};

export const postRatingStore = async (req: Request, res: Response, next: NextFunction) => {
  if (!req.user) {
    throw new UnauthorizedError('User is unauthorized.');
  }

  const MIN_POST_COUNT_TO_RATE = 3;

  const post = await new PostRetriever(req.params.id, [PostFlag.INCLUDE_THREAD]).getSingleObject();

  const limitedUser = await isLimitedUser(req.user.id);

  if (limitedUser) {
    const postCount = await Post.count({
      where: { user_id: req.user.id },
    });

    if (postCount < MIN_POST_COUNT_TO_RATE) {
      const remainingPosts = MIN_POST_COUNT_TO_RATE - postCount;
      throw new ForbiddenError(
        `You must make at least ${remainingPosts} more ${
          remainingPosts === 1 ? 'post' : 'posts'
        } before you can rate other posts.`
      );
    }
  }

  await authorize(req.user?.id, [`subforum-${post.thread!.subforumId}-post-rating-create`]);
  return next();
};
