import express from 'express';
import multer from 'multer';

import errorHandler from '../services/errorHandler';

import { authMiddleware, contentFormatVersion } from '../middleware';

import {
  subforumGetAllController,
  subforumGetThreadsController,
  reportGetController,
  reportCreateController,
  reportResolveController,
  reportsGetAllController,
  reportsOpenCountController,
  postController,
  threadController,
  authController,
  usersController,
  userController,
  ratingsController,
  banController,
  moderationController,
  imageController,
  statsController,
  eventLogController,
  mentionsController,
  readThreadController,
  userProfileController,
  tagController,
  threadAdController,
  messageOfTheDayController,
  postImageController,
  conversationController,
  messageController,
  stripeWebhookController,
} from '../controllers';
import { rateLimiterUserMiddleware } from '../middleware/rateLimit';
import { logIpInfo } from '../middleware/ip';
import {
  banControllerPolicy,
  messageOfTheDayControllerPolicy,
  conversationControllerPolicy,
  imageControllerPolicy,
  messageControllerPolicy,
  reportGetPolicy,
  reportResolvePolicy,
  reportCreatePolicy,
  reportGetAllPolicy,
  tagControllerPolicy,
  moderationControllerPolicy,
  reportGetOpenCountPolicy,
  postControllerPolicy,
  threadControllerPolicy,
  subforumGetThreadsPolicy,
  userProfileControllerPolicy,
  userControllerPolicy,
} from '../policies';
import tweetEmbed from '../controllers/embedController';

const storage = multer.memoryStorage();
const upload = multer({ storage });

const { catchErrors } = errorHandler;
const { authentication } = authMiddleware;
const router = express.Router();

const saveRedirectUrl = (req, res, next) => {
  req.session.redirect = req.query.redirect;
  next();
};

// Auth
router.get('/auth/google/login', saveRedirectUrl, authController.passport.google);
router.get('/auth/google/callback', authController.passport.google);
router.get('/auth/twitter/login', saveRedirectUrl, authController.passport.twitter);
router.get('/auth/twitter/callback', authController.passport.twitter);
router.get('/auth/github/login', saveRedirectUrl, authController.passport.github);
router.get('/auth/github/callback', authController.passport.github);
router.get('/auth/steam/login', saveRedirectUrl, authController.passport.steam);
router.get('/auth/steam/callback', authController.passport.steam);
router.post('/auth/request-token', authController.requestToken);
router.post('/auth/logout', authController.logout);
router.get('/auth/finish', authController.finish);

// Stats
router.get('/stats', statsController.index);

// Users
router.get('/users/:page?', authentication, catchErrors(usersController.search));

// User
router.get('/user', authentication.optional, userController.index);
router.put('/user', contentFormatVersion, authentication, userController.updateUser);
router.get('/user/syncData', authentication.optional, logIpInfo, userController.syncData);
router.put(
  '/user/profile',
  authentication.optional,
  catchErrors(userProfileControllerPolicy.update),
  upload.single('backgroundImage'),
  userProfileController.updateProfile
);
router.put(
  '/user/updateProfileRatingsDisplay',
  authentication,
  userController.updateProfileRatingsDisplay
);

router.get('/user/profileRatingsDisplay', authentication, userController.getProfileRatingsDisplay);
router.get('/user/:id', authentication.optional, catchErrors(userController.show));
router.get('/user/:id/bans', authentication.optional, userController.getBans);
router.get('/user/:id/threads/:page?', authentication.optional, userController.getThreads);
router.get('/user/:id/posts/:page?', authentication.optional, userController.getPosts);
router.get('/user/:id/topRatings', authentication.optional, userController.getTopRatings);
router.get('/user/:id/profile', authentication.optional, userProfileController.show);
router.delete(
  '/user/:id',
  authentication,
  catchErrors(userControllerPolicy.deleteAccount),
  userController.deleteAccount
);

// Posts
router.get(
  '/post/:id',
  authentication.optional,
  catchErrors(postControllerPolicy.get),
  postController.withPostsAndCount
);
router.post(
  '/post',
  contentFormatVersion,
  authentication,
  rateLimiterUserMiddleware,
  catchErrors(postControllerPolicy.postStore),
  catchErrors(postController.storePost)
);
router.put(
  '/post',
  contentFormatVersion,
  authentication,
  catchErrors(postControllerPolicy.postUpdate),
  catchErrors(postController.updatePost)
);

router.get('/tweet-embed', authentication.optional, catchErrors(tweetEmbed));

// Threads
router.get('/thread/latest', catchErrors(threadController.latestThreads));
router.get('/thread/popular', catchErrors(threadController.popularThreads));
router.get('/popular-threads', catchErrors(threadController.popularThreads));
router.get(
  '/thread/:id/:page?',
  authentication.optional,
  catchErrors(threadControllerPolicy.getPostsAndCount),
  threadController.withPostsAndCount
);
router.post(
  '/thread',
  contentFormatVersion,
  authentication,
  rateLimiterUserMiddleware,
  catchErrors(threadControllerPolicy.store),
  catchErrors(threadController.storeThread)
);
router.put(
  '/thread',
  contentFormatVersion,
  authentication,
  catchErrors(threadControllerPolicy.update),
  catchErrors(threadController.updateThread)
);
router.put(
  '/thread/tags',
  contentFormatVersion,
  authentication,
  catchErrors(threadControllerPolicy.updateTags),
  catchErrors(threadController.updateThreadTags)
);

// Subforums
router.get('/subforum', authentication.optional, catchErrors(subforumGetAllController.getAll));
router.get(
  '/subforum/:id/:page?',
  authentication.optional,
  catchErrors(subforumGetThreadsPolicy.getThreads),
  catchErrors(subforumGetThreadsController.getThreads)
);

// Ratings
router.put(
  '/rating',
  contentFormatVersion,
  authentication,
  catchErrors(postControllerPolicy.postRatingStore),
  catchErrors(ratingsController.storeRating)
);

// Bans
router.post(
  '/ban',
  authentication,
  catchErrors(banControllerPolicy.store),
  catchErrors(banController.store)
);

// Alerts
router.post('/alert', authentication, catchErrors(readThreadController.createAlert)); // deprecate
router.delete('/alert', authentication, catchErrors(readThreadController.deleteReadThread)); // deprecate
router.post('/alert/list', authentication, catchErrors(readThreadController.listAlerts)); // deprecate

router.get('/alerts/:page?', authentication, catchErrors(readThreadController.listAlerts));
router.post('/alerts', authentication, catchErrors(readThreadController.createAlert));
router.delete('/alerts', authentication, catchErrors(readThreadController.deleteReadThread));

// Read Threads
router.post(
  '/readThreads',
  contentFormatVersion,
  authentication,
  catchErrors(readThreadController.createReadThread)
);
router.delete(
  '/readThreads',
  contentFormatVersion,
  authentication,
  catchErrors(readThreadController.deleteReadThread)
);

// Reports
router.get(
  '/reports/:id',
  authentication,
  catchErrors(reportGetPolicy.get),
  catchErrors(reportGetController.get)
);
router.post(
  '/reports/:id/resolve',
  authentication,
  catchErrors(reportResolvePolicy.post),
  catchErrors(reportResolveController.post)
);
router.post(
  '/reports',
  contentFormatVersion,
  authentication,
  catchErrors(reportCreatePolicy.post),
  catchErrors(reportCreateController.post)
);
router.get(
  '/reports/:page?',
  authentication,
  catchErrors(reportGetAllPolicy.get),
  catchErrors(reportsGetAllController.get)
);
router.get(
  '/reports/open-count',
  authentication,
  catchErrors(reportGetOpenCountPolicy.get),
  catchErrors(reportsOpenCountController.get)
);

// Images
router.post(
  '/avatar',
  contentFormatVersion,
  authentication,
  catchErrors(imageControllerPolicy.avatarUpload),
  upload.single('image'),
  catchErrors(imageController.avatarUpload)
);
router.post(
  '/background',
  contentFormatVersion,
  authentication,
  upload.single('image'),
  catchErrors(imageController.backgroundUpload)
);

router.post(
  '/postImages',
  contentFormatVersion,
  authentication,
  upload.single('image'),
  catchErrors(postImageController.store)
);
router.get('/image/:filename', catchErrors(imageController.show));

// Events
router.get('/events', catchErrors(eventLogController.index));

// Mentions
router.post(
  '/mentions/get',
  contentFormatVersion,
  authentication,
  catchErrors(mentionsController.index)
);
router.put(
  '/mentions',
  contentFormatVersion,
  authentication,
  catchErrors(mentionsController.markAsRead)
);

// Tags
router.post(
  '/tag',
  contentFormatVersion,
  authentication,
  catchErrors(tagControllerPolicy.store),
  catchErrors(tagController.store)
);
router.get('/tag/list', catchErrors(tagController.index));

// Moderation
router.post(
  '/moderation/ipsByUser',
  authentication,
  catchErrors(moderationControllerPolicy.getIpsByUsername),
  catchErrors(moderationController.getIpsByUsername)
);
router.post(
  '/moderation/usersByIp',
  authentication,
  catchErrors(moderationControllerPolicy.getUsernamesByIp),
  catchErrors(moderationController.getUsernamesByIp)
);
router.post(
  '/moderation/changeThreadStatus',
  authentication,
  catchErrors(moderationControllerPolicy.changeThreadStatus),
  catchErrors(moderationController.changeThreadStatus)
);
router.post(
  '/moderation/removeUserImage',
  authentication,
  catchErrors(moderationControllerPolicy.removeUserImage),
  catchErrors(moderationController.removeUserImage)
);
router.post(
  '/moderation/removeUserProfile',
  authentication,
  catchErrors(moderationControllerPolicy.removeUserProfile),
  catchErrors(moderationController.removeUserProfile)
);
router.post(
  '/moderation/removeUserPronouns',
  authentication,
  catchErrors(moderationControllerPolicy.removeUserPronouns),
  catchErrors(moderationController.removeUserPronouns)
);
router.get(
  '/moderation/getLatestUsers',
  authentication,
  catchErrors(moderationControllerPolicy.getLatestUsers),
  catchErrors(moderationController.getLatestUsers)
);
router.get(
  '/moderation/getDashboardData',
  authentication,
  catchErrors(moderationControllerPolicy.getDashboardData),
  catchErrors(moderationController.getDashboardData)
);
router.post(
  '/moderation/getFullUserInfo',
  authentication,
  catchErrors(moderationControllerPolicy.getFullUserInfo),
  catchErrors(moderationController.getFullUserInfo)
);
router.post(
  '/moderation/makeBanInvalid',
  authentication,
  catchErrors(moderationControllerPolicy.makeBanInvalid),
  catchErrors(moderationController.makeBanInvalid)
);
router.post(
  '/moderation/banReason',
  authentication,
  catchErrors(moderationControllerPolicy.editBanReason),
  catchErrors(moderationController.editBanReason)
);

router.get(
  '/moderation/adminSettings',
  authentication,
  catchErrors(moderationControllerPolicy.getAdminSettings),
  catchErrors(moderationController.getAdminSettings)
);
router.put(
  '/moderation/adminSettings',
  authentication,
  catchErrors(moderationControllerPolicy.setAdminSettings),
  catchErrors(moderationController.setAdminSettings)
);

// Thread Ads
router.get('/threadAds/list', catchErrors(threadAdController.index));
router.get('/threadAds/random', catchErrors(threadAdController.random));

// MOTD
router.get('/motd', catchErrors(messageOfTheDayController.latest));
router.post(
  '/motd',
  authentication,
  catchErrors(messageOfTheDayControllerPolicy.create),
  catchErrors(messageOfTheDayController.create)
);

// Conversations
router.get('/conversations', authentication, catchErrors(conversationController.index));
router.get(
  '/conversations/:id',
  authentication,
  catchErrors(conversationControllerPolicy.show),
  catchErrors(conversationController.show)
);
router.post(
  '/conversations/:id/archive',
  authentication,
  catchErrors(conversationControllerPolicy.show),
  catchErrors(conversationController.destroy)
);

// Messages
router.post(
  '/messages',
  authentication,
  catchErrors(messageControllerPolicy.store),
  catchErrors(messageController.store)
);
router.put(
  '/messages/:id',
  authentication,
  catchErrors(messageControllerPolicy.update),
  catchErrors(messageController.update)
);

// Internal Stripe Webhooks
// Not in V2 because its internal, not part of the Knockout interface
// Use express.raw() because the Stripe webhook authentication method needs the raw request body
router.post('/stripeWebhooks', catchErrors(stripeWebhookController.post));

// Misc
router.get('/static/scripts/*', express.static('.'));
router.get('/static/reference.html', express.static('.'));
router.get('/version', (req, res) =>
  res.json({
    contentFormatVersion: contentFormatVersion.CONTENT_FORMAT_VERSION,
    // TODO: API version information (ie. git commit id and date)
  })
);

export default router;
