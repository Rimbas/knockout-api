import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';

const threadPostRoomPrefix = 'threads';

export enum ThreadPostRoomSuffix {
  POSTS = 'posts',
  SUBSCRIBED = 'subscribed',
}

export const threadPostRoom = (id: number, suffix: ThreadPostRoomSuffix) =>
  `${threadPostRoomPrefix}:${id}:${suffix}`;

export default async (socket) => {
  const joinAllThreadPosts = async (ids: number[], suffix: ThreadPostRoomSuffix) => {
    const permissionCodes = await getUserPermissionCodes(socket.request.user?.id);
    const threads = await new ThreadRetriever(ids, [ThreadFlag.RETRIEVE_SHALLOW]).getObjectArray();

    const rooms: string[] = [];
    threads.forEach((thread) => {
      const canViewThread = userCanViewThread(permissionCodes, thread, socket.request.user?.id);
      if (canViewThread) {
        rooms.push(threadPostRoom(thread.id, suffix));
      }
    });
    socket.join(rooms);
  };

  const joinThreadPosts = async (id: number, suffix: ThreadPostRoomSuffix) =>
    id !== undefined && joinAllThreadPosts([id], suffix);

  const leaveThreadPosts = async (id: number, suffix: ThreadPostRoomSuffix) => {
    if (socket.request.user?.id) {
      socket.leave(threadPostRoom(id, suffix));
    }
  };

  socket.on('threadPosts:join', (id) => joinThreadPosts(id, ThreadPostRoomSuffix.POSTS));
  socket.on('threadPosts:leave', (id) => leaveThreadPosts(id, ThreadPostRoomSuffix.POSTS));

  socket.on('subscribedThreadPosts:joinAll', (id) =>
    joinAllThreadPosts(id, ThreadPostRoomSuffix.SUBSCRIBED)
  );
  socket.on('subscribedThreadPosts:join', (id) =>
    joinThreadPosts(id, ThreadPostRoomSuffix.SUBSCRIBED)
  );
  socket.on('subscribedThreadPosts:leave', (id) =>
    leaveThreadPosts(id, ThreadPostRoomSuffix.SUBSCRIBED)
  );
};
