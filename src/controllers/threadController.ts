import { Request, Response, User } from 'express';
import httpStatus from 'http-status';
import {
  ForbiddenError,
  InternalServerError,
  NotFoundError,
  UnauthorizedError,
} from 'routing-controllers';
import { Server } from 'socket.io';
import {
  ThreadWithRecentPosts,
  CreateThreadRequest,
  EventType,
  UpdateThreadRequest,
  ThreadWithLastPost,
  UpdateThreadTagsRequest,
  Thread as ThreadSchema,
} from 'knockout-schema';
import { Op } from 'sequelize';
import { POSTS_PER_PAGE } from '../../config/server';
import ResponseStrategy from '../helpers/responseStrategy';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import knex from '../services/knex';
import redis from '../services/redisClient';
import { validatePostLength } from '../validations/post';
import { validateThreadTitleLength, validateThreadTagCount } from '../validations/thread';
import { validateUserFields } from '../validations/user';
import * as postController from './postController';
import errorHandler from '../services/errorHandler';
import PostRetriever, { PostFlag } from '../retriever/post';
import { getPopularThreadViewsKey } from '../handlers/threadHandler';
import { createEvent, createSocketEvent, threadUpdateEvent } from './eventLogController';
import {
  getViewableSubforumIdsByUser,
  userHasPermissions,
  userHiddenThreadIds,
} from '../helpers/user';
import { getRoleIdsWithPermissions } from '../helpers/permissions';
import ThreadTagsRetriever from '../retriever/threadTags';
import Thread from '../models/thread';
import sequelize from '../models/sequelize';
import ThreadTag from '../models/threadTag';

const POPULAR_THREADS_KEY = 'popular-threads';
const POPULAR_THREADS_TTL = 300;

async function getQueryWithMapping<T>(
  cached,
  query: () => Promise<any[]>,
  mapCallback: (row) => T
): Promise<[value: T[], fromCache: boolean]> {
  const cachedObject = cached && JSON.parse(cached);

  if (cached && cachedObject) {
    return [cachedObject, true];
  }

  const rows = await query();
  return [rows.map(mapCallback), false];
}

enum ViewerType {
  Viewers,
  RecentPosts, // Deprioritized when sorting popular threads
}

type ThreadIdAndViewers = {
  id: number;
  viewers: number;
  viewerType: ViewerType;
};

const populateWithRecentReplies = async (
  subforumId: number,
  ignoredThreadIds: Set<number>
): Promise<ThreadIdAndViewers[]> => {
  const cacheKey = `${POPULAR_THREADS_KEY}-${subforumId}`;
  const threadsCache = await redis.get(cacheKey);

  const query = async () =>
    knex
      .from('Posts as po')
      .select('th.id as thread_id', knex.raw('MAX(po.thread_post_number) as replyCount'))
      .join('Threads as th', 'po.thread_id', 'th.id')
      .where('th.subforum_id', subforumId)
      .whereNotIn('th.id', Array.from(ignoredThreadIds))
      .whereRaw(
        'po.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY) AND locked = 0 AND th.deleted_at IS NULL'
      )
      .groupBy('th.id')
      .orderByRaw('replyCount DESC')
      .limit(20);

  const [threadIdsAndReplyCounts, isCached] = await getQueryWithMapping(
    threadsCache,
    query,
    (row) => ({
      id: Number(row.thread_id),
      replies: Number(row.replyCount),
    })
  );

  // Set cache expiration to the TTL, but only if the retrieved value wasn't cached
  if (!isCached) {
    redis.setex(cacheKey, POPULAR_THREADS_TTL, JSON.stringify(threadIdsAndReplyCounts));
  }

  return threadIdsAndReplyCounts.map(({ id, replies }) => ({
    id,
    viewers: replies,
    viewerType: ViewerType.RecentPosts,
  }));
};

const filterSubforums = (subforumIds: number[], excludedSubforums: string[]): number[] => {
  if (excludedSubforums.length > 0) {
    return subforumIds.filter((id) => !excludedSubforums.includes(id.toString()));
  }

  return subforumIds;
};

export const popular = async (req: Request, excludedSubforums: string[] = []) => {
  const subforumIds = filterSubforums(
    await getViewableSubforumIdsByUser(req.user?.id),
    excludedSubforums
  );

  let allThreads: ThreadIdAndViewers[] = [];

  const pipeline = redis.pipeline();

  subforumIds.forEach((subforumId) => {
    const viewerCacheKey = getPopularThreadViewsKey(subforumId);
    pipeline.zrevrangebyscore(viewerCacheKey, 'inf', '-inf', 'WITHSCORES', 'LIMIT', 0, 20);
  });

  // do not populate with threads the user has hidden
  const hiddenThreadIds = await userHiddenThreadIds(req.user?.id);

  const viewerCacheValues = await pipeline.exec();

  const tasks = subforumIds.map(async (subforumId, index) => {
    const viewerCacheValue = (
      viewerCacheValues ? viewerCacheValues[index][1] : undefined
    ) as Array<string>;

    const threadViewerMap: object = (viewerCacheValue || []).reduce((map, k, j, res) => {
      if (j % 2 !== 0) {
        // eslint-disable-next-line no-param-reassign
        map[Number(res[j - 1])] = Number(k);
      }

      return map;
    }, {});

    let threadIds: string[] = [];

    // Only use current viewers if there's atleast 1 viewer in subforum
    if (
      Object.keys(threadViewerMap).length >= 1 &&
      viewerCacheValue.length &&
      Number(viewerCacheValue[1]) > 0
    ) {
      threadIds = Object.keys(threadViewerMap).filter(
        (id) => !hiddenThreadIds.includes(Number(id))
      );
      for (let j = 0; j < threadIds.length; ++j) {
        const threadId = threadIds[j];

        if (threadViewerMap[threadId] > 0) {
          allThreads.push({
            id: Number(threadId),
            viewers: threadViewerMap[threadId],
            viewerType: ViewerType.Viewers,
          });
        }
      }
    }

    // if we have 20 threads with active viewers, we dont need to fetch popular threads with recent replies
    if (threadIds.length === 20) {
      return;
    }

    // Create a set containing thread ids that have active viewers or are hidden by the user
    const ignoredThreadIds = new Set<number>();

    for (let j = 0; j < allThreads.length; ++j) {
      ignoredThreadIds.add(allThreads[j].id);
    }

    for (let j = 0; j < hiddenThreadIds.length; ++j) {
      ignoredThreadIds.add(hiddenThreadIds[j]);
    }

    // Get popularity by the amount of recent posts, ignoring threads that have active viewers and hidden threads
    allThreads.push(...(await populateWithRecentReplies(Number(subforumId), ignoredThreadIds)));
  });

  await Promise.all(tasks);

  const viewerFlags = [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_USER,
    ThreadFlag.INCLUDE_TAGS,
    ThreadFlag.INCLUDE_VIWERS,
  ];
  const viewerThreadsTask = new ThreadRetriever(
    allThreads.filter((t) => t.viewerType === ViewerType.Viewers).map((t) => t.id),
    viewerFlags
  ).getObjectArray();

  const repliesFlags = [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_USER,
    ThreadFlag.INCLUDE_TAGS,
    ThreadFlag.INCLUDE_RECENT_POSTS,
  ];
  const repliesThreadsTask = new ThreadRetriever(
    allThreads.filter((t) => t.viewerType === ViewerType.RecentPosts).map((t) => t.id),
    repliesFlags
  ).getObjectArray() as Promise<ThreadWithRecentPosts[]>;

  const viewerThreads = await viewerThreadsTask;
  const repliesThreads = await repliesThreadsTask;

  const viewerThreadMap: Map<number, ThreadSchema> = new Map();
  viewerThreads.forEach((thread) => {
    viewerThreadMap.set(thread.id, thread);
  });

  const repliesThreadMap: Map<number, ThreadWithRecentPosts> = new Map();
  repliesThreads.forEach((thread) => {
    repliesThreadMap.set(thread.id, thread);
  });

  // Update viewers by recent post count to match repliesThreads in case the cached number is outdated
  allThreads.forEach((thread) => {
    if (!repliesThreadMap.has(thread.id)) {
      return;
    }

    // eslint-disable-next-line no-param-reassign
    thread.viewers = repliesThreadMap.get(thread.id)!.recentPostCount;
  });

  // Sort threads by viewerType (prioritize viewers over recent replies) and then by count
  allThreads = allThreads.sort((a, b) => {
    if (a.viewerType - b.viewerType !== 0) {
      return a.viewerType - b.viewerType;
    }

    return b.viewers - a.viewers;
  });

  return allThreads.slice(0, 20).map((t) => ({
    ...(t.viewerType === ViewerType.Viewers ? viewerThreadMap : repliesThreadMap).get(t.id),
    viewers:
      t.viewerType !== ViewerType.Viewers
        ? undefined
        : {
            memberCount: t.viewers,
          },
  }));
};

export const popularThreads = async (req: Request, res: Response) => {
  const threads = await popular(req);

  return ResponseStrategy.send(res, { list: threads });
};

export const latest = async (req: Request, excludedSubforums: string[] = []) => {
  // exclude private subforums from the latest threads
  const subforumIds = filterSubforums(
    await getViewableSubforumIdsByUser(req.user?.id, true),
    excludedSubforums
  );

  // exclude hidden threads
  const hiddenThreadIds = await userHiddenThreadIds(req.user?.id);

  // get latest 20 thread IDs from all viewable subforums
  // exclude locked, deleted, or hidden threads
  const latestThreadIds = (
    await Thread.findAll({
      attributes: ['id'],
      where: {
        subforum_id: subforumIds,
        locked: false,
        deletedAt: null,
        id: {
          [Op.notIn]: hiddenThreadIds,
        },
      },
      order: [['created_at', 'desc']],
      limit: 20,
      raw: true,
    })
  ).map((thread) => thread.id);

  return new ThreadRetriever(latestThreadIds, [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_USER,
    ThreadFlag.INCLUDE_TAGS,
  ]).getObjectArray();
};

export const latestThreads = async (req: Request, res: Response) => {
  const threads = await latest(req);
  return ResponseStrategy.send(res, { list: threads });
};

export const store = async (req: Request, res: Response, body: CreateThreadRequest) => {
  const skipPostValidations = await userHasPermissions(
    [`subforum-${body.subforum_id}-post-bypass-validations`],
    req.user?.id
  );

  // validations for a valid post
  if (
    !skipPostValidations &&
    (!validatePostLength(body.content) ||
      !validateThreadTitleLength(body.title) ||
      !validateThreadTagCount(req.body.tagIds))
  ) {
    throw new InternalServerError('Failed validations.');
  }

  if (!validateUserFields(req.user)) {
    throw new UnauthorizedError('Invalid user');
  }

  let thread: Thread;
  try {
    await sequelize.transaction(async (transaction) => {
      thread = await Thread.create(
        {
          ...body,
          user_id: req.user!.id,
          postCount: 0,
        },
        { transaction }
      );

      // Insert thread tags
      if (body.tag_ids && body.tag_ids.length > 0) {
        await ThreadTag.bulkCreate(
          body.tag_ids.map((tag) => ({
            tag_id: tag,
            thread_id: thread!.id,
          })),
          { transaction }
        );
      }

      await postController.store(
        { ...body, thread_id: thread.id },
        req.user!,
        req.ipInfo!,
        undefined,
        req.header('CF-IPCountry'),
        transaction
      );

      // send socket event to only the roles that can view this thread
      const roleIds = await getRoleIdsWithPermissions([`subforum-${body.subforum_id}-view`]);
      await createSocketEvent(
        req.user!.id,
        EventType.THREAD_CREATED,
        thread.id,
        req.app.get('io'),
        {},
        undefined,
        roleIds,
        transaction
      );
    });
  } catch (error) {
    console.error(error);
    throw new InternalServerError('Error creating thread.');
  }

  return (await Thread.findOne({ where: { id: thread!.id } }))!.toJSON();
};

export const storeThread = async (req: Request, res: Response) => {
  try {
    const result = await store(req, res, {
      ...req.body,
      background_url: req.body.backgroundUrl,
      background_type: req.body.backgroundType,
      tag_ids: req.body.tagIds,
    });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    if (exception instanceof InternalServerError) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR);
    } else if (exception instanceof UnauthorizedError) {
      res.status(httpStatus.UNAUTHORIZED);
    } else {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
    }
    res.json({ error: exception.message });
  }
};

export const update = async (id: number, body: UpdateThreadRequest, user: User, io: Server) => {
  const thread = await Thread.findOne({ where: { id } });

  if (!thread) {
    throw new NotFoundError('Thread not found');
  }

  const oldTitle = thread.title;
  const skipPostValidations = await userHasPermissions(
    [`subforum-${thread.subforum_id}-post-bypass-validations`],
    user.id
  );

  if (body.title) {
    if (!skipPostValidations && !validateThreadTitleLength(body.title)) {
      throw new ForbiddenError('Invalid thread title');
    }
  }

  if (thread.locked === false) {
    await knex('Threads').where({ id }).update(body);
  } else if (!skipPostValidations) {
    throw new ForbiddenError("You can't do that, dummy");
  }

  await new ThreadRetriever(id).invalidate();

  if (body.subforum_id) {
    const key = getPopularThreadViewsKey(thread.subforum_id);
    await redis.zrem(key, thread.id);
  }

  const newThread = await Thread.findOne({ where: { id } });

  if (!newThread) {
    throw new NotFoundError('Newly created thread not found.');
  }

  const result = newThread.toJSON();

  if (body.title || body.subforum_id) {
    threadUpdateEvent(
      {
        oldTitle,
        threadId: id,
        userId: user.id,
        body,
      },
      io
    );
  }

  if (body.background_url) {
    const roleIds = await getRoleIdsWithPermissions([`subforum-${newThread.subforum_id}-view`]);

    createEvent(user.id, EventType.THREAD_BACKGROUND_UPDATED, id, io, {}, roleIds);
  }

  return result;
};

export const updateThread = async (req: Request, res: Response) => {
  try {
    const result = await update(
      req.body.id,
      {
        ...req.body,
        background_url: req.body.backgroundUrl,
        background_type: req.body.backgroundType,
      },
      req.user!,
      req.app.get('io')
    );

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    if (err instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: err.message });
    } else {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
    }
  }
};

export const getPostsAndCount = async (req: Request, ratingUsernames = false) => {
  // if no page parameter is provided, default to page 1
  const pageParam: number = Number(req.params.page) || 1;

  // get total posts for this thread
  const postIds = await knex
    .from('Posts as po')
    .select('po.id as id')
    .where('po.thread_id', req.params.id)
    .limit(Number(POSTS_PER_PAGE))
    .offset(Number(POSTS_PER_PAGE) * (pageParam - 1));

  const flags = [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_LAST_POST,
    ThreadFlag.INCLUDE_POST_COUNT,
    ThreadFlag.INCLUDE_USER,
    ThreadFlag.INCLUDE_TAGS,
    ThreadFlag.INCLUDE_SUBFORUM,
    ThreadFlag.INCLUDE_VIWERS,
  ];

  const shallowThread: any = await new ThreadRetriever(req.params.id, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();

  if (!shallowThread) {
    throw new NotFoundError();
  }

  if (
    await userHasPermissions(
      [`subforum-${shallowThread.subforumId}-view-thread-viewers`],
      req.user?.id
    )
  ) {
    flags.push(ThreadFlag.INCLUDE_VIEWER_USERS);
  }

  const thread = (await new ThreadRetriever(req.params.id, flags, {
    userId: req.user?.id,
  }).getSingleObject()) as ThreadWithLastPost;

  if (!thread) {
    throw new NotFoundError();
  }

  if (postIds.length > 0) {
    const postFlags = [
      PostFlag.INCLUDE_LAST_EDITED_USER,
      PostFlag.INCLUDE_POST_RESPONSES,
      PostFlag.INCLUDE_POST_MENTION_USERS,
    ];
    if (ratingUsernames) {
      postFlags.push(PostFlag.RETRIEVE_RATING_USERNAMES);
    }

    const posts = await new PostRetriever(
      postIds.map((post) => post.id),
      postFlags,
      { userId: req.user?.id }
    ).getObjectArray();

    const response: any = {
      ...thread,
      totalPosts: thread.postCount,
      currentPage: pageParam,
      threadBackgroundUrl: thread.backgroundUrl,
      threadBackgroundType: thread.backgroundType,
      posts,
    };

    return response;
  }

  const response = {
    ...thread,
    currentPage: pageParam,
    totalPosts: thread.postCount,
    posts: [],
  };

  return response;
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  try {
    const response = await getPostsAndCount(req, true);
    return ResponseStrategy.send(res, {
      ...response,
      // legacy fields
      unreadPostCount: response.readThread ?? 0,
      readThreadUnreadPosts: response.readThread ?? 0,
      readThreadLastSeen: response.readThread?.lastSeen,
      readThreadLastPostNumber: response.readThread?.lastPostNumber,
      read: response.readThread != null,
      hasRead: response.readThread != null,
      hasSeenNoNewPosts: false,
      isSubscribedTo: response.readThread?.isSubscription,
      firstUnreadId: response.readThread?.firstUnreadId,
      subscribed: response.readThread?.isSubscription,
      subscriptionLastSeen: response.readThread?.lastSeen,
      subscriptionLastPostNumber: response.readThread?.lastPostNumber,
    });
  } catch (error) {
    if (error instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
      return res.json({ error: error.message });
    }
    if (error instanceof NotFoundError) {
      res.status(httpStatus.NOT_FOUND);
      return res.json({ error: error.message });
    }
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const updateTags = async (id: number, body: UpdateThreadTagsRequest) => {
  // delete all existing tags for that thread
  await knex('ThreadTags').delete().where({
    thread_id: id,
  });

  try {
    if (body.tag_ids.length > 0) {
      await knex('ThreadTags').insert(
        body.tag_ids.map((tag) => ({
          tag_id: tag,
          thread_id: id,
        }))
      );
    }
  } catch (error) {
    throw new InternalServerError('Error updating thread tags.');
  }

  return new ThreadTagsRetriever(id).invalidate();
};

export const updateThreadTags = async (req: Request, res: Response) => {
  if (!req.body.threadId || !req.body.tags || req.body.tags.length > 3) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    return res.json({ error: 'Invalid content' });
  }
  try {
    await updateTags(req.body.threadId, { ...req.body, tag_ids: req.body.tags });
  } catch (error) {
    if (error instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
      return res.json({ error: error.message });
    }
    return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ error: error.message });
  }

  res.status(httpStatus.OK);
  return res.json({ message: 'Tags updated.' });
};
