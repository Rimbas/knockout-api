/* eslint-disable import/prefer-default-export */
import { Request, Response } from 'express';

import ResponseStrategy from '../../helpers/responseStrategy';
import SubforumRetriever, { SubforumFlag } from '../../retriever/subforum';

import { getViewableSubforumIdsByUser } from '../../helpers/user';

export const getSubforums = async (hideNsfw: string, userId?: number) => {
  const flags = [
    SubforumFlag.INCLUDE_TOTAL_THREADS,
    SubforumFlag.INCLUDE_TOTAL_POSTS,
    SubforumFlag.INCLUDE_LAST_POST,
  ];

  if (Number(hideNsfw)) {
    flags.push(SubforumFlag.HIDE_NSFW);
  }

  const subforumIds = await getViewableSubforumIdsByUser(userId);

  return new SubforumRetriever(subforumIds, flags, {
    userId,
  }).getObjectArray();
};

export const getAll = async (req: Request, res: Response) => {
  const subforums = await getSubforums(req.query.hideNsfw, req.user?.id);

  const output = subforums.map((subforum) => ({
    id: subforum.id,
    name: subforum.name,
    createdAt: subforum.createdAt,
    updatedAt: subforum.updatedAt,
    description: subforum.description,
    icon: subforum.icon,
    totalThreads: subforum.totalThreads,
    totalPosts: subforum.totalPosts,
    lastPostId: subforum.lastPost?.id,
    lastPost: subforum.lastPost,
  }));

  const responseBody = { list: output };

  return ResponseStrategy.send(res, responseBody);
};
