import { Request, Response } from 'express';
import httpStatus from 'http-status';
import Sequelize, { ScopeOptions } from 'sequelize';
import { Server } from 'socket.io';
import {
  EventType,
  UpdateUserRequest,
  User as UserSchema,
  USERNAME_CHARACTER_LIMIT,
} from 'knockout-schema';
import ms from 'ms';
import { BadRequestError, ForbiddenError, HttpError } from 'routing-controllers';
import ResponseStrategy from '../helpers/responseStrategy';
import UserRetriever, { UserFlag } from '../retriever/user';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import PostRetriever, { PostFlag } from '../retriever/post';
import BanRetriever, { BanFlag } from '../retriever/ban';

import knex from '../services/knex';
import redis from '../services/redisClient';
import { getMentions } from './mentionsController';
import { countOpenReportsQuery } from './moderationController';
import ratingList from '../helpers/ratingList.json';
import errorHandler from '../services/errorHandler';
import { banUser } from './banController';
import RoleRetriever, { RoleFlag } from '../retriever/role';
import {
  demoteUserFromGold,
  getUserPermissionCodes,
  unBanUser,
  userHiddenThreadIds,
} from '../helpers/user';
import { threadsPerPage } from '../constants/pagination';
import { RoleCode } from '../helpers/permissions';
import { createEvent } from './eventLogController';
import { userQueue } from '../jobs';
import { jobInfo } from '../log';
import { UserJob } from '../jobs/processUserJobs';
import { findGoldSubscription } from '../helpers/stripe';
import PreviousUsername from '../models/previousUsername';
import User from '../models/user';
import Thread from '../models/thread';
import Tag from '../models/tag';
import Post from '../models/post';
import { getAlerts } from './readThreadController';

/**
 * Send the loaded user data to the client
 */
export const index = async (req: Request, res: Response) => {
  try {
    if (req.isLoggedIn) {
      res.status(httpStatus.OK);
      res.json({ user: req.user });
    } else {
      res.status(httpStatus.UNAUTHORIZED);
      res.json({ error: 'You are not logged in.' });
    }
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has ocurred. ' });
  }
};

export const checkUsername = async (user, username?: string): Promise<string> => {
  if (user.username) {
    const previousUsername = await PreviousUsername.findOne({
      where: {
        user_id: user.id,
        createdAt: {
          [Sequelize.Op.gt]: new Date(Date.now() - ms('1 year')),
        },
      },
    });
    if (previousUsername) {
      throw new ForbiddenError('You can only change your username once every year.');
    }
  }

  if (!username) {
    return user.username;
  }

  const tentativeUsername = username.trim();

  if (
    tentativeUsername.length < 3 ||
    tentativeUsername.length > USERNAME_CHARACTER_LIMIT ||
    !/^[\w\-\_\s\á\é\ó\ú\ã\ê\ô\ç\!\?\$\#\&]+$/.test(tentativeUsername)
  ) {
    throw new BadRequestError('This username is not valid.');
  }

  const existingUserCheck = await knex
    .select('*')
    .from('Users')
    .where({ username: tentativeUsername })
    .first();

  if (existingUserCheck && existingUserCheck.length !== 0) {
    throw new ForbiddenError('This username has already been used.');
  }

  return tentativeUsername;
};

export const update = async (id: number, body: UpdateUserRequest): Promise<UserSchema> => {
  const user = await knex.select('*').from('Users').where({ id }).first();

  // shitty way of making sure users can only change what we want
  let fields: {
    username?: string;
    avatar_url?: string;
    background_url?: string;
    pronouns?: string;
    showOnlineStatus?: boolean;
    disableIncomingMessages?: boolean;
  } = {};
  if (body.username && user.username !== body.username) {
    const username = await checkUsername(user, body.username);
    fields = { ...fields, username };
  }
  if (body.avatarUrl && (body.avatarUrl === `${user.id}.webp` || body.avatarUrl === 'none.webp')) {
    fields = { ...fields, avatar_url: body.avatarUrl };
  }
  if (body.backgroundUrl && body.backgroundUrl === `${user.id}-bg.webp`) {
    fields = { ...fields, background_url: body.backgroundUrl };
  }
  let pronounsChanged = false;
  if (body.pronouns !== user.pronouns) {
    fields.pronouns = body.pronouns?.toLowerCase();
    pronounsChanged = true;
  }

  if (body.showOnlineStatus !== user.show_online_status) {
    fields = { ...fields, showOnlineStatus: body.showOnlineStatus };
  }

  if (body.disableIncomingMessages !== user.disable_incoming_messages) {
    fields = { ...fields, disableIncomingMessages: body.disableIncomingMessages };
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { username, avatar_url, background_url, showOnlineStatus, disableIncomingMessages } =
    fields;

  if (
    !username &&
    !avatar_url &&
    !background_url &&
    !pronounsChanged &&
    showOnlineStatus === null &&
    disableIncomingMessages === null
  ) {
    throw new Error('Nothing changed.');
  }

  const userModel = await User.findOne({ where: { id } });
  await userModel!.update({ ...fields });
  await userModel!.save();

  const retriever = new UserRetriever(id);
  retriever.invalidate();

  return retriever.getSingleObject();
};

export const updateUser = async (req: Request, res: Response) => {
  try {
    const newUser = await update(req.user!.id, req.body);

    res.status(httpStatus.OK);
    return res.json(newUser);
  } catch (err) {
    if (!(err instanceof HttpError)) {
      return errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res);
    }

    if (err instanceof BadRequestError) {
      res.status(httpStatus.BAD_REQUEST);
    }
    if (err instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
    }
    return res.json({ error: err.message });
  }
};

export const authCheck = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on userController.authCheck');
    }
    if (req.user == null || typeof req.user?.id === 'undefined') {
      throw new Error('Invalid user.');
    }

    res.status(httpStatus.OK);
    return res.send('OK');
  } catch (err) {
    return errorHandler.respondWithError(httpStatus.OK, err, res, 'Your authcheck failed');
  }
};

export const getThreads = async (req: Request, res: Response) => {
  const userId = req.params.id;
  const hideNsfw = req.query.hideNsfw || false;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const permissionCodes = await getUserPermissionCodes(req.user?.id);
  const hiddenThreadIds = await userHiddenThreadIds(req.user?.id);
  const threadScopes: Sequelize.ScopeOptions[] = [
    { method: ['byUser', req.user?.id, permissionCodes] },
  ];
  const whereCondition = {
    id: {
      [Sequelize.Op.notIn]: hiddenThreadIds,
    },
    user_id: userId,
    '$Tags.id$': {
      [Sequelize.Op.or]: [
        null,
        !hideNsfw,
        {
          [Sequelize.Op.ne]: 1,
        },
      ],
    },
  };

  const offset = Number(req.params.page) * threadsPerPage - threadsPerPage;

  const query = await Thread.scope(threadScopes).findAndCountAll({
    attributes: ['id'],
    limit: threadsPerPage,
    offset,
    subQuery: false,
    where: whereCondition,
    include: {
      attributes: ['id'],
      model: Tag,
      required: false,
    },
    order: [['created_at', 'desc']],
  });

  const threadIds = query.rows.map((thread) => thread.id);

  const flags = [
    ThreadFlag.RETRIEVE_SHALLOW,
    ThreadFlag.INCLUDE_LAST_POST,
    ThreadFlag.INCLUDE_POST_COUNT,
  ];

  if (hideNsfw) {
    flags.push(ThreadFlag.INCLUDE_TAGS);
  }

  const threads = await new ThreadRetriever(threadIds, flags, {
    userId: req.user?.id,
  }).getObjectArray();

  return ResponseStrategy.send(res, {
    totalThreads: query.count,
    currentPage: Number(req.params.page) || 1,
    threads,
  });
};

export const getPosts = async (req: Request, res: Response) => {
  const postsPerPage = 40;
  const userId = req.params.id;
  const offset = req.params.page ? Number(req.params.page) * postsPerPage - postsPerPage : 0;
  const hideNsfw = req.query.hideNsfw || false;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const permissionCodes = await getUserPermissionCodes(req.user?.id);
  const hiddenThreadIds = await userHiddenThreadIds(req.user?.id);
  const postScopes: ScopeOptions[] = [{ method: ['byUser', userId, permissionCodes, hideNsfw] }];

  const query = await Post.scope(postScopes).findAll({
    attributes: ['id'],
    where: {
      thread_id: {
        [Sequelize.Op.notIn]: hiddenThreadIds,
      },
    },
    limit: postsPerPage,
    offset,
    subQuery: false,
    order: [['id', 'desc']],
  });

  const postIds = query.map((post) => post.id);
  const allPosts = await knex
    .from('Posts as p')
    .count('p.id as count')
    .where('user_id', userId)
    .first();

  const posts = await new PostRetriever(
    postIds,
    [PostFlag.INCLUDE_THREAD, PostFlag.INCLUDE_POST_MENTION_USERS],
    {
      userId: req.user?.id,
    }
  ).getObjectArray();

  return ResponseStrategy.send(res, {
    totalPosts: allPosts!['count'],
    currentPage: Number(req.params.page) || 1,
    posts,
  });
};

export const getBans = async (req: Request, res: Response) => {
  const userId = req.params.id;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const query = await knex
    .from('Bans as b')
    .select('b.id')
    .where('user_id', userId)
    .orderBy('b.id', 'desc');

  const banIds = query.map((ban) => ban.id);

  const bans = await new BanRetriever(banIds, [
    BanFlag.INCLUDE_POST,
    BanFlag.INCLUDE_THREAD,
  ]).getObjectArray();
  return ResponseStrategy.send(res, bans);
};

export const show = async (req: Request, res: Response) => {
  const userId = req.params.id;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const user = await new UserRetriever(Number(userId), [
    UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
    UserFlag.INCLUDE_ONLINE,
  ]).getSingleObject();
  return ResponseStrategy.send(res, user);
};

const manuallyUnbanUser = async (userId: number, banId: number, banExpiresAt: Date) => {
  // log an error here since our jobs should have unbanned the user
  const logMessage = `[manuallyUnbanUser] User ${userId} was not unbanned by the UnbanUser job.
    The latest banId is ${banId} and the ban expiration date was ${banExpiresAt}.
    Manually unbanning user. Current time: ${new Date()}`;
  console.error(logMessage);
  await unBanUser(userId);
};

export const syncData = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login for user data sync.');
    }
    if (!req.user) {
      throw new Error('Invalid user.');
    }

    interface UserDataSyncResponse {
      id: number;
      username?: string;
      isBanned?: boolean;
      createdAt: Date;
      banInfo?: {
        banMessage: string;
        expiresAt: Date;
        threadId: number;
        postContent: string;
        postId: number;
      };
      avatarUrl?: string;
      backgroundUrl?: string;
      subscriptions?: Array<object>;
      subscriptionIds?: Array<number>;
      mentions?: Array<object>;
      reports?: number;
      role?: object;
    }
    const userDataSyncResponse: UserDataSyncResponse = {
      id: req.user.id,
      username: req.user.username,
      isBanned: req.user.isBanned,
      avatarUrl: req.user.avatar_url,
      backgroundUrl: req.user.background_url,
      createdAt: req.user.createdAt,
    };

    if (req.user.isBanned) {
      const bans = await knex('Bans')
        .select(
          'id',
          'ban_reason',
          'expires_at',
          'post_id',
          'Posts.thread_id',
          knex.raw('(SUBSTRING(Posts.content, 1, 250)) as postContent')
        )
        .leftJoin('Posts', 'Bans.post_id', 'Posts.id')
        .where({
          'Bans.user_id': req.user?.id,
        })
        .orderBy('Bans.created_at', 'desc')
        .limit(1);

      const {
        ban_reason: banMessage,
        expires_at: expiresAt,
        thread_id: threadId,
        post_id: postId,
        postContent,
      } = bans[0];

      if (banMessage) {
        userDataSyncResponse.banInfo = {
          banMessage,
          expiresAt,
          threadId,
          postContent,
          postId,
        };
      }

      // if the user has no active bans (any bans with an expiration date in the future)
      // but the user is still banned, unban them
      // check if any active bans exist
      const activeBanCount = await knex('Bans')
        .count('id as count')
        .where('user_id', req.user?.id)
        .andWhere('expires_at', '>', new Date())
        .first();

      if (!activeBanCount || Number(activeBanCount.count) === 0) {
        await manuallyUnbanUser(req.user?.id, bans[0].id, bans[0].expiresAt);
      }
    }

    const userRetriever = new UserRetriever(req.user?.id, [
      UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION,
    ]);
    const cachedUser = await userRetriever.getSingleObject();

    // demote gold users who still have the gold role,
    // but their donation expiration expired and they dont have an active subscription
    const donationUpgradeExpired =
      cachedUser.donationUpgradeExpiresAt &&
      new Date(cachedUser.donationUpgradeExpiresAt) < new Date();
    const userIsGold = cachedUser.role?.code === RoleCode.PAID_GOLD_USER;

    if (userIsGold && donationUpgradeExpired) {
      const currentSubscription = await findGoldSubscription(req.user?.id);
      if (currentSubscription?.status !== 'active') {
        await demoteUserFromGold(req.user?.id);
        createEvent(req.user?.id, EventType.GOLD_LOST, req.user?.id, req.app.get('io'));
      }
    }

    //
    // cache invalidation for bans
    //
    if (cachedUser.banned !== req.user.isBanned) {
      await userRetriever.invalidate();
    }

    if (!req.user.isBanned) {
      // get mentions
      userDataSyncResponse.mentions = await getMentions(req.user?.id);

      // get alerts
      const alerts = await getAlerts(req.user?.id, 1);
      userDataSyncResponse.subscriptions = alerts.alerts;
      userDataSyncResponse.subscriptionIds = alerts.ids;

      // add reports if the user has the correct permission
      const userRole = await new RoleRetriever(cachedUser.role?.id, [
        RoleFlag.INCLUDE_PERMISSION_CODES,
      ]).getSingleObject();
      if (userRole?.permissionCodes?.includes('report-view')) {
        userDataSyncResponse.reports = Number(await countOpenReportsQuery());
      }
    }

    // update online timestamp
    await User.update({ lastOnlineAt: new Date() }, { where: { id: req.user?.id } });

    // if cached user is offline and their have showOnlineStatus set, update to be online
    if (!cachedUser.online && (cachedUser as any).showOnlineStatus) {
      userRetriever.invalidate();
    }

    // add role to response
    userDataSyncResponse.role = cachedUser.role;

    res.status(httpStatus.OK);
    return res.json(userDataSyncResponse);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.OK, error, res, 'Your authcheck failed');
  }
};

export const getTopRatings = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;

    const ratingsHiddenForUser = await knex('Users')
      .select('hide_profile_ratings')
      .where({ id: userId, deleted_at: null });

    if (!ratingsHiddenForUser[0] || ratingsHiddenForUser[0].hide_profile_ratings === 1) {
      res.status(httpStatus.OK);
      return res.json([]);
    }

    const cached = await redis.get(`top-ratings-${userId}`);

    if (cached) {
      res.status(httpStatus.OK);
      return res.send(cached);
    }

    const ratingsLookup = await knex('Posts')
      .select('Ratings.rating_id')
      .count('Ratings.rating_id as count')
      .rightJoin('Ratings', 'Ratings.post_id', 'Posts.id')
      .whereRaw(
        `
      Posts.user_id = ?
    `,
        [userId]
      )
      .groupBy('rating_id');

    const ratingsNamed = ratingsLookup.map((val) => {
      if (ratingList[val.rating_id]) {
        return {
          name: ratingList[val.rating_id].short,
          count: val.count,
        };
      }
      return {};
    });

    redis.setex(`top-ratings-${userId}`, 168000, JSON.stringify(ratingsNamed));

    res.status(httpStatus.OK);
    return res.json(ratingsNamed);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, error, res);
  }
};

export const updateProfileRatingsDisplay = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const hide = req.body.hideProfileRatings === true;
    const boolValue = hide ? 1 : 0;

    await knex('Users').update({ hide_profile_ratings: boolValue }).where({ id: req.user?.id });

    res.status(httpStatus.OK);
    res.json({ message: 'Profile ratings preference saved.' });
  } catch (err) {
    errorHandler.respondWithError(
      httpStatus.BAD_REQUEST,
      err,
      res,
      'You have provided invalid data.'
    );
  }
};

export const getProfileRatingsDisplay = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const ratingsHiddenForUser = await knex('Users')
      .select('hide_profile_ratings')
      .where({ id: req.user?.id });

    if (!ratingsHiddenForUser[0]) {
      throw new Error('Could not find data for user.');
    }

    res.status(httpStatus.OK);
    res.json({ ratingsHiddenForUser: ratingsHiddenForUser[0].hide_profile_ratings === 1 });
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res, 'Could not find data for user');
  }
};

export const wipeAccount = async (userId: number, reason: string, wipedBy: number, io: Server) => {
  await banUser(0, userId, reason, wipedBy, io, undefined, true);

  jobInfo(`Enqueuing user wipe job`);
  await userQueue.add(
    { type: UserJob.WIPE_USER, userId },
    { attempts: 3, backoff: { type: 'exponential', delay: 30000 } }
  );
  return { message: 'User wipe in progress' };
};

export const archive = async (userId: number) => {
  try {
    jobInfo(`Enqueuing user deletion job`);
    await userQueue.add(
      { type: UserJob.DELETE_USER, userId },
      {
        attempts: 3,
        backoff: { type: 'exponential', delay: 30000 },
        jobId: `delete-user-${userId}`,
      }
    );
    return { message: 'User account deletion is now in progress.' };
  } catch (err) {
    throw new BadRequestError('Could not find data for user');
  }
};

export const deleteAccount = async (req: Request, res: Response) => {
  try {
    await archive(req.params.id);
    res.status(httpStatus.OK);
    res.json({ message: 'User account deletion is now in progress.' });
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res, 'Could not find data for user');
  }
};
