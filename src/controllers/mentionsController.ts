import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { Op } from 'sequelize';
import MentionRetriever from '../retriever/mention';
import errorHandler from '../services/errorHandler';
import Mention from '../models/mention';

export const markMentionInactive = async (postIds: number[], userId: number) =>
  Mention.update({ inactive: true }, { where: { mentionsUser: userId, postId: postIds } });

export const markAsRead = async (req: Request, res: Response) => {
  try {
    if (!req.body.postIds) {
      throw new Error('No postIds supplied.');
    }

    const postIds = Array.isArray(req.body.postIds) ? req.body.postIds : [req.body.postIds];

    await markMentionInactive(postIds, req.user!.id);

    res.status(httpStatus.OK);
    res.json({ message: 'Posts marked as read!' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getMentions = async (userId?: number) => {
  if (!userId) return [];

  const mentionIds = (
    await Mention.findAll({
      attributes: ['id'],
      where: {
        mentionsUser: userId,
        inactive: { [Op.or]: [null, false] },
      },
      limit: 20,
    })
  ).map((mention) => mention.id);

  return new MentionRetriever(mentionIds).getObjectArray();
};

export const index = async (req: Request, res: Response) => {
  try {
    const results = await getMentions(req.user?.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
