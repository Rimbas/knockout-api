/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-unused-vars */

import passport, { Profile } from 'passport';
import { Request } from 'express';
import { loginWithExternalId, Provider } from './common';
import * as config from '../../../config/server';
/**
 * @param isOpenID Whether the provider is OpenID-based (like Steam) and thus
 * has a different callback format (using req, id, profile, done as parameters)
 */
function createLoginCallback(provider: Provider, isOpenID?: boolean) {
  // eslint-disable-next-line func-names
  const loginCallback = async function (
    req: Request,
    accessToken: string,
    refreshToken: string | undefined,
    profile: Profile,
    done: () => void
  ) {
    const { res } = req;
    const ip = req.ipInfo;
    const { redirect } = req.session as any;

    // if an authenticated user is making this request, get the user and use this login method on their account
    if (accessToken) {
      await loginWithExternalId(
        provider,
        profile.id,
        res!,
        ip!,
        req.cookies?.knockoutJwt,
        redirect
      );
    } else {
      res!.status(401).json({
        error: 'Unauthorized',
        message: 'Authentication provider did not provide an access token',
      });
    }
    // Do not call "done" because it will make Passport handle the user token, but we want to validate it manually
  };
  if (isOpenID) {
    // Skip the refreshToken when calling the callback
    return (req: Request, id: string, profile: passport.Profile, done: () => void) =>
      loginCallback(req, id, undefined, profile, done);
  }
  return loginCallback;
}

// Twitter
if (config.TWITTER_CONSUMER_KEY) {
  const Twitter = require('passport-twitter');
  const callback = createLoginCallback(Provider.TWITTER);
  passport.use(
    new Twitter.Strategy(
      {
        userAuthorizationURL: 'https://api.twitter.com/oauth/authenticate?force_login=true',
        consumerKey: config.TWITTER_CONSUMER_KEY,
        consumerSecret: config.TWITTER_CONSUMER_SECRET,
        callbackURL: '/auth/twitter/callback',
        passReqToCallback: true,
      },
      callback
    )
  );
}

// GitHub
if (config.GITHUB_CLIENT_ID) {
  const GitHub = require('passport-github');
  const callback = createLoginCallback(Provider.GITHUB);
  passport.use(
    new GitHub.Strategy(
      {
        authorizationURL: 'https://github.com/login/oauth/authorize?login=',
        clientID: config.GITHUB_CLIENT_ID,
        clientSecret: config.GITHUB_CLIENT_SECRET,
        callbackURL: '/auth/github/callback',
        passReqToCallback: true,
      },
      callback
    )
  );
}

// Steam
if (config.STEAM_REALM) {
  const Steam = require('passport-steam');
  const callback = createLoginCallback(Provider.STEAM, true);
  passport.use(
    new Steam.Strategy(
      {
        realm: config.STEAM_REALM,
        apiKey: config.STEAM_API_KEY,
        returnURL: `${config.STEAM_REALM}/auth/steam/callback`,
        passReqToCallback: true,
      },
      callback
    )
  );
}

// Google
if (config.GOOGLE_CLIENT_ID) {
  const Google = require('passport-google-oauth20');
  const callback = createLoginCallback(Provider.GOOGLE);
  passport.use(
    new Google.Strategy(
      {
        clientID: config.GOOGLE_CLIENT_ID,
        clientSecret: config.GOOGLE_CLIENT_SECRET,
        callbackURL: `${config.STEAM_REALM}/auth/google/callback`,
        scope: ['profile'],
        passReqToCallback: true,
        proxy: true,
      },
      callback
    )
  );
}

export default {
  twitter: passport.authenticate('twitter'),
  github: passport.authenticate('github'),
  steam: passport.authenticate('steam', { prompt: 'consent' }),
  google: passport.authenticate('google', { prompt: 'select_account' }),
};
