import { Request, Response } from 'express';
import httpStatus from 'http-status';

import redis from '../services/redisClient';
import errorHandler from '../services/errorHandler';
import ThreadAd from '../models/threadAd';

const THREAD_ADS_LIST_KEY = 'thread-ads-list';
const THREAD_ADS_LIST_TTL = 86400;

export const getThreadAds = async () =>
  ThreadAd.findAll({
    attributes: ['id', 'description', 'query', 'imageUrl'],
    raw: true,
  });

export const index = async (req: Request, res: Response) => {
  try {
    const cachedData = await redis.get(THREAD_ADS_LIST_KEY);

    if (cachedData) {
      res.status(httpStatus.OK);
      return res.json(JSON.parse(cachedData));
    }

    const ads = await getThreadAds();

    redis.setex(THREAD_ADS_LIST_KEY, THREAD_ADS_LIST_TTL, JSON.stringify(ads));

    res.status(httpStatus.OK);
    return res.json(ads);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const random = async (req: Request, res: Response) => {
  try {
    const cachedData = await redis.get(THREAD_ADS_LIST_KEY);

    if (cachedData) {
      const result = JSON.parse(cachedData);
      const item = result[Math.floor(Math.random() * result.length)];

      res.status(httpStatus.OK);
      return res.json(item);
    }

    const ads = await getThreadAds();

    redis.setex(THREAD_ADS_LIST_KEY, THREAD_ADS_LIST_TTL, JSON.stringify(ads));

    res.status(httpStatus.OK);
    return res.json(ads);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
