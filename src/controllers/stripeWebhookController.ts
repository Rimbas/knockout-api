import { Request, Response } from 'express';
import StripeWebhookHandler from '../services/stripeWebhookHandler';

// eslint-disable-next-line import/prefer-default-export
export const post = async (req: Request, res: Response) => {
  try {
    await new StripeWebhookHandler().handleWebhookEvent(
      req.rawBody!,
      req.headers['stripe-signature']!.toString(),
      req.app.get('io')
    );
  } catch (err) {
    console.error(err);
    res.status(400).send(`Stripe webhook error: ${err.message}`);
    return;
  }

  // Return a 200 response to acknowledge receipt of the event
  res.send();
};
