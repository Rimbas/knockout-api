import { Request, Response } from 'express';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import passport from './auth/passport';
import { JWT_SECRET as jwtSecret, APP_JWT_SECRET as appJwtSecret } from '../../config/server';

export { passport };

export function requestToken(req: Request, res: Response) {
  if (!req.query.key) {
    res.sendStatus(httpStatus.BAD_REQUEST);
  }
  try {
    const apiKey: { iat: number; exp: number; aud: string } = jwt.verify(req.query.key, jwtSecret, {
      algorithms: ['HS256'],
    }) as any;

    const authorization: { id: number; iat: number; exp: number; aud: string } = jwt.verify(
      req.body.authorization,
      jwtSecret,
      {
        algorithms: ['HS256'],
        audience: apiKey.aud,
      }
    ) as any;

    const token = jwt.sign({ id: authorization.id }, appJwtSecret, {
      algorithm: 'HS256',
      expiresIn: '1 week',
      audience: apiKey.aud,
    });

    res.status(httpStatus.OK).json({ token });
  } catch (error) {
    res.sendStatus(httpStatus.UNAUTHORIZED);
  }
}

export function logout(req: Request, res: Response) {
  res.clearCookie('knockoutJwt');
  res.sendStatus(httpStatus.OK);
}

export function finish(req: Request, res: Response) {
  res.status(httpStatus.OK).send(`<script src="/static/scripts/authRedirectHandler.js"></script>`);
}
