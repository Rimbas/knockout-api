import bodyParser from 'body-parser';
import { NextFunction, Request } from 'express';
import { ServerResponse } from 'http';

// Adds the rawBody to the request object, used for Stripe webhook authentication
const rawBodyInjector = (req: Request, _res: ServerResponse, buf: Buffer, encoding: any) => {
  if (buf?.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
};

export default (req: Request, res: ServerResponse, next: NextFunction): void => {
  if (req.originalUrl === '/stripeWebhooks') {
    // on the stripe webhook route, process the request with our raw body injector
    bodyParser.json({ verify: rawBodyInjector })(req, res, next);
  } else {
    next();
  }
};
