/* eslint-disable class-methods-use-this */
import httpStatus from 'http-status';
import {
  BadRequestError,
  ExpressErrorMiddlewareInterface,
  HttpError,
  Middleware,
} from 'routing-controllers';
import errorHandler from '../services/errorHandler';

const env = process.env.NODE_ENV || 'development';

@Middleware({ type: 'after' })
export default class ErrorHandler implements ExpressErrorMiddlewareInterface {
  error(error: any, request: any, response: any): void {
    if (!response.headersSent && (error instanceof BadRequestError || error instanceof HttpError)) {
      response.status(error.httpCode);
      return response.json({
        name: error.name,
        stack: env === 'development' ? error.stack : undefined,
        message: error.message,
        errors: ({ ...error } as any).errors,
      });
    }
    return errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, error, response);
  }
}
