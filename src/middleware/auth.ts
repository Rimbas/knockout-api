import { Handler, NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import ms from 'ms';
import cookie from 'cookie';
import { JWT_SECRET as jwtSecret, APP_JWT_SECRET as appJwtSecret } from '../../config/server';
import { generateToken, updateToken } from '../controllers/auth/common';

import UserRetriever from '../retriever/user';
import { DELETED_USER_NAME } from '../constants/deletedUser';

function unauthorized(req: Request, res: Response, message?: string) {
  res
    .status(httpStatus.UNAUTHORIZED)
    .send({ message: message || 'Invalid credentials. Please log out and try again.' });
}

function authenticationWithParams({ optional, socket }: { optional: boolean; socket?: boolean }) {
  const authentication = async (req: Request, res: Response, next: NextFunction) => {
    const cookies = socket ? cookie.parse((req.headers.cookie as string) || '') : req.cookies;
    const knockoutJwt = cookies ? cookies.knockoutJwt : undefined;
    const appJwt = req.headers.authorization;

    let token: { id: number; iat: number; exp: number };

    if (!knockoutJwt && !appJwt) {
      req.isLoggedIn = false;
      if (!socket) res.clearCookie('knockoutJwt');
      if (optional) {
        return next();
      }
      return unauthorized(req, res, 'Missing credentials. Please log in.');
    }

    try {
      if (req.query?.key) {
        const apiKey: { iat: number; exp: number; aud: string } = jwt.verify(
          req.query.key,
          jwtSecret,
          {
            algorithms: ['HS256'],
          }
        ) as any;
        // @ts-ignore
        token = jwt.verify(appJwt, appJwtSecret, {
          algorithms: ['HS256'],
          audience: apiKey.aud,
        });
      } else {
        // @ts-ignore
        token = jwt.verify(knockoutJwt, jwtSecret, { algorithms: ['HS256'] });
      }
    } catch (error) {
      // if error is not a JsonWebTokenError, log it
      if (error.name !== 'JsonWebTokenError') {
        console.error(`JWT validation error: ${error}`);
      }
      req.isLoggedIn = false;
      if (socket) {
        return next();
      }
      res.clearCookie('knockoutJwt');
      return unauthorized(req, res, 'Invalid credentials. Try logging out and back in.');
    }

    try {
      // Load the logged in user
      const user = await new UserRetriever(token.id).getSingleObject();

      // Add user info to req.user for later use
      req.user = {
        id: user.id,
        username: user.username,
        role_id: user.role.id,
        avatar_url: user.avatarUrl,
        background_url: user.backgroundUrl,
        isBanned: user.banned,
        title: user.title,
        createdAt: new Date(user.createdAt),
      };

      if (user.username === DELETED_USER_NAME) {
        throw new Error('User is deleted');
      }

      // Refresh the token if it's more than one day old
      if (token.exp && Date.now() > token.iat * 1000 + ms('1 day')) {
        const newToken = generateToken(user);
        if (!socket) updateToken(res, newToken);
      }

      if (res.set) {
        // Authenticated requests should not be stored in the public cache
        res.set('Cache-Control', 'private');
      }

      // Continue onto next route handler
      req.isLoggedIn = true;
      return next();
    } catch (error) {
      console.error(`User validation error: ${error}`);
      req.isLoggedIn = false;
      if (socket) return next();
      res.clearCookie('knockoutJwt');
      return unauthorized(req, res);
    }
  };
  return authentication;
}

interface AuthMiddleware {
  (req: Request, res: Response, next: NextFunction): Promise<void>;
  required: Handler;
  optional: Handler;
  socket: Handler;
}

function createAuthMiddleware(): AuthMiddleware {
  const auth = authenticationWithParams({ optional: false });
  // TypeScript *really* doesn't like the fact that we have to assign
  // properties to a function after it's already assigned to a variable,
  // so we bypass it with Object.assign, which is not type-checked
  Object.assign(auth, {
    required: authenticationWithParams({ optional: false }),
    optional: authenticationWithParams({ optional: true }),
    socket: authenticationWithParams({ optional: true, socket: true }),
  });
  return auth as AuthMiddleware;
}

// eslint-disable-next-line import/prefer-default-export
export const authentication = createAuthMiddleware();
