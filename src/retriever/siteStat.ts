import AbstractRetriever from './abstractRetriever';
import SiteStat from '../models/siteStat';

const SELECTED_ATTRIBUTES = [
  'id',
  'fromDate',
  'toDate',
  'totalActiveUsers',
  'totalBans',
  'totalNewUsers',
  'totalPosts',
  'totalReports',
  'totalThreads',
  'createdAt',
];

export default class SiteStatRetriever extends AbstractRetriever {
  protected cacheLifetime = 604800;

  protected cachePrefix: string = 'siteStat';

  protected async query(ids: Array<number>) {
    return SiteStat.findAll({
      raw: true,
      attributes: SELECTED_ATTRIBUTES,
      where: { id: ids },
    });
  }

  protected format(data) {
    return data;
  }
}
