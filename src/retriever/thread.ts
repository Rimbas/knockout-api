/* eslint-disable import/no-cycle */
import Sequelize from 'sequelize';
import { Rating, Thread } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import UserRetriever from './user';
import PostRetriever, { PostFlag } from './post';
import knex from '../services/knex';
import PostRatingRetriever, { PostRatingFlag } from './postRating';
import SubforumRetriever from './subforum';
import { getGuestCountKey, getMemberCountKey, getViewersPipeline } from '../handlers/threadHandler';
import redis from '../services/redisClient';

import ThreadTagsRetriever from './threadTags';
import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';
import Post from '../models/post';
import ReadThread from '../models/readThread';
import ThreadModel from '../models/thread';

export enum ThreadFlag {
  INCLUDE_SUBFORUM,
  RETRIEVE_SHALLOW,
  INCLUDE_USER,
  INCLUDE_LAST_POST,
  INCLUDE_POST_COUNT,
  EXCLUDE_READ_THREADS,
  INCLUDE_RECENT_POSTS,
  INCLUDE_TAGS,
  INCLUDE_VIWERS,
  INCLUDE_VIEWER_USERS,
}

export default class ThreadRetriever extends AbstractRetriever<Thread> {
  protected cachePrefix: string = 'thread';

  private lastPostCachePrefix: string = 'threadPosts';

  protected recentPostCachePrefix: string = 'threadRecentPosts';

  protected postCountCachePrefix: string = 'threadPostCount;';

  protected async query(ids: Array<number>) {
    return ThreadModel.findAll({
      where: {
        id: ids,
      },
      transaction: this.args['transaction'],
    });
  }

  private async getThreadTags(threads: Thread[]) {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_TAGS))
      return new Map();

    const threadIds = threads.map((thread) => thread.id).filter((id) => id !== null);

    return new ThreadTagsRetriever(threadIds).get();
  }

  private async getFirstPostRatings(threads: Thread[]) {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW)) return {};
    const ids = threads.map((thread) => thread.id);

    const firstPosts = await knex('Posts')
      .select('Posts.id', 'Posts.thread_id')
      .where('Posts.thread_post_number', 1)
      .whereIn('Posts.thread_id', ids);

    const postIds: number[] = firstPosts.map((el) => el.id);

    const postThreadDict = firstPosts.reduce((acc, val) => {
      acc[val.id] = val.thread_id;
      return acc;
    }, {});

    const firstPostRatings = (await new PostRatingRetriever(postIds, [
      PostRatingFlag.EXCLUDE_USERS,
    ]).get()) as Map<number, Rating[]>;

    const threadTopRatings = {};

    postIds.forEach((postId) => {
      const threadId = postThreadDict[postId];
      firstPostRatings.get(postId)?.forEach((rating) => {
        if (
          (!threadTopRatings[threadId] || threadTopRatings[threadId].count < rating.count) &&
          rating.count >= 10
        ) {
          threadTopRatings[threadId] = rating;
        }
      });
    });

    return threadTopRatings;
  }

  private async getSubforums(threads: Thread[]) {
    if (!this.hasFlag(ThreadFlag.INCLUDE_SUBFORUM)) return new Map();
    const subforumIds = threads.map((thread) => thread.subforumId).filter((id) => id !== null);

    return new SubforumRetriever(subforumIds).get();
  }

  private async getPostCounts() {
    if (!this.hasFlag(ThreadFlag.INCLUDE_POST_COUNT)) return {};

    const cachedPostCounts = await this.cacheGet(this.postCountCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedPostCounts);

    const postCounts = await Post.findAll({
      attributes: [
        'thread_id',
        [Sequelize.fn('MAX', Sequelize.col('Post.thread_post_number')), 'postCount'],
      ],
      where: { thread_id: uncachedIds },
      group: ['thread_id'],
    });

    const result = postCounts.reduce((list, count) => {
      const threadId = Number(count.thread_id);
      list[threadId] = { postCount: (count.dataValues as any).postCount };
      this.cacheSet(this.postCountCachePrefix, threadId, list[threadId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedPostCounts.get(id) !== null) result[id] = cachedPostCounts.get(id);
    });

    return result;
  }

  private async getLastPosts() {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_LAST_POST))
      return {};

    const cachedLastPosts = await this.cacheGet(this.lastPostCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedLastPosts);

    let lastPosts: any[] = [];

    if (uncachedIds.length > 0) {
      lastPosts = await knex.raw(
        `SELECT
            Posts.thread_id as threadId, Posts.id as postId
          FROM (
            SELECT MAX(Posts.id) maxPostId
            FROM Posts
            WHERE Posts.thread_id IN (${uncachedIds})
            GROUP BY Posts.thread_id
          ) AS latest_posts
          INNER JOIN Posts
            ON Posts.id = latest_posts.maxPostId 
          GROUP BY Posts.thread_id`
      );

      lastPosts = lastPosts?.length ? lastPosts[0] : [];
    }

    const postIds = lastPosts.map((post) => post?.postId).filter((post) => post !== null);
    const postRetriever = new PostRetriever(postIds, [
      PostFlag.RETRIEVE_SHALLOW,
      PostFlag.INCLUDE_USER,
      PostFlag.TRUNCATE_CONTENT,
    ]);
    const rawPosts = await postRetriever.get();

    const result = lastPosts.reduce((list, post) => {
      list[post.threadId] = {
        lastPost: { ...rawPosts.get(post.postId), thread: post.threadId },
      };
      this.cacheSet(this.lastPostCachePrefix, post.threadId, list[post.threadId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedLastPosts.get(id) !== null) result[id] = cachedLastPosts.get(id);
    });
    return result;
  }

  private async getRecentPosts() {
    if (!this.hasFlag(ThreadFlag.INCLUDE_RECENT_POSTS)) return {};
    const cachedRecentPosts = await this.cacheGet(this.recentPostCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedRecentPosts);

    const postCounts = await knex
      .from('Posts as p')
      .select('p.thread_id as id')
      .count('id as threadRecentPostCount')
      .whereIn('p.thread_id', uncachedIds)
      .whereRaw('p.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY)')
      .groupBy('p.thread_id');

    const result = postCounts.reduce((list, count) => {
      list[count.id] = count.threadRecentPostCount;
      this.cacheSet(this.recentPostCachePrefix, count.id, list[count.id]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedRecentPosts.get(id) !== null) result[id] = cachedRecentPosts.get(id);
    });

    return result;
  }

  private async getReadThreads(
    threads: Thread[],
    userId: number
  ): Promise<Map<number, ReadThread>> {
    if (userId == null || this.hasFlag(ThreadFlag.EXCLUDE_READ_THREADS)) return new Map();

    const threadIds = threads.map((thread) => thread.id);

    const readThreads = await ReadThread.findAll({
      attributes: ['thread_id', 'lastPostNumber', 'lastSeen', 'isSubscription', 'user_id'],
      where: {
        user_id: userId,
        thread_id: threadIds,
      },
    });

    const output = new Map<number, ReadThread>();

    readThreads.forEach((readThread) => {
      output.set(readThread.thread_id, readThread);
    });
    return output;
  }

  private async getReadThreadFirstUnreadId(readThread, postCount) {
    if (postCount === readThread.lastPostNumber) {
      return -1;
    }

    if (readThread.lastPostNumber) {
      return (
        (
          await knex('Posts')
            .select('id as firstUnreadId')
            .first()
            .where('thread_id', readThread.thread_id)
            .where('thread_post_number', readThread.lastPostNumber + 1)
            .limit(1)
        )?.firstUnreadId || -1
      );
    }

    const result = await knex('Posts')
      .select('id as firstUnreadId', 'thread_post_number')
      .first()
      .where('thread_id', readThread.thread_id)
      .where('created_at', '>', readThread.lastSeen)
      .orderBy('created_at')
      .limit(1);

    if (!result) {
      await ReadThread.update(
        {
          lastPostNumber: postCount,
        },
        {
          where: {
            user_id: this.args['userId'],
            thread_id: readThread.thread_id,
          },
        }
      );
      return -1;
    }

    await ReadThread.update(
      {
        lastPostNumber: result.thread_post_number - 1,
      },
      {
        where: {
          user_id: this.args['userId'],
          thread_id: readThread.thread_id,
        },
      }
    );
    return result.firstUnreadId;
  }

  private async getViewers(ids) {
    if (this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_VIWERS))
      return {};
    const result = {};
    const pipeline = redis.pipeline();
    ids.forEach((id) => {
      if (this.hasFlag(ThreadFlag.INCLUDE_VIEWER_USERS)) getViewersPipeline(id, pipeline);
      pipeline.get(getMemberCountKey(id));
      pipeline.get(getGuestCountKey(id));
    });
    const results = await pipeline.exec();
    let index = 0;

    if (results) {
      const viewerQueries = ids.map(async (id) => {
        let userObjects;
        if (this.hasFlag(ThreadFlag.INCLUDE_VIEWER_USERS)) {
          const members = results[index][1] as Array<string>;
          userObjects = await new UserRetriever(members.map(Number)).getObjectArray();
          index += 1;
        }
        result[id] = {
          memberCount: Number(results[index][1]) || 0,
          guestCount: Number(results[index + 1][1]) || 0,
          users: userObjects,
        };
        index += 2;
      });
      await Promise.all(viewerQueries);
    }

    return result;
  }

  protected format(data: ThreadModel) {
    return {
      id: data.id,
      title: data.title,
      iconId: data.icon_id,
      subforumId: data.subforum_id,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      deletedAt: data.deletedAt,
      deleted: Boolean(data.deletedAt != null),
      locked: Boolean(data.locked),
      pinned: Boolean(data.pinned),
      lastPost: {},
      backgroundUrl: data.background_url,
      backgroundType: data.background_type,
      user: data.user_id,
    };
  }

  protected async populateData(threads) {
    // grab data from related caches
    const users =
      this.hasFlag(ThreadFlag.RETRIEVE_SHALLOW) && !this.hasFlag(ThreadFlag.INCLUDE_USER)
        ? new Map()
        : await new UserRetriever(threads.map((thread) => thread.user)).get();
    const posts = await this.getLastPosts();
    const postCounts = await this.getPostCounts();
    const recentPostCounts = await this.getRecentPosts();
    const firstPostRatings = await this.getFirstPostRatings(threads);
    const readThreads = await this.getReadThreads(threads, this.args['userId'] || null);
    const subforums = await this.getSubforums(threads);
    const tags = await this.getThreadTags(threads);
    const viewers = await this.getViewers(threads.map((thread) => thread.id));
    const userPermissionCodes = await getUserPermissionCodes(this.args['userId']);

    // merge related data in
    const populatedThreads = (
      await Promise.all(
        threads.map(async (thread) => {
          if (
            this.hasArgument('userId') &&
            !userCanViewThread(userPermissionCodes, thread, this.args['userId'])
          ) {
            return undefined;
          }

          thread.user = users.get(thread.user) || thread.user;

          thread.lastPost = posts[thread.id] != null ? posts[thread.id].lastPost : {};
          thread.postCount = postCounts[thread.id]?.postCount || 0;
          thread.recentPostCount = recentPostCounts[thread.id] || 0;

          const readThread = readThreads.get(thread.id);

          if (readThread) {
            const readThreadJson = readThread.toJSON();
            const firstUnreadId = await this.getReadThreadFirstUnreadId(
              readThreadJson,
              postCounts[thread.id]?.postCount
            );

            if ((readThread.lastPostNumber ?? -1) === -1) {
              await readThread.reload();
            }

            thread.readThread = {
              ...readThreadJson,
              user_id: undefined,
              thread_id: undefined,
              firstUnreadId,
              unreadPostCount:
                thread.postCount - (readThreadJson.lastPostNumber ?? thread.postCount),
            };
          }

          if (firstPostRatings[thread.id]) {
            thread.firstPostTopRating = firstPostRatings[thread.id];
          }

          thread.subforum = subforums.get(thread.subforumId) || null;
          thread.tags = tags.get(thread.id)?.tags || [];
          thread.viewers = viewers[thread.id];

          return thread;
        })
      )
    ).filter((thread) => thread !== undefined);

    return super.populateData(populatedThreads);
  }

  async invalidatePosts() {
    this.cacheDrop(this.lastPostCachePrefix, this.ids);
    this.cacheDrop(this.recentPostCachePrefix, this.ids);
    this.cacheDrop(this.postCountCachePrefix, this.ids);
  }
}
