import { Alert } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import ThreadRetriever, { ThreadFlag } from './thread';
import knex from '../services/knex';
import sequelize from '../models/sequelize';
import ReadThread from '../models/readThread';

export enum ReadThreadFlag {
  HIDE_NSFW,
}

export default class ReadThreadRetriever extends AbstractRetriever<Alert> {
  protected cachePrefix: string = 'alert';

  private user: number;

  private isSubscription: boolean;

  constructor(
    ids: Array<number>,
    user: number,
    isSubscription: boolean,
    flags?: Array<number>,
    args?: Object
  ) {
    super(ids, flags, args);
    this.user = user;
    this.isSubscription = isSubscription;
  }

  protected async query(ids: Array<number>): Promise<any[]> {
    return ReadThread.findAll({
      where: {
        user_id: this.user,
        thread_id: ids,
        isSubscription: this.isSubscription,
      },
      attributes: {
        include: [
          [
            sequelize.literal(
              '(SELECT id FROM Posts WHERE thread_id = ReadThread.thread_id AND thread_post_number = ReadThread.last_post_number + 1 LIMIT 1)'
            ),
            'firstUnreadId',
          ],
        ],
      },
      raw: true,
    });
  }

  private async getThreads(readThreads: Array<any>) {
    const threads = readThreads.map((readThread) => readThread.thread_id);
    const threadFlags = [
      ThreadFlag.RETRIEVE_SHALLOW,
      ThreadFlag.EXCLUDE_READ_THREADS,
      ThreadFlag.INCLUDE_LAST_POST,
      ThreadFlag.INCLUDE_POST_COUNT,
      ThreadFlag.INCLUDE_USER,
    ];

    if (this.hasFlag(ReadThreadFlag.HIDE_NSFW)) {
      threadFlags.push(ThreadFlag.INCLUDE_TAGS);
    }

    return new ThreadRetriever(threads, threadFlags).get();
  }

  private static async getUnreadCount(data) {
    if (data.lastPostNumber) {
      return data.thread.postCount - data.lastPostNumber;
    }
    const result = await knex('Posts')
      .count('id as unreadPosts')
      .first()
      .where('thread_id', data.thread_id)
      .where('created_at', '>', data.lastSeen);

    return result!.unreadPosts;
  }

  private async getFirstUnreadId(data) {
    if (data.firstUnreadId) {
      return data.firstUnreadId;
    }

    // Thread has no unread posts
    if (data.thread.postCount === data.lastPostNumber) {
      return -1;
    }

    const result = await knex('Posts')
      .select('id as firstUnreadId', 'thread_post_number')
      .first()
      .where('thread_id', data.thread_id)
      .where('created_at', '>', data.lastSeen)
      .orderBy('created_at')
      .limit(1);

    if (!result) {
      await ReadThread.update(
        {
          lastPostNumber: data.thread.postCount,
        },
        {
          where: {
            user_id: this.user,
            thread_id: data.thread_id,
          },
        }
      );
      return -1;
    }

    await ReadThread.update(
      {
        lastPostNumber: result.thread_post_number - 1,
      },
      {
        where: {
          user_id: this.user,
          thread_id: data.thread_id,
        },
      }
    );
    return result.firstUnreadId;
  }

  protected async format(data) {
    return {
      id: data.thread_id,
      thread: data.thread,
      unreadPosts: Number(await ReadThreadRetriever.getUnreadCount(data)),
      firstUnreadId: await this.getFirstUnreadId(data),
    };
  }

  protected async getRawData() {
    const readThreads = await this.query(this.ids);
    const threads = await this.getThreads(readThreads);

    // merge related data in
    readThreads.forEach((readThread) => {
      readThread.thread = threads.get(readThread.thread_id);
    });

    const filteredReadThreads = readThreads.filter((readThread) => readThread.thread !== undefined);

    return Promise.all(filteredReadThreads.map((readThread) => this.format(readThread)));
  }

  protected async populateData(readThreads: Alert[]) {
    const result = new Map();

    readThreads.forEach((readThread) => {
      result.set(readThread.id, readThread);
    });
    return result;
  }
}
