import PostResponse from '../models/postResponse';
import AbstractRetriever from './abstractRetriever';

export default class PostResponseRetriever extends AbstractRetriever {
  protected cachePrefix: string = 'post-responses';

  protected async query(ids: Array<number>) {
    return PostResponse.findAll({
      raw: true,
      attributes: [['original_post_id', 'id'], 'responsePostId', 'createdAt'],
      where: { originalPostId: ids },
    }) as any;
  }

  protected async getEntities(ids: Array<number>) {
    const results = await this.query(ids);
    const output = new Map();

    ids.forEach((id) => {
      const responses = results.filter((res) => res.id === id);

      if (responses.length === 0) {
        output.set(id, [{ id }]);
      } else {
        output.set(id, responses);
      }
    });
    return output;
  }

  protected format(data) {
    return {
      id: data[0]?.id,
      responses: data,
    };
  }
}
