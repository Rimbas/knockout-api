import knex from '../services/knex';
import AbstractRetriever from './abstractRetriever';

const tagsFromTagString = (tagString: string) => {
  if (tagString === '') {
    return [];
  }

  return tagString.split(',').map((tag) => {
    const tagParts = tag.split(':');
    return { [tagParts[0]]: tagParts[1] };
  });
};

export default class ThreadTagsRetriever extends AbstractRetriever {
  protected cachePrefix: string = 'threadTags';

  protected async query(ids: Array<number>) {
    const query = await knex
      .from('ThreadTags as tht')
      .select('tht.thread_id as id', knex.raw("GROUP_CONCAT(tht.tag_id, ':', t.name) as tags"))
      .leftJoin('Tags as t', 'tht.tag_id', 't.id')
      .whereIn('tht.thread_id', ids)
      .groupBy('tht.thread_id');

    return ids.map(
      (threadId) => query.find((tht) => Number(tht.id) === threadId) || { id: threadId, tags: '' }
    );
  }

  protected format(data) {
    return {
      id: data.id,
      tags: tagsFromTagString(data.tags),
    };
  }
}
