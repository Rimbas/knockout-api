import { CalendarEvent } from 'knockout-schema';
import { FindAttributeOptions } from 'sequelize';
import AbstractRetriever from './abstractRetriever';
import UserRetriever from './user';
import ThreadRetriever, { ThreadFlag } from './thread';
import CalendarEventModel from '../models/calendarEvent';

const SELECTED_ATTRIBUTES: FindAttributeOptions = [
  'id',
  'createdBy',
  'title',
  'description',
  ['thread_id', 'thread'],
  'startsAt',
  'endsAt',
  'createdAt',
  'updatedAt',
];

export default class CalendarEventRetriever extends AbstractRetriever<CalendarEvent> {
  protected cacheLifetime = 3600; // 1 hour

  protected cachePrefix: string = 'calendarEvent';

  protected async query(ids: Array<number>) {
    return CalendarEventModel.findAll({
      raw: true,
      attributes: SELECTED_ATTRIBUTES,
      where: { id: ids },
    });
  }

  protected format(data): CalendarEvent {
    return data;
  }

  protected async populateData(calendarEvents: CalendarEvent[]) {
    // grab data from related caches
    const users = await new UserRetriever(
      calendarEvents.map((event) => Number(event.createdBy))
    ).get();

    const threads = await new ThreadRetriever(
      calendarEvents.map((event: any) => Number(event.thread)),
      [ThreadFlag.RETRIEVE_SHALLOW, ThreadFlag.EXCLUDE_READ_THREADS]
    ).get();

    // merge related data in
    const populatedCalendarEvents = calendarEvents.map((calendarEvent: any) => ({
      ...calendarEvent,
      createdBy: users.get(Number(calendarEvent.createdBy)) || undefined,
      thread: threads.get(Number(calendarEvent.thread)) || undefined,
    }));

    return super.populateData(populatedCalendarEvents);
  }
}
