import { User } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import RoleRetriever from './role';
import { RoleCode } from '../helpers/permissions';
import ProfileRetriever from './userProfile';
import userIsOnline from '../helpers/userIsOnline';
import redis from '../services/redisClient';
import HiddenThread from '../models/hiddenThread';

export enum UserFlag {
  HIDE_BANNED,
  INCLUDE_STRIPE_CUSTOMER_ID,
  INCLUDE_DONATION_UPGRADE_EXPIRATION,
  INCLUDE_ONLINE,
  INCLUDE_HIDDEN_THREADS,
}

const BIO_TITLE_LENGTH = 30;

export default class UserRetriever extends AbstractRetriever<User> {
  protected cacheLifetime = 7200; // 2 hours

  protected cachePrefix: string = 'user';

  protected hiddenThreadsCachePrefix: string = 'userHiddenThreads';

  protected async query(ids: Array<number>) {
    return knex
      .from('Users as u')
      .select(
        'u.id as id',
        'u.username as userUsername',
        'u.avatar_url as userAvatarUrl',
        'u.background_url as userBackgroundUrl',
        'u.created_at as userCreatedAt',
        'u.updated_at as userUpdatedAt',
        'u.role_id as userRoleId',
        'u.stripe_customer_id as userStripeCustomerId',
        'u.title as userTitle',
        'u.pronouns as userPronouns',
        'u.donation_upgrade_expires_at as userDonationUpgradeExpiresAt',
        'u.use_bio_for_title as userUseBioForTitle',
        'u.last_online_at as userLastOnlineAt',
        'u.show_online_status as userShowOnlineStatus',
        'u.disable_incoming_messages as userDisableIncomingMessages',
        knex.raw(
          '(select count(*) from Threads as t where t.user_id = u.id and deleted_at is null) as userThreadCount'
        ),
        knex.raw('(select count(*) from Posts as p where p.user_id = u.id) as userPostCount')
      )
      .whereIn('u.id', ids);
  }

  protected format(data): Object {
    return {
      id: data.id,
      role: data.userRoleId,
      username: data.userUsername,
      avatarUrl: data.userAvatarUrl,
      backgroundUrl: data.userBackgroundUrl,
      posts: data.userPostCount,
      threads: data.userThreadCount,
      createdAt: data.userCreatedAt,
      updatedAt: data.userUpdatedAt,
      banned: false,
      isBanned: false, // deprecate
      stripeCustomerId: data.userStripeCustomerId,
      title: data.userTitle,
      pronouns: data.userPronouns,
      donationUpgradeExpiresAt: data.userDonationUpgradeExpiresAt,
      useBioForTitle: data.userUseBioForTitle,
      lastOnlineAt: data.userLastOnlineAt,
      showOnlineStatus: data.userShowOnlineStatus,
      disableIncomingMessages: data.userDisableIncomingMessages,
    };
  }

  private async getHiddenThreads(users): Promise<Map<number, Number[]>> {
    if (!this.hasFlag(UserFlag.INCLUDE_HIDDEN_THREADS)) return new Map();

    const userIds = users.map((user) => user?.id).filter((id) => id !== null);

    const result = new Map<number, Number[]>();
    await Promise.all(
      userIds.map(async (userId) => {
        const cachedSavedThreads = await redis.get(`${this.hiddenThreadsCachePrefix}-${userId}`);
        if (cachedSavedThreads) {
          result.set(userId, JSON.parse(cachedSavedThreads));
        } else {
          const hiddenThreadIds = (
            await HiddenThread.findAll({
              attributes: ['threadId'],
              where: { userId },
            })
          ).map((hiddenThread) => hiddenThread.threadId);

          result.set(userId, hiddenThreadIds);
          redis.setex(
            `${this.hiddenThreadsCachePrefix}-${userId}`,
            86400,
            JSON.stringify(hiddenThreadIds)
          );
        }
      })
    );

    return result;
  }

  async populateData(users) {
    const data = users.filter((user) => !this.hasFlag(UserFlag.HIDE_BANNED) || !user.banned);

    const roles = await new RoleRetriever(data.map((user) => user.role)).get();

    const bioTitleUserIds = data.filter((user) => user.useBioForTitle).map((user) => user.id);
    const bioTitleProfiles = await new ProfileRetriever(bioTitleUserIds).getObjectArray();
    const bioTitles = bioTitleProfiles.reduce((titles, profile) => {
      const bioTitle = profile.bio?.split('\n')[0].slice(0, BIO_TITLE_LENGTH);
      return { ...titles, [profile.id]: bioTitle };
    }, {});

    const hiddenThreads = await this.getHiddenThreads(users);

    const populatedUsers = data.map((user) => {
      const userTitle = bioTitles[user.id] || user.title;
      const showOnlineStatus = this.hasFlag(UserFlag.INCLUDE_ONLINE) && user.showOnlineStatus;

      return {
        ...user,
        title: userTitle,
        role: roles.get(user.role) || user.role,
        banned: roles.get(user.role)?.code === RoleCode.BANNED_USER,
        isBanned: roles.get(user.role)?.code === RoleCode.BANNED_USER,
        stripeCustomerId: this.hasFlag(UserFlag.INCLUDE_STRIPE_CUSTOMER_ID)
          ? user.stripeCustomerId
          : undefined,
        donationUpgradeExpiresAt: this.hasFlag(UserFlag.INCLUDE_DONATION_UPGRADE_EXPIRATION)
          ? user.donationUpgradeExpiresAt
          : undefined,
        online: showOnlineStatus ? userIsOnline(new Date(user.lastOnlineAt)) : undefined,
        hiddenThreads: this.hasFlag(UserFlag.INCLUDE_HIDDEN_THREADS)
          ? hiddenThreads.get(user.id)
          : undefined,
      };
    });
    return super.populateData(populatedUsers);
  }

  async invalidateHiddenThreads() {
    this.cacheDrop(this.hiddenThreadsCachePrefix, this.ids);
  }
}
