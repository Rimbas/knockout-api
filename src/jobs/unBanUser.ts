import { Op } from 'sequelize';
import { RoleCode } from '../helpers/permissions';
import { unBanUser } from '../helpers/user';
import { jobError, jobInfo } from '../log';
import UserRetriever from '../retriever/user';
import Ban from '../models/ban';
import { DELETED_USER_NAME } from '../constants/deletedUser';

interface UnbanUserOptions {
  userId: number;
}

const log = (message: string) => jobInfo(`[jobs/unban-user] ${message}`);
const error = (message: string) => jobError(`[jobs/unban-user] ${message}`);

export default async (options: UnbanUserOptions) => {
  const { userId } = options;
  log(`Running job with userId ${userId}...`);

  try {
    const user = await new UserRetriever(userId).getSingleObject();

    // dont unban deleted users
    if (user.username === DELETED_USER_NAME) {
      log(`User ${userId} is deleted. Exiting...`);
      return;
    }

    // make sure user has no active bans
    const userActiveBanCount = await Ban.count({
      where: {
        expires_at: { [Op.gt]: new Date() },
        user_id: userId,
      },
    });

    if (userActiveBanCount > 0) {
      log(`User ${userId} still has at least one active ban. Exiting...`);
      return;
    }

    // make sure user has banned user role

    if (user.role.code !== RoleCode.BANNED_USER) {
      log(`User ${userId} is already unbanned. Exiting...`);
      return;
    }

    await unBanUser(userId);
    log(`Unbanned user ${userId}`);
  } catch (e) {
    error(`Failed to run job: ${e.message}`);
  }
};
