import { jobError, jobInfo } from '../log';
import deleteUser from './deleteUser';
import unBanUser from './unBanUser';
import wipeUser from './wipeUser';

export enum UserJob {
  UNBAN_USER = 'unBanUser',
  DELETE_USER = 'deleteUser',
  WIPE_USER = 'wipeUser',
}

const log = (msg: string) => {
  jobInfo(`[jobs/user-job-processor ${msg}`);
};

const error = (msg: string) => {
  jobError(`[jobs/user-job-processor ${msg}`);
};

export default async (job) => {
  const { type } = job.data;
  try {
    switch (type) {
      case UserJob.UNBAN_USER:
        await unBanUser(job.data);
        break;
      case UserJob.DELETE_USER:
        await deleteUser(job.data);
        break;
      case UserJob.WIPE_USER:
        await wipeUser(job.data);
        break;
      default:
        log(`Unhandled Job type encountered: ${type}`);
    }
    log(`Processed job with data ${JSON.stringify(job.data)}`);
    return await Promise.resolve({ status: 'complete' });
  } catch (e) {
    error(`Failed to process job ${type}: ${e.message}`);
    // Throw the error again so that Bull's automatic failure retry can kick in
    throw e;
  }
};
