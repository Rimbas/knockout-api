/* eslint-disable no-await-in-loop */

import { Op } from 'sequelize';
import { jobError, jobInfo } from '../log';
import SiteStatRetriever from '../retriever/siteStat';
import User from '../models/user';
import Ban from '../models/ban';
import Post from '../models/post';
import Report from '../models/report';
import Thread from '../models/thread';
import SiteStat from '../models/siteStat';

const log = (message: string) => jobInfo(`[jobs/calculate-site-stats] ${message}`);
const error = (message: string) => jobError(`[jobs/calculate-site-stats] ${message}`);

const totalActiveUsers = (fromDate: Date, toDate: Date) =>
  User.count({
    where: { lastOnlineAt: { [Op.between]: [fromDate, toDate] } },
  });

const totalBans = (fromDate: Date, toDate: Date) =>
  Ban.count({
    where: { createdAt: { [Op.between]: [fromDate, toDate] } },
  });

const totalNewUsers = (fromDate: Date, toDate: Date) =>
  User.count({
    where: { createdAt: { [Op.between]: [fromDate, toDate] } },
  });

const totalPosts = (fromDate: Date, toDate: Date) =>
  Post.count({
    where: { createdAt: { [Op.between]: [fromDate, toDate] } },
  });

const totalReports = (fromDate: Date, toDate: Date) =>
  Report.count({
    where: { createdAt: { [Op.between]: [fromDate, toDate] } },
  });

const totalThreads = (fromDate: Date, toDate: Date) =>
  Thread.count({
    where: { createdAt: { [Op.between]: [fromDate, toDate] } },
  });

export default async () => {
  log('Running job...');

  try {
    const latestSiteStatId = (await SiteStat.findOne({
      attributes: ['id'],
      order: [['to_date', 'desc']],
    }))!.id;

    const latestSiteStat = await new SiteStatRetriever(latestSiteStatId).getSingleObject();

    const newSiteStatFromDate = new Date(latestSiteStat.toDate);
    const newSiteStatToDate = new Date();

    log('Calcuating totals...');
    const activeUsers = totalActiveUsers(newSiteStatFromDate, newSiteStatToDate);
    const bans = totalBans(newSiteStatFromDate, newSiteStatToDate);
    const newUsers = totalNewUsers(newSiteStatFromDate, newSiteStatToDate);
    const posts = totalPosts(newSiteStatFromDate, newSiteStatToDate);
    const reports = totalReports(newSiteStatFromDate, newSiteStatToDate);
    const threads = totalThreads(newSiteStatFromDate, newSiteStatToDate);

    log('Creating new SiteStat...');
    await SiteStat.create({
      fromDate: newSiteStatFromDate,
      toDate: newSiteStatToDate,
      totalActiveUsers: await activeUsers,
      totalBans: await bans,
      totalNewUsers: await newUsers,
      totalPosts: await posts,
      totalReports: await reports,
      totalThreads: await threads,
    });

    return null;
  } catch (e) {
    error(`Failed to run job: ${e.message}`);
    return null;
  }
};
