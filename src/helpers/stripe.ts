import { GoldSubscription } from 'knockout-schema';
import Stripe from 'stripe';

import {
  STRIPE_DONATION_PRICE_ID,
  STRIPE_GOLD_MEMBER_PRICE_ID,
  STRIPE_WEBHOOK_SIGNING_SECRET,
} from '../../config/server';

import UserRetriever, { UserFlag } from '../retriever/user';
import redis from '../services/redisClient';
import stripeClient from '../services/stripeClient';
import User from '../models/user';

export const AVAILABLE_PRODUCT_TYPES = ['Gold Member'];

const USER_KNOCKOUT_GOLD_PREFIX = 'knockout-gold-user-';
const USER_KNOCKOUT_GOLD_TTL_SECONDS = 86400;

/**
 * Takes a Stripe.Subscription object and returns a GoldSubscription
 *
 * @param stripeSubscription the raw Stripe Subscription
 * @returns the GoldSubscription object corresponding to the Stripe Subscription,
 *          or null if the stripe sub is null
 */
const formattedSubscription = (
  stripeSubscription?: Stripe.Subscription
): GoldSubscription | null => {
  if (!stripeSubscription) {
    return null;
  }

  const {
    id,
    start_date: startDate,
    status,
    cancel_at: cancelAt,
    current_period_end: currentPeriodEnd,
  } = stripeSubscription;
  const startedAt = new Date(startDate * 1000).toISOString();
  const canceledAt = cancelAt ? new Date(cancelAt * 1000).toISOString() : '';
  const nextPaymentAt = currentPeriodEnd ? new Date(currentPeriodEnd * 1000).toISOString() : '';
  return {
    stripeId: id,
    status,
    startedAt,
    canceledAt,
    nextPaymentAt,
  };
};

/**
 * The Knockout Gold subscription cache key for a user
 *
 * @param userId the ID of the User to get KO subscription info for
 * @returns the cache key
 */
const subscriptionCacheKey = (userId: number) => `${USER_KNOCKOUT_GOLD_PREFIX}-${userId}`;

/**
 * Finds the corresponding Stripe Customer ID for a user,
 * or creates one if it does not exist already.
 *
 * @param userId the user ID to fetch the Stripe customer ID for
 * @returns the Stripe customer ID of the user
 */
const findOrCreateStripeCustomerId = async (userId: number): Promise<string | undefined> => {
  // if the user already has a stripe customer id, return it
  const userRetriever = new UserRetriever(userId, [UserFlag.INCLUDE_STRIPE_CUSTOMER_ID]);
  const user = await userRetriever.getSingleObject();
  let { stripeCustomerId } = user;

  if (stripeCustomerId) {
    return stripeCustomerId;
  }

  try {
    // otherwise create a new customer and return its id
    const newStripeCustomer = await stripeClient.customers.create({
      description: `Knockout User ${userId}`,
      metadata: {
        knockoutUserId: userId,
      },
    });

    stripeCustomerId = newStripeCustomer['id'];

    // update user and bust user cache

    const userRecord = await User.findOne({ where: { id: userId } });
    await userRecord!.update({ stripeCustomerId });
    await userRecord!.save();
    await userRetriever.invalidate();

    return stripeCustomerId;
  } catch (exception) {
    console.error(`[stripe-api] Error creating stripe customer for user with ID ${userId}:`);
    console.error(exception.message);
    return undefined;
  }
};

/**
 * Finds the current Knockout Gold subscription for a user
 *
 * @param userId
 * @returns the associated Knockout Gold Stripe Subscription, or null if no sub is found
 */
export const findGoldSubscription = async (userId: number): Promise<GoldSubscription | null> => {
  // if the user does not have a stripe customer ID, return null
  const userRetriever = new UserRetriever(userId, [UserFlag.INCLUDE_STRIPE_CUSTOMER_ID]);
  const user = await userRetriever.getSingleObject();
  const { stripeCustomerId } = user;

  if (!stripeCustomerId) {
    return null;
  }

  // if we already have a cached subscription, return it
  const cachedSubscription = await redis.get(subscriptionCacheKey(userId));
  const parsedSubscription = cachedSubscription ? JSON.parse(cachedSubscription) : null;

  if (parsedSubscription) {
    return parsedSubscription;
  }

  // otherwise, get the subscription for the user
  try {
    const userGoldMemberSubscriptions = await stripeClient.subscriptions.list({
      customer: stripeCustomerId,
      price: STRIPE_GOLD_MEMBER_PRICE_ID ?? undefined,
    });

    const sub = formattedSubscription(userGoldMemberSubscriptions.data[0]);

    redis.setex(subscriptionCacheKey(userId), USER_KNOCKOUT_GOLD_TTL_SECONDS, JSON.stringify(sub));

    return sub;
  } catch (exception) {
    console.error(`[stripe-api] Error finding gold membership for user with ID ${userId}:`);
    console.error(exception.message);
    return null;
  }
};
/**
 * Creates a Stripe Checkout Session for purchasing a Knockout Gold subscription for a user
 * Does not create a new session of user already has an active Knockout Gold subscription
 *
 * @param userId the user ID we are creating this checkout session for
 * @param successUrl the URL to redirect to on successful payment
 * @param cancelUrl the URL to redirect to when the user cancels the operation
 * @returns the corresponding Stripe Checkout Session, or null if the user has Knockout Gold already
 */
export const goldCheckoutSession = async (
  userId: number,
  successUrl: string,
  cancelUrl: string
): Promise<Stripe.Checkout.Session | null> => {
  const stripeCustomerid = await findOrCreateStripeCustomerId(userId);
  const currentSubscription = await findGoldSubscription(userId);

  // make sure we cant get a duplicate checkout session if the user is already gold
  if (currentSubscription) {
    return null;
  }

  try {
    const session = await stripeClient.checkout.sessions.create({
      mode: 'subscription',
      payment_method_types: ['card'],
      customer: stripeCustomerid,
      line_items: [
        {
          price: STRIPE_GOLD_MEMBER_PRICE_ID ?? undefined,
          quantity: 1,
        },
      ],
      metadata: {
        name: `Knockout Gold checkout session for User ${userId}`,
        type: 'subscription',
      },
      success_url: successUrl,
      cancel_url: cancelUrl,
    });

    return session;
  } catch (exception) {
    console.error(
      `[stripe-api] Error creating Knockout Gold checkout session for user with ID ${userId}`
    );
    console.error(exception.message);
    return null;
  }
};

/**
 * Creates a Stripe Checkout Session for a user donating a fixed amount
 *
 * @param userId the user ID we are creating this checkout session for
 * @param successUrl the URL to redirect to on successful payment
 * @param cancelUrl the URL to redirect to when the user cancels the operation
 * @param amountCentsUsd: the amount in cents USD the checkout session's donation will be for
 * @returns the corresponding Stripe Checkout Session, or null if the session could not be generated
 */
export const donationCheckoutSession = async (
  userId: number,
  successUrl: string,
  cancelUrl: string
): Promise<Stripe.Checkout.Session | null> => {
  const stripeCustomerid = await findOrCreateStripeCustomerId(userId);

  try {
    const session = await stripeClient.checkout.sessions.create({
      mode: 'payment',
      payment_method_types: ['card'],
      customer: stripeCustomerid,
      line_items: [
        {
          price: STRIPE_DONATION_PRICE_ID ?? undefined,
          quantity: 1,
        },
      ],
      metadata: {
        name: `Knockout donation checkout session for User ${userId}`,
        type: 'donation',
      },
      success_url: successUrl,
      cancel_url: cancelUrl,
      submit_type: 'donate',
    });

    return session;
  } catch (exception) {
    console.error(
      `[stripe-api] Error creating Knockout donation checkout session for user with ID ${userId}`
    );
    console.error(exception.message);
    return null;
  }
};

/**
 * Cancels a user's current Knockout Gold subscription, if they have one
 *
 * @param userId the user who we want to cancel Knockout Gold for
 * @returns the cancelled Stripe Subscription, or null if no sub was found
 */
export const cancelGoldSubscription = async (userId: number): Promise<GoldSubscription | null> => {
  const subscription = await findGoldSubscription(userId);
  if (!subscription) {
    return null;
  }

  try {
    // cancel the subscription amd bust the subscription cache for the user
    const cancelledSubscription = await stripeClient.subscriptions.del(subscription.stripeId);

    redis.del(subscriptionCacheKey(userId));
    return formattedSubscription(cancelledSubscription);
  } catch (exception) {
    console.error(
      `[stripe-api] Error cancelling Knockout Gold subscription for user with ID ${userId}`
    );
    console.error(exception.message);
    return null;
  }
};

export const paymentPortalSessionUrl = async (
  userId: number,
  returnUrl: string
): Promise<string> => {
  const stripeCustomerId = await findOrCreateStripeCustomerId(userId);

  if (!stripeCustomerId) {
    return '';
  }

  try {
    const session = await stripeClient.billingPortal.sessions.create({
      customer: stripeCustomerId,
      return_url: returnUrl,
    });
    return session.url;
  } catch (exception) {
    console.error(`[stripe-api] Error retrieving Payment Portal for user with ID ${userId}`);
    console.error(exception.message);
    return '';
  }
};

export const getWebhookEvent = async (
  rawRequestBody: string | Buffer,
  header: string
): Promise<Stripe.Event> => {
  const secret = STRIPE_WEBHOOK_SIGNING_SECRET;
  const event = await stripeClient.webhooks.constructEventAsync(rawRequestBody, header, secret!);
  return event;
};
