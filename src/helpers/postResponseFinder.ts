import { ShortcodeTree } from 'shortcode-tree';

export default (content: string): number[] => {
  try {
    const tree = ShortcodeTree.parse(content);
    const postIds = tree.children
      .map((node) => {
        if (node.shortcode?.properties?.postId) {
          return Number(node.shortcode.properties.postId);
        }
        return null;
      })
      .filter((id) => !!id);
    return [...new Set<number>(postIds)];
  } catch (e) {
    console.error(`Could not parse post for post responses: ${e}`);
    return [];
  }
};
