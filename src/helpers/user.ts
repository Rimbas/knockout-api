import { PermissionCode, Thread } from 'knockout-schema';
import { Op } from 'sequelize';
import RoleRetriever, { RoleFlag } from '../retriever/role';
import UserRetriever, { UserFlag } from '../retriever/user';
import {
  getBasicUserRoleId,
  getGuestUserRoleId,
  getLimitedUserRoleId,
  getPaidGoldUserRoleId,
  getBannedUserRoleId,
  getModeratorLevelRoleIds,
} from './role';
import PreviousUserRole from '../models/previousUserRole';
import User from '../models/user';
import Subforum from '../models/subforum';

export const unBanUser = async (userId: number) => {
  // make user's new role the same as their first unbanned previous role
  const bannedUserRoleId = await getBannedUserRoleId();
  const previousUserRole = await PreviousUserRole.findOne({
    attributes: ['id', 'role_id'],
    where: { user_id: userId, role_id: { [Op.not]: bannedUserRoleId } },
    order: [['created_at', 'DESC']],
  });

  let newRoleId = previousUserRole?.role_id;
  // if previous role is not found, make them a limited user
  if (!newRoleId) {
    newRoleId = await getLimitedUserRoleId();
  }

  const user = await User.findOne({ where: { id: userId } });
  await user!.update({ roleId: newRoleId });
  await user!.save();

  await new UserRetriever(userId).invalidate();
};

export const demoteUserFromGold = async (userId: number) => {
  const user = await User.findOne({ attributes: ['id', 'roleId'], where: { id: userId } });

  // if the user is banned, do not update their current role (dont unban them). update their previous role
  // to a basic user, so that they go back to that role when they are unbanned
  const bannedUserRoleId = await getBannedUserRoleId();
  const basicUserRoleId = await getBasicUserRoleId();
  if (user!.roleId === bannedUserRoleId) {
    await PreviousUserRole.create({ user_id: userId, role_id: basicUserRoleId });
    return;
  }

  // make user's new role the same as their first previous role that is not paid-gold or banned
  // this accounts for cases where a previously banned user is demoted from gold after their ban
  // in this case their previous role is the banned user, but we don't want to revert to that, so we
  // find the nearest valid role
  const invalidRoleIds = [bannedUserRoleId, await getPaidGoldUserRoleId()];
  const previousUserRole = await PreviousUserRole.findOne({
    attributes: ['id', 'role_id'],
    where: { user_id: userId, role_id: { [Op.notIn]: invalidRoleIds } },
    order: [['created_at', 'DESC']],
  });

  let newRoleId = previousUserRole?.role_id;
  // if previous role is not found, make them a basic user
  if (!newRoleId) {
    newRoleId = basicUserRoleId;
  }

  await user!.update({ roleId: newRoleId });
  await user!.save();

  await new UserRetriever(userId).invalidate();
};

export const promoteUserToGold = async (userId: number) => {
  const goldRoleId = await getPaidGoldUserRoleId();
  const user = await User.findOne({ where: { id: userId } });

  // dont promote user to gold if they are a moderator / admin
  const invalidRoleIds = await getModeratorLevelRoleIds();
  if (invalidRoleIds.includes(user!.roleId)) {
    return;
  }

  // if the user is banned, do not update their current role (dont unban them). Update
  // their previous role to gold, so that their subscription resumes when they are unbanned
  const bannedUserRoleId = await getBannedUserRoleId();
  if (user!.roleId === bannedUserRoleId) {
    await PreviousUserRole.create({ user_id: userId, role_id: goldRoleId });
    return;
  }

  await user!.update({ roleId: goldRoleId });
  await user!.save();

  await new UserRetriever(userId).invalidate();
};

export const getUserPermissionCodes = async (userId?: number): Promise<PermissionCode[]> => {
  // if we don't get a user id, return the guest user permission codes
  if (!userId) {
    const guestRoleId = await getGuestUserRoleId();
    const { permissionCodes } = await new RoleRetriever(guestRoleId, [
      RoleFlag.INCLUDE_PERMISSION_CODES,
    ]).getSingleObject();
    return permissionCodes;
  }

  const {
    role: { id: userRoleId },
  } = await new UserRetriever(userId).getSingleObject();

  const role = await new RoleRetriever(userRoleId, [
    RoleFlag.INCLUDE_PERMISSION_CODES,
  ]).getSingleObject();

  return role.permissionCodes;
};

export const userHasPermissions = async (
  permissionCodes: PermissionCode[],
  userId?: number
): Promise<boolean> => {
  const userCodes = await getUserPermissionCodes(userId);
  return permissionCodes.every((code) => userCodes.includes(code));
};

export const userCanViewSubforum = (
  userPermissionCodes: PermissionCode[],
  subforumId: number,
  excludePrivateSubforums: boolean = false
): boolean => {
  if (excludePrivateSubforums) {
    return userPermissionCodes.includes(`subforum-${subforumId}-view`);
  }

  return (
    userPermissionCodes.includes(`subforum-${subforumId}-view`) ||
    userPermissionCodes.includes(`subforum-${subforumId}-view-own-threads`)
  );
};

export const userCanViewThread = (
  userPermissionCodes: PermissionCode[],
  thread: Thread,
  userId?: number
): boolean => {
  if (
    thread.deletedAt !== null &&
    !userPermissionCodes.includes(`subforum-${thread.subforumId}-view-deleted-threads`)
  ) {
    return false;
  }

  const threadUserId = thread.user?.id || thread.user;

  if (userId === threadUserId) {
    return userCanViewSubforum(userPermissionCodes, thread.subforumId);
  }
  return userPermissionCodes.includes(`subforum-${thread.subforumId}-view`);
};

export const getViewableSubforumIdsByUser = async (
  userId?: number,
  excludePrivateSubforums: boolean = false
): Promise<number[]> => {
  const userCodes = await getUserPermissionCodes(userId);
  return Subforum.findAll({
    attributes: ['id'],
    order: [['created_at', 'asc']],
  }).then((records) =>
    records.reduce((list: number[], subforum) => {
      if (userCanViewSubforum(userCodes, subforum.id, excludePrivateSubforums)) {
        list.push(subforum.id);
      }
      return list;
    }, [])
  );
};

export const userHiddenThreadIds = async (userId?: number): Promise<number[]> => {
  if (!userId) {
    return [];
  }

  return (
    (await new UserRetriever(userId, [UserFlag.INCLUDE_HIDDEN_THREADS]).getSingleObject()) as any
  ).hiddenThreads;
};
