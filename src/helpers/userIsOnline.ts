import ms from 'ms';

const ONLINE_THRESHOLD_MINUTES = 15;

export default (lastOnlineDate: Date): boolean => {
  const diff = Date.now() - lastOnlineDate.getTime();

  return diff < ms(`${ONLINE_THRESHOLD_MINUTES} minutes`);
};
