import { ShortcodeTree } from 'shortcode-tree';

const USERID_MENTION_REGEX = /@<(\d+).*?>/g;

export const postQuoteFinder = (content: string): number[] => {
  const quotes: number[] = [];
  const contentTree = ShortcodeTree.parse(content);
  contentTree.children.forEach((node) => {
    if (node.shortcode?.name === 'quote' && node.shortcode.properties.mentionsUser) {
      quotes.push(Number(node.shortcode.properties.mentionsUser));
    }
  });

  // remove duplicates
  return [...new Set(quotes)];
};

const userIdMentions = (content: string): number[] => {
  const mentions: number[] = [];
  let match = USERID_MENTION_REGEX.exec(content);

  // using matchAll here requires us to
  // transpile es2020, going to use a
  // less elegant solution
  while (match && match[0] !== '') {
    mentions.push(Number(match[1]));
    match = USERID_MENTION_REGEX.exec(content);
  }

  return mentions;
};

export const postMentionFinder = (content: string, parseQuotes: boolean = false): number[] => {
  const mentions: number[] = [];
  const contentTree = ShortcodeTree.parse(content);
  if (contentTree.children.length === 0) {
    mentions.push(...userIdMentions(contentTree.text));
  }

  contentTree.children.forEach((node) => {
    // dont count mentions that are contained in quotes unless we are parsing quotes
    if (node.shortcode?.name !== 'quote' || parseQuotes) {
      mentions.push(...userIdMentions(node.text));
    }
  });

  // remove duplicates
  return [...new Set(mentions)];
};
