import { RoleCode } from './permissions';
import redis from '../services/redisClient';
import Role from '../models/role';

const getCachedRoleId = async (roleCode: RoleCode, defaultDescription: string): Promise<number> => {
  const roleIdKey = `role:${roleCode}-id`;
  if (process.env.NODE_ENV !== 'test') {
    const cachedValue = Number(await redis.get(roleIdKey));
    if (cachedValue) return cachedValue;
  }

  const [role] = await Role.findOrCreate({
    attributes: ['id'],
    where: { code: roleCode },
    defaults: { description: defaultDescription, code: roleCode },
  });

  if (process.env.NODE_ENV !== 'test') {
    await redis.set(roleIdKey, String(role.id));
  }

  return role.id;
};

export const getBannedUserRoleId = async () =>
  getCachedRoleId(RoleCode.BANNED_USER, 'A banned user with restricted forum access');

export const getGuestUserRoleId = async () =>
  getCachedRoleId(RoleCode.GUEST, 'A guest user who is not logged in to the forum');

export const getLimitedUserRoleId = async () =>
  getCachedRoleId(RoleCode.LIMITED_USER, 'A limited user with limited forum access');

export const getPaidGoldUserRoleId = async () =>
  getCachedRoleId(
    RoleCode.PAID_GOLD_USER,
    'A Gold User who has this role through a Knockout Gold subscription'
  );

export const getBasicUserRoleId = async () =>
  getCachedRoleId(RoleCode.BASIC_USER, 'A basic user with normal forum access');

export const getModeratorRoleId = async () =>
  getCachedRoleId(RoleCode.MODERATOR, 'An elevated user with moderator priveleges');

export const getAdminRoleId = async () =>
  getCachedRoleId(RoleCode.ADMIN, 'An elevated user with administrative priveleges');

export const getModeratorLevelRoleIds = async () => {
  const roles = await Role.findAll({
    attributes: ['id'],
    where: {
      code: [
        RoleCode.MODERATOR_IN_TRAINING,
        RoleCode.MODERATOR,
        RoleCode.SUPER_MODERATOR,
        RoleCode.ADMIN,
      ],
    },
  });
  return roles.map((role) => role.id);
};
