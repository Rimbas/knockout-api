import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class LogEvent extends Model<InferAttributes<LogEvent>, InferCreationAttributes<LogEvent>> {
  declare userId: number;

  declare entity: string;

  declare action: string;

  declare success: boolean;

  declare message: string;

  declare properties: string;

  declare createdAt: CreationOptional<Date>;
}

LogEvent.init(
  {
    userId: { type: DataTypes.INTEGER, field: 'user_id' },
    entity: DataTypes.STRING,
    action: DataTypes.STRING,
    success: DataTypes.BOOLEAN,
    message: DataTypes.STRING,
    properties: DataTypes.STRING,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default LogEvent;
