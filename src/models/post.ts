/* eslint-disable import/no-cycle */
import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  Op,
} from 'sequelize';
import { THREAD_POST_LIMIT } from 'knockout-schema';
import sequelize from './sequelize';
import nsfwTagName from '../constants/nsfwTagName';
import ThreadRetriever from '../retriever/thread';
import PostRetriever from '../retriever/post';
import Thread from './thread';

class Post extends Model<InferAttributes<Post>, InferCreationAttributes<Post>> {
  declare id: CreationOptional<number>;

  declare content: string;

  declare user_id: number;

  declare thread_id: number;

  declare country_name?: string | null;

  declare app_name?: string | null;

  declare distinguished?: boolean;

  declare thread_post_number: CreationOptional<number>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

Post.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    content: DataTypes.STRING,
    user_id: DataTypes.BIGINT,
    thread_id: DataTypes.BIGINT,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    country_name: DataTypes.STRING,
    app_name: DataTypes.STRING,
    thread_post_number: DataTypes.INTEGER,
    distinguished: DataTypes.BOOLEAN,
  },
  {
    scopes: {
      byUser(userId, userPermissionCodes, hideNsfw) {
        const where = {
          user_id: userId,
        };
        if (hideNsfw) {
          where['$Thread.Tags.name$'] = {
            [Op.or]: [
              null,
              {
                [Op.ne]: nsfwTagName,
              },
            ],
          };
        }
        return {
          where,
          subquery: false,
          include: [
            {
              model: sequelize.models.Thread.scope({
                method: ['byUser', userId, userPermissionCodes],
              }),
              attributes: ['id'],
              required: true,
              include: [
                {
                  attributes: ['id'],
                  model: sequelize.models.Tag,
                  as: 'Tags',
                  required: false,
                },
              ],
            },
          ],
        };
      },
    },
    hooks: {
      async afterCreate(post, options) {
        try {
          const thread = await Thread.findOne({
            where: { id: post.thread_id },
            transaction: options.transaction,
          });

          if (!thread) {
            throw new Error('Thread not found');
          }

          // if thread has null postCount, use count query and assign value to field
          // otherwise, increment the postCount column
          if (!thread.postCount) {
            thread.postCount = await Post.count({
              where: { thread_id: post.thread_id },
              transaction: options.transaction,
            });
          } else {
            await thread.increment('postCount', { transaction: options.transaction });
          }
          await thread.save({ transaction: options.transaction });
          await thread.reload({ transaction: options.transaction });

          post.thread_post_number = thread.postCount;
          await post.save({ transaction: options.transaction });
          await post.reload({ transaction: options.transaction });

          // if the thread is at or above the post limit and needs to be locked, lock it
          const threadReachedPostLimit =
            post.thread_post_number >= THREAD_POST_LIMIT && !thread.locked;

          if (threadReachedPostLimit) {
            thread.locked = true;
          }

          thread.updatedAt = new Date();
          thread.changed('updatedAt', true);

          await thread.save({ transaction: options.transaction });
          await new ThreadRetriever(thread.id).invalidate();
          await new PostRetriever(post.id).invalidate();
        } catch (err) {
          console.error(err);
          options.transaction?.rollback();
        }
      },
    },
    timestamps: true,
    sequelize,
  }
);

export default Post;
