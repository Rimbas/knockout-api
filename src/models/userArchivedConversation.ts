import {
  Model,
  InferAttributes,
  InferCreationAttributes,
  CreationOptional,
  DataTypes,
} from 'sequelize';
import sequelize from './sequelize';

class UserArchivedConversation extends Model<
  InferAttributes<UserArchivedConversation>,
  InferCreationAttributes<UserArchivedConversation>
> {
  declare userId: number;

  declare conversationId: number;

  declare createdAt: CreationOptional<Date>;
}

UserArchivedConversation.init(
  {
    userId: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      field: 'user_id',
    },
    conversationId: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      field: 'conversation_id',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default UserArchivedConversation;
