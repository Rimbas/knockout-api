import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class UserProfile extends Model<
  InferAttributes<UserProfile>,
  InferCreationAttributes<UserProfile>
> {
  declare user_id: number;

  declare heading_text?: string | null;

  declare personal_site?: string | null;

  declare background_url?: string | null;

  declare background_type?: string | null;

  declare steam?: string | null;

  declare discord?: string | null;

  declare github?: string | null;

  declare youtube?: string | null;

  declare twitter?: string | null;

  declare fediverse?: string | null;

  declare twitch?: string | null;

  declare gitlab?: string | null;

  declare tumblr?: string | null;

  declare bluesky?: string | null;

  declare header?: string | null;

  declare disable_comments: CreationOptional<boolean>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

UserProfile.init(
  {
    user_id: {
      primaryKey: true,
      allowNull: false,
      unique: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    heading_text: DataTypes.STRING,
    personal_site: DataTypes.STRING,
    background_url: DataTypes.STRING,
    background_type: DataTypes.STRING,
    steam: DataTypes.STRING,
    discord: DataTypes.STRING,
    github: DataTypes.STRING,
    youtube: DataTypes.STRING,
    twitter: DataTypes.STRING,
    fediverse: DataTypes.STRING,
    twitch: DataTypes.STRING,
    gitlab: DataTypes.STRING,
    tumblr: DataTypes.STRING,
    bluesky: DataTypes.STRING,
    header: DataTypes.STRING,
    disable_comments: DataTypes.BOOLEAN,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default UserProfile;
