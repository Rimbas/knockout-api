import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class IspBan extends Model<InferAttributes<IspBan>, InferCreationAttributes<IspBan>> {
  declare netname: string;

  declare asn: string;

  declare createdBy: number;

  declare updatedBy?: CreationOptional<number>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt?: CreationOptional<Date>;
}

IspBan.init(
  {
    netname: DataTypes.STRING,
    asn: DataTypes.STRING,
    createdBy: { type: DataTypes.INTEGER, field: 'created_by' },
    updatedBy: { type: DataTypes.INTEGER, field: 'updated_by' },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default IspBan;
