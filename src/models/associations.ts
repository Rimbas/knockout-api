import IpAddress from './ipAddress';
import Ban from './ban';
import CalendarEvent from './calendarEvent';
import Conversation from './conversation';
import ConversationUser from './conversationUser';
import Event from './event';
import ExternalAccount from './externalAccount';
import HiddenThread from './hiddenThread';
import IpBan from './ipBan';
import Message from './message';
import Notification from './notification';
import Permission from './permission';
import Post from './post';
import PostEdit from './postEdit';
import PostResponse from './postResponse';
import PreviousUserRole from './previousUserRole';
import PreviousUsername from './previousUsername';
import ProfileComment from './profileComment';
import Rating from './rating';
import ReadThread from './readThread';
import Report from './report';
import Role from './role';
import RolePermission from './rolePermission';
import Subforum from './subforum';
import Tag from './tag';
import Thread from './thread';
import ThreadTag from './threadTag';
import User from './user';
import UserProfile from './userProfile';

export default () => {
  Conversation.hasMany(ConversationUser, { foreignKey: 'conversation_id' });
  ConversationUser.belongsTo(Conversation, {
    as: 'conversation',
    foreignKey: 'conversation_id',
  });

  ConversationUser.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id',
  });

  // a user belongs to a role
  User.belongsTo(Role, { foreignKey: 'role_id' });
  // a role can belong to many users
  Role.hasMany(User, { foreignKey: 'role_id' });

  Ban.belongsTo(User, { as: 'user', foreignKey: 'user_id' });
  Ban.belongsTo(User, { as: 'banned_by_user', foreignKey: 'banned_by' });
  PostResponse.belongsTo(Post, {
    as: 'original_post',
    foreignKey: 'original_post_id',
  });
  PostResponse.belongsTo(Post, {
    as: 'response_post',
    foreignKey: 'response_post_id',
  });

  Ban.belongsTo(Post, { as: 'post', foreignKey: 'post_id' });
  PostEdit.belongsTo(Post, {
    as: 'post',
    foreignKey: 'post_id',
  });

  CalendarEvent.belongsTo(User, {
    as: 'user',
    foreignKey: 'created_by',
  });
  CalendarEvent.belongsTo(Thread, {
    as: 'thread',
    foreignKey: 'threadId',
  });

  Event.belongsTo(User, {
    as: 'user',
    foreignKey: 'creator',
  });

  ExternalAccount.belongsTo(User, {
    as: 'user',
    foreignKey: 'userId',
  });

  HiddenThread.belongsTo(User, { foreignKey: 'user_id' });
  HiddenThread.belongsTo(Thread, { foreignKey: 'thread_id' });

  IpAddress.belongsTo(User, { as: 'user', foreignKey: 'user_id' });
  IpAddress.belongsTo(Post, { as: 'post', foreignKey: 'post_id' });

  // An ipBan can have a creator
  IpBan.belongsTo(User, { as: 'user', foreignKey: 'created_by' });

  Message.belongsTo(Conversation, {
    as: 'conversation',
    foreignKey: 'conversation_id',
  });
  Message.belongsTo(User, { as: 'user', foreignKey: 'user_id' });

  Notification.belongsTo(User, {
    as: 'user',
    foreignKey: 'userId',
  });

  PostEdit.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id',
  });

  PreviousUsername.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id',
  });

  PreviousUserRole.belongsTo(User, {
    as: 'user',
    foreignKey: 'user_id',
  });
  PreviousUserRole.belongsTo(Role, {
    as: 'role',
    foreignKey: 'role_id',
  });

  ProfileComment.belongsTo(User, {
    as: 'user',
    foreignKey: 'author',
  });

  Rating.belongsTo(Post, { as: 'post', foreignKey: 'post_id' });
  Rating.belongsTo(User, { as: 'user', foreignKey: 'user_id' });

  ReadThread.belongsTo(User, { foreignKey: 'user_id' });
  ReadThread.belongsTo(Thread, { foreignKey: 'thread_id' });

  Report.belongsTo(Post, { as: 'post', foreignKey: 'postId' });

  // A role can have many permissions through RolePermissions
  Role.belongsToMany(Permission, { through: 'RolePermissions' });

  RolePermission.belongsTo(Role, {
    as: 'role',
    foreignKey: 'role_id',
  });
  RolePermission.belongsTo(Permission, {
    as: 'permission',
    foreignKey: 'permission_id',
  });

  Subforum.hasMany(Thread, {
    as: 'threads',
    foreignKey: 'subforum_id',
  });
  Thread.belongsTo(Subforum, {
    as: 'subforum',
    foreignKey: 'subforum_id',
  });

  // // A user can have many posts
  Thread.hasMany(Post, { as: 'posts', foreignKey: 'thread_id' });
  // A post can belong to a thread
  Post.belongsTo(Thread, { foreignKey: 'thread_id' });

  ThreadTag.belongsTo(Tag, { foreignKey: 'tag_id' });
  ThreadTag.belongsTo(Thread, { foreignKey: 'thread_id' });
  Thread.belongsToMany(Tag, {
    through: ThreadTag,
    foreignKey: 'thread_id',
    otherKey: 'tag_id',
  });
  Tag.belongsToMany(Thread, {
    through: ThreadTag,
    foreignKey: 'tag_id',
    otherKey: 'thread_id',
    constraints: false,
  });

  // A user can have many posts
  User.hasMany(Post, { foreignKey: 'user_id' });
  // A post belongs to a user
  Post.belongsTo(User, { as: 'user', foreignKey: 'user_id' });

  User.hasMany(Thread, { foreignKey: 'user_id' });
  Thread.belongsTo(User, { as: 'user', foreignKey: 'user_id' });

  UserProfile.belongsTo(User, { as: 'user', foreignKey: 'user_id' });
};
