import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Report extends Model<InferAttributes<Report>, InferCreationAttributes<Report>> {
  declare postId: number;

  declare reportedBy: number;

  declare reportReason: string;

  declare solvedBy?: number;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

Report.init(
  {
    postId: {
      type: DataTypes.INTEGER.UNSIGNED,
      field: 'post_id',
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
    reportedBy: {
      type: DataTypes.INTEGER,
      field: 'reported_by',
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    reportReason: {
      type: DataTypes.TEXT,
      field: 'report_reason',
    },
    solvedBy: {
      type: DataTypes.INTEGER,
      field: 'solved_by',
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default Report;
