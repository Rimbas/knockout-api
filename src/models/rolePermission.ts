import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import sequelize from './sequelize';

class RolePermission extends Model<
  InferAttributes<RolePermission>,
  InferCreationAttributes<RolePermission>
> {
  declare role_id: number;

  declare permission_id: number;
}

RolePermission.init(
  {
    role_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.BIGINT,
      references: {
        model: 'Roles',
        key: 'id',
      },
    },
    permission_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.BIGINT,
      references: {
        model: 'Permissions',
        key: 'id',
      },
    },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default RolePermission;
