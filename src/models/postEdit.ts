import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class PostEdit extends Model<InferAttributes<PostEdit>, InferCreationAttributes<PostEdit>> {
  declare postId: number;

  declare userId: number;

  declare editReason?: string | null;

  declare content: string;

  declare createdAt: CreationOptional<Date>;
}

PostEdit.init(
  {
    postId: {
      field: 'post_id',
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
    userId: {
      field: 'user_id',
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    editReason: { type: DataTypes.STRING, field: 'edit_reason' },
    content: DataTypes.TEXT,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default PostEdit;
