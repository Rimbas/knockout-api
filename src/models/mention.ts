import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Mention extends Model<InferAttributes<Mention>, InferCreationAttributes<Mention>> {
  declare id: CreationOptional<number>;

  declare postId: number;

  declare mentionsUser: number;

  declare content: string;

  declare inactive?: boolean | null;

  declare threadId: number;

  declare threadTitle: CreationOptional<string>;

  declare page: number;

  declare mentionedBy: number;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

Mention.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    postId: {
      field: 'post_id',
      type: DataTypes.INTEGER.UNSIGNED,
      references: { model: 'Posts', key: 'id' },
    },
    mentionsUser: {
      field: 'mentions_user',
      type: DataTypes.INTEGER,
      references: { model: 'Users', key: 'id' },
    },
    content: { type: DataTypes.JSON, allowNull: false },
    inactive: { type: DataTypes.BOOLEAN, allowNull: true },
    threadId: {
      field: 'thread_id',
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      references: { model: 'Threads', key: 'id' },
    },
    threadTitle: { field: 'thread_title', type: DataTypes.STRING, allowNull: true },
    page: { type: DataTypes.INTEGER, allowNull: false },
    mentionedBy: {
      field: 'mentioned_by',
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: 'Users', key: 'id' },
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default Mention;
