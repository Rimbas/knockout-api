import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import ratingList from '../helpers/ratingList.json';
import sequelize from './sequelize';
import knex from '../services/knex';

class Rating extends Model<InferAttributes<Rating>, InferCreationAttributes<Rating>> {
  declare post_id: number;

  declare user_id: number;

  declare rating_id: number;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

Rating.init(
  {
    post_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    rating_id: {
      type: DataTypes.INTEGER.UNSIGNED,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  },
  {
    timestamps: true,
    sequelize,
  }
);

function sortRatings(ratings) {
  Object.keys(ratings).forEach((post) => {
    ratings[post].sort((a, b) => b.count - a.count);
  });
  return ratings;
}

export const getRatingsForPostsWithUsers = (postIds) =>
  knex
    .from('Ratings as rating')
    .select(
      'rating.rating_id as ratingId',
      'rating.post_id as ratingPostId',
      'userRatedBy.username as ratedBy'
    )
    .whereIn('rating.post_id', postIds)
    .leftJoin('Users as userRatedBy', 'rating.user_id', 'userRatedBy.id')
    .then((results) => {
      let resultArray = Array.isArray(results) ? results : [results];

      resultArray = resultArray.reduce((ratings, rating) => {
        if (ratings[rating.ratingPostId] === undefined) {
          ratings[rating.ratingPostId] = {};
        }
        if (ratings[rating.ratingPostId][rating.ratingId] === undefined) {
          ratings[rating.ratingPostId][rating.ratingId] = [];
        }
        ratings[rating.ratingPostId][rating.ratingId].push(rating.ratedBy);

        return ratings;
      }, {});

      Object.keys(resultArray).forEach((key) => {
        const ratingArray: object[] = [];
        Object.keys(resultArray[key]).forEach((ratingId) => {
          const ratingShorthand = ratingList[ratingId].short;

          ratingArray.push({
            rating: ratingShorthand,
            rating_id: ratingId,
            users: resultArray[key][ratingId],
            count: resultArray[key][ratingId].length,
          });
        });

        resultArray[key] = ratingArray;
      });

      return sortRatings(resultArray);
    });

export const getRatingsForPosts = (postIds) =>
  knex
    .from('Ratings as rating')
    .select('rating.rating_id as ratingId', 'rating.post_id as ratingPostId')
    .count('rating.rating_id as ratingCount')
    .whereIn('rating.post_id', postIds)
    .groupBy('rating.post_id', 'rating.rating_id')
    .then((results) => {
      const resultArray = Array.isArray(results) ? results : [results];

      return sortRatings(
        resultArray.reduce((ratings, rating) => {
          if (ratings[rating.ratingPostId] === undefined) {
            ratings[rating.ratingPostId] = [];
          }

          const ratingShorthand = ratingList[rating.ratingId].short;
          ratings[rating.ratingPostId].push({
            rating: ratingShorthand,
            rating_id: rating.ratingId,
            count: rating.ratingCount,
          });

          return ratings;
        }, {})
      );
    });

export default Rating;
