import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  Op,
} from 'sequelize';
import sequelize from './sequelize';
import nsfwTagName from '../constants/nsfwTagName';
import Thread from './thread';
import Tag from './tag';

class ReadThread extends Model<InferAttributes<ReadThread>, InferCreationAttributes<ReadThread>> {
  declare user_id: number;

  declare thread_id: number;

  declare lastSeen?: CreationOptional<Date>;

  declare lastPostNumber: number;

  declare isSubscription: boolean;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

ReadThread.init(
  {
    user_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    thread_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Threads',
        key: 'id',
      },
    },
    lastSeen: {
      type: DataTypes.DATE,
      field: 'last_seen',
      defaultValue: DataTypes.NOW,
    },
    lastPostNumber: {
      type: DataTypes.INTEGER,
      field: 'last_post_number',
    },
    isSubscription: {
      type: DataTypes.BOOLEAN,
      field: 'is_subscription',
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  },
  {
    timestamps: true,
    sequelize,
    scopes: {
      byUser(userId, userPermissionCodes, hideNsfw) {
        const where = {
          user_id: userId,
        };
        if (hideNsfw) {
          where['$Thread.Tags.name$'] = {
            [Op.or]: [
              null,
              {
                [Op.ne]: nsfwTagName,
              },
            ],
          };
        }
        return {
          where,
          subquery: false,
          include: [
            {
              model: Thread.scope({
                method: ['byUser', userId, userPermissionCodes],
              }),
              attributes: ['id'],
              required: true,
              include: [
                {
                  attributes: ['id'],
                  model: Tag,
                  as: 'Tags',
                  required: false,
                },
              ],
            },
          ],
        };
      },
    },
  }
);

export default ReadThread;
