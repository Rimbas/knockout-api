import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Event extends Model<InferAttributes<Event>, InferCreationAttributes<Event>> {
  declare id: CreationOptional<number>;

  declare type: string;

  declare dataId: number;

  declare content: string;

  declare creator: number;

  declare createdAt: CreationOptional<Date>;
}

Event.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
    },
    type: DataTypes.STRING,
    dataId: { type: DataTypes.BIGINT, field: 'data_id' },
    content: { type: DataTypes.STRING },
    creator: {
      type: DataTypes.BIGINT,
      field: 'executed_by',
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default Event;
