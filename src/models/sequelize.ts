import { Sequelize } from 'sequelize';

import { NODE_ENV } from '../../config/server';

const config = require('../../config/config')[NODE_ENV];

const testConnection = (sequelize: Sequelize) => {
  sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch((err) => {
      console.error('Unable to connect to the database:', err);
    });
};

console.log('Connection to database: ', config.database);
console.log('Database host: ', config.host);
const sequelize = new Sequelize(config.database, config.username, config.password, config);

testConnection(sequelize);

export default sequelize;
