import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class PreviousUserRole extends Model<
  InferAttributes<PreviousUserRole>,
  InferCreationAttributes<PreviousUserRole>
> {
  declare user_id: number;

  declare role_id: number;

  declare createdAt: CreationOptional<Date>;
}

PreviousUserRole.init(
  {
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    role_id: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Roles',
        key: 'id',
      },
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default PreviousUserRole;
