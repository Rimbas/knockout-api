import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class PostResponse extends Model<
  InferAttributes<PostResponse>,
  InferCreationAttributes<PostResponse>
> {
  public originalPostId!: number;

  public responsePostId!: number;

  public createdAt!: CreationOptional<Date>;
}

PostResponse.init(
  {
    originalPostId: {
      field: 'original_post_id',
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
    responsePostId: {
      field: 'response_post_id',
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default PostResponse;
