import path from 'path';
import winston from 'winston';

let defaultTransports: winston.transport[] = [];

// if not running in production, add console log to all winston transports
if (process.env.NODE_ENV !== 'production') {
  defaultTransports = [new winston.transports.Console()];
}

let baseLogPath;
if (process.env.NODE_ENV === 'production') {
  baseLogPath = '/var/log';
} else {
  baseLogPath = path.join(__dirname, '../../tmp/log');
}

const jobLogger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
  defaultMeta: { service: 'jobs' },
  transports: [
    new winston.transports.File({
      filename: `${baseLogPath}/knockout-api-jobs-err.log`,
      level: 'error',
      maxsize: 20000000,
      maxFiles: 10,
      tailable: true,
    }),
    new winston.transports.File({
      filename: `${baseLogPath}/knockout-api-jobs.log`,
      level: 'info',
      maxsize: 20000000,
      maxFiles: 10,
      tailable: true,
    }),
    ...defaultTransports,
  ],
});

export const jobError = (msg: string) => jobLogger.log('error', msg);

export const jobInfo = (msg: string) => jobLogger.log('info', msg);
