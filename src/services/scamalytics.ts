import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import redis from './redisClient';
import {
  SCAMALYTICS_TEST,
  SCAMALYTICS_URL,
  SCAMALYTICS_USERNAME,
  SCAMALYTICS_KEY,
  SCAMALYTICS_THRESHOLD,
} from '../../config/server';

const SCAMALYTICS_TIMEOUT_MS = 5000;

const SCAMALYTICS_CACHE_TTL = 2592000;

export default class Scamalytics {
  private ip: string;

  constructor(ip) {
    axios.defaults.adapter = httpAdapter;
    this.ip = ip;
  }

  getCacheKey() {
    return `scamalytics-${this.ip}`;
  }

  public async setQueryCache(data) {
    await redis.setex(this.getCacheKey(), SCAMALYTICS_CACHE_TTL, JSON.stringify(data));
  }

  public async queryCache() {
    const response = await redis.get(this.getCacheKey());
    return response ? JSON.parse(response) : null;
  }

  public async queryAPI() {
    try {
      axios.defaults.adapter = httpAdapter;
      const response = await axios({
        method: 'get',
        baseURL: `https://${SCAMALYTICS_URL}/`,
        url: `/${SCAMALYTICS_USERNAME}/`,
        data: {
          ip: this.ip,
          key: SCAMALYTICS_KEY,
          test: SCAMALYTICS_TEST,
        },
        headers: {
          Accept: 'application/json',
        },
        timeout: SCAMALYTICS_TIMEOUT_MS,
      });
      return response.data;
    } catch (err) {
      return null;
    }
  }

  public async query() {
    const cacheResults = await this.queryCache();
    if (cacheResults !== null) return cacheResults;
    const apiResults = await this.queryAPI();
    return apiResults;
  }

  public async isRisky() {
    try {
      const result = await this.query();
      console.log('IP Risk', {
        ip: result.ip,
        risk: result.risk,
        score: result.score,
      });
      const output = Number(result.score) >= Number(SCAMALYTICS_THRESHOLD);
      await this.setQueryCache(result);
      return output;
    } catch (err) {
      return false;
    }
  }
}
