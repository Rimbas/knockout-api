import { Server } from 'socket.io';
import { EventType } from 'knockout-schema';
import Stripe from 'stripe';
import { STRIPE_GOLD_MEMBER_PRICE_ID } from '../../config/server';
import { demoteUserFromGold, promoteUserToGold } from '../helpers/user';
import UserRetriever from '../retriever/user';
import { createEvent } from '../controllers/eventLogController';
import { getPaidGoldUserRoleId } from '../helpers/role';
import { getWebhookEvent } from '../helpers/stripe';
import User from '../models/user';

const GOLD_PRICE_PENCE = 350;

export default class StripeWebhookHandler {
  private event: Stripe.Event;

  public async handleWebhookEvent(rawEventBody: string, stripeSignature: string, io?: Server) {
    this.event = await getWebhookEvent(rawEventBody, stripeSignature);

    switch (this.event.type) {
      // gold subscription payment succeeded
      case 'invoice.payment_succeeded':
        await this.handlePaymentSucceeded(io);
        break;
      // donation completed
      case 'checkout.session.completed':
        await this.handleCheckoutSessionCompleted();
        break;
      // subscription canceled
      case 'customer.subscription.deleted':
        await this.handleGoldSubscriptionCanceled(io);
        break;
      default:
      // noop
    }
  }

  private async handlePaymentSucceeded(io?: Server) {
    // make sure the invoice corresponds to our gold membership price
    const invoice = this.event.data.object as Stripe.Invoice;
    if (invoice.lines.data[0]?.price?.id !== STRIPE_GOLD_MEMBER_PRICE_ID) {
      return;
    }

    const stripeCustomerId = invoice.customer as string;

    const user = await User.findOne({
      attributes: ['id', 'roleId'],
      where: { stripeCustomerId },
    });

    // only need to promote if the user isnt already gold
    const goldRoleId = await getPaidGoldUserRoleId();
    if (user!.roleId === goldRoleId) {
      return;
    }

    await promoteUserToGold(user!.id);
    createEvent(user!.id, EventType.GOLD_EARNED, user!.id, io);
  }

  private async handleCheckoutSessionCompleted() {
    // verify this charge is for a donation
    const session = this.event.data.object as Stripe.Checkout.Session;
    if (session.metadata?.['type'] !== 'donation') {
      return;
    }

    const amount = session.amount_total;

    // return immediately if donation amount is less than the gold price (minimum amount to apply upgrade)
    if (!amount || amount < GOLD_PRICE_PENCE) {
      return;
    }

    const stripeCustomerId = session.customer as string;

    const user = await User.findOne({
      attributes: ['id', 'donationUpgradeExpiresAt'],
      where: { stripeCustomerId },
    });

    const { donationUpgradeExpiresAt } = user!;

    // give user floor(amount / gold price) months of donation benefits i.e.
    // add that amount of months to their donation_upgrade_expires_at, or if that date is in the past / null,
    // set the column to NOW() + x months

    // amount of months to add to donation perks
    const donationPerkMonths = Math.floor(amount / GOLD_PRICE_PENCE);

    // new expiration is max(now, expiration) + months
    const now = new Date();
    const newDonationUpgradeExpiresAt =
      new Date(donationUpgradeExpiresAt!) < now ? now : new Date(donationUpgradeExpiresAt!);

    newDonationUpgradeExpiresAt.setMonth(
      newDonationUpgradeExpiresAt.getMonth() + donationPerkMonths
    );

    // promote to gold, update donation_upgrade_expires_at, and redirect to the supplied success URL
    await promoteUserToGold(user!.id);

    await user!.update({ donationUpgradeExpiresAt: newDonationUpgradeExpiresAt });
    await user!.save();
    await new UserRetriever(user!.id).invalidate();
  }

  private async handleGoldSubscriptionCanceled(io?: Server) {
    const subscription = this.event.data.object as Stripe.Subscription;

    const stripeCustomerId = subscription.customer as string;

    const user = await User.findOne({
      attributes: ['id', 'donationUpgradeExpiresAt'],
      where: { stripeCustomerId },
    });

    const donationUpgradeExpired = new Date(user!.donationUpgradeExpiresAt!) < new Date();
    if (donationUpgradeExpired) {
      await demoteUserFromGold(user!.id);
      createEvent(user!.id, EventType.GOLD_LOST, user!.id, io);
    }
  }
}
