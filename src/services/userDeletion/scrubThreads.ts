import Thread from '../../models/thread';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Scrubbing Threads...', userId);
    await Thread.update(
      {
        background_url: null,
        background_type: null,
      },
      { where: { user_id: userId } }
    );
  } catch (e) {
    logError('Threads', userId, e.message);
  }
};
