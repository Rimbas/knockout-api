import UserProfile from '../../models/userProfile';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Scrubbing UserProfile...', userId);
    await UserProfile.update(
      {
        heading_text: null,
        personal_site: null,
        background_url: null,
        background_type: null,
        steam: null,
        discord: null,
        github: null,
        youtube: null,
        twitter: null,
        fediverse: null,
        twitch: null,
        gitlab: null,
        tumblr: null,
        bluesky: null,
        header: null,
      },
      { where: { user_id: userId } }
    );
  } catch (e) {
    logError('UserProfiles', userId, e.message);
  }
};
