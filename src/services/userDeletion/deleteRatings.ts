import Rating from '../../models/rating';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Deleting Ratings...', userId);
    await Rating.destroy({ where: { user_id: userId } });
  } catch (e) {
    logError('Ratings', userId, e.message, 'delete');
  }
};
