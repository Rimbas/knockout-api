import { jobError, jobInfo } from '../../log';

export const logInfo = (msg: string, userId: number) => {
  jobInfo(`[user-deletion] [User:${userId}] ${msg}`);
};

export const logError = (
  resource: string,
  userId: number,
  message: string,
  action: string = 'scrub'
) => {
  jobError(`[user-deletion] [User:${userId} Could not ${action} ${resource}: ${message}`);
};
