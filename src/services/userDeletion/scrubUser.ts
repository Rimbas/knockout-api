import { DELETED_USER_NAME } from '../../constants/deletedUser';
import { getBannedUserRoleId } from '../../helpers/role';
import User from '../../models/user';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Scrubbing and banning user...', userId);
    const bannedUserRoleId = await getBannedUserRoleId();
    await User.update(
      {
        username: DELETED_USER_NAME,
        avatar_url: '',
        background_url: '',
        title: null,
        pronouns: null,
        roleId: bannedUserRoleId,
      },
      { where: { id: userId } }
    );
  } catch (e) {
    logError('Users', userId, e.message);
  }
};
