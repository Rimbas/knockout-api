import 'express';

// Global (non-imported) types
declare global {
  // Custom global variables
  namespace NodeJS {
    interface Global {
      __basedir: string;
    }
  }
}

declare module 'express' {
  // Extend the Express request object
  interface Request {
    ipInfo?: string;
    isLoggedIn?: boolean;
    user?: User;
    rawBody?: string;
  }

  /**
   * Authenticated forum user
   */
  interface User {
    id: number;
    username?: string;
    role_id: number;
    avatar_url?: string;
    background_url?: string;
    isBanned?: boolean;
    title?: string;
    createdAt: Date;
  }
}
