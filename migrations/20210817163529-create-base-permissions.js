'use strict';

import { aggregatePermissions, subforumPermissions } from '../src/helpers/permissions';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // do not run if we already have permissions in the DB
    const permissionCount = await queryInterface.sequelize.query(
      'SELECT COUNT(*) FROM Permissions',
      { type: Sequelize.QueryTypes.SELECT }
    ).then((result) => result[0].count);

    if (permissionCount > 0) {
      return true;
    }

    const subforumIds = await queryInterface.sequelize.query(
      `SELECT id FROM Subforums`,
      { type: Sequelize.QueryTypes.SELECT }
    ).then((records) => records.map((record) => record.id));

    const permissions = aggregatePermissions().concat(
      subforumPermissions(subforumIds)
    );

    return queryInterface.bulkInsert('Permissions', permissions);
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Permissions', {});
  }
};
