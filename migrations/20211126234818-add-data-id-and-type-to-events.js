'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Events', 'data_id', {
      type: Sequelize.BIGINT,
      allowNull: true,
    });
    await queryInterface.addColumn('Events', 'type', {
      type: Sequelize.STRING,
      allowNull: true,
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Events', 'data_id');
    await queryInterface.removeColumn('Events', 'type');
  }
};

