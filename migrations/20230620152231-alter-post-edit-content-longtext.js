'use strict';

export async function up(queryInterface, Sequelize) {
  return queryInterface.changeColumn('PostEdits', 'content', {
    type: Sequelize.TEXT('long'),
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.changeColumn('PostEdits', 'content', {
    type: Sequelize.TEXT,
  });
}
