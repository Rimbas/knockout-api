'use strict';

export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('UserArchivedConversations', {
    user_id: {
      allowNull: false,
      type: Sequelize.INTEGER,
      references: {
        model: "Users",
        key: "id"
      }
    },
    conversation_id: {
      allowNull: false,
      type: Sequelize.BIGINT,
      references: {
        model: "Conversations",
        key: "id"
      }
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.fn('NOW')
    }
  }).then(() => {
    return queryInterface.sequelize.query(
      'ALTER TABLE `UserArchivedConversations` ADD CONSTRAINT `userarchivedconversations_pk` PRIMARY KEY (`user_id`, `conversation_id`)'
    )
  });
}

export function down(queryInterface, Sequelize) {
  return queryInterface.dropTable('UserArchivedConversations');
}
