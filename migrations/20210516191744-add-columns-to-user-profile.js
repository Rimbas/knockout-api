'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('UserProfiles', 'steam', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'discord', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'github', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'youtube', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'twitter', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'twitch', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'gitlab', {
      allowNull: true,
      type: Sequelize.STRING
    });
    await queryInterface.addColumn('UserProfiles', 'tumblr', {
      allowNull: true,
      type: Sequelize.STRING
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('UserProfiles', 'steam');
    await queryInterface.removeColumn('UserProfiles', 'discord');
    await queryInterface.removeColumn('UserProfiles', 'github');
    await queryInterface.removeColumn('UserProfiles', 'youtube');
    await queryInterface.removeColumn('UserProfiles', 'twitter');
    await queryInterface.removeColumn('UserProfiles', 'twitch');
    await queryInterface.removeColumn('UserProfiles', 'gitlab');
    await queryInterface.removeColumn('UserProfiles', 'tumblr');
  }
};