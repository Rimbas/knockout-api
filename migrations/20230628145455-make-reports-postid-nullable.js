'use strict';

export async function up(queryInterface, Sequelize) {
  return queryInterface.changeColumn('Reports', 'post_id', {
    allowNull: true,
    type: Sequelize.INTEGER.UNSIGNED,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.changeColumn('Reports', 'post_id', {
    allowNull: false,
    type: Sequelize.INTEGER.UNSIGNED,
  });
}
