'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Bans', 
    {
      fields: [
        {
          attribute: 'user_id'
        },
        {
          attribute: 'expires_at',
          order: 'DESC'
        },
      ]
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeIndex(
      'Bans',
      'bans_user_id_expires_at'
    );
  }
};
