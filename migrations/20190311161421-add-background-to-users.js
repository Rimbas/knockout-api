// @ts-check
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Users', 'background_url', {
      allowNull: false,
      type: Sequelize.STRING,
      defaultValue: ''
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Users', 'background_url');
  }
};
