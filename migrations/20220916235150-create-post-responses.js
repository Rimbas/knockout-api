export async function up(queryInterface, Sequelize) {
  return queryInterface
    .createTable('PostResponses', {
      original_post_id: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'Posts',
          key: 'id',
        },
      },
      response_post_id: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'Posts',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW'),
      },
    })
    .then(() =>
      queryInterface.sequelize.query(
        'ALTER TABLE `PostResponses` ADD CONSTRAINT `postresponses_pk` PRIMARY KEY (`original_post_id`, `response_post_id`)'
      )
    );
}

export async function down(queryInterface) {
  return queryInterface.dropTable('PostResponses');
}
