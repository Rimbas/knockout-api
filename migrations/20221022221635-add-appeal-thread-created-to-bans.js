'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Bans', 'appeal_thread_created', {
    allowNull: true,
    type: Sequelize.BOOLEAN,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Bans', 'appeal_thread_created');
}
