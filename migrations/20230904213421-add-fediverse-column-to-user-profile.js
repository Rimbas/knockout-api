'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('UserProfiles', 'fediverse', {
      allowNull: true,
      type: Sequelize.STRING,
    });
  },
  down: async (queryInterface) => {
    await queryInterface.removeColumn('UserProfiles', 'fediverse');
  },
};
