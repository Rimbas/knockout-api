'use strict';

import knex from '../src/services/knex';
import {
  addAndAssociateSubforumPermissions,
} from '../src/helpers/permissions';

// Add the DIY and Hobbies subforum and the related permissions
export async function up(queryInterface, Sequelize) {
  // insert subforum
  const subforumId = await knex('Subforums').insert({
    name: 'DIY and Hobbies',
    icon_id: 1,
    icon: 'https://img.icons8.com/color/96/potters-wheel.png',
    nsfw: false,
    description: "Check out my handmade gasmask collection!"
  }).then((subforums) => subforums[0]);
  
  // this is a regular subforum that can use the general method
  await addAndAssociateSubforumPermissions(subforumId);
}

export async function down(queryInterface, Sequelize) {
  // noop
}
