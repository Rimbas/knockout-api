'use strict';

export async function up(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'usergroup');
}

export async function down(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'usergroup', {
    allowNull: true,
    type: Sequelize.INTEGER,
  });
}

