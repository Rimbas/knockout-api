'use strict';

export async function up(queryInterface, Sequelize) {
  // Change column 'report_reason' on 'Reports' table 
  // from STRING to TEXT.
  await queryInterface.changeColumn('Reports', 'report_reason', {
    allowNull: false,
    type: Sequelize.TEXT,
  });
}

export async function down(queryInterface, Sequelize) {
  // This migration is not reversible. If you want to revert it,
  // you will need to manually truncate any report_reasons that
  // are longer than 255 characters, and then change the column
  // back to STRING.
}
