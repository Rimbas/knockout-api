'use strict';

import knex from '../src/services/knex';
import { RoleCode } from '../src/helpers/permissions';

// Let the Basic User user role create calendar events
export async function up() {
  const createPermissionId = (await knex('Permissions').where({ code: 'calendar-event-create' }))[0]
    .id;

  const basicRoleId = await knex('Roles')
    .select('id')
    .where('code', RoleCode.BASIC_USER)
    .then((roles) => roles[0].id);

  await knex('RolePermissions').insert({ role_id: basicRoleId, permission_id: createPermissionId });
}

export async function down() {
  // noop
}
