'use strict';

export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('SiteStats', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT
    },
    from_date: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    to_date: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    total_active_users: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    total_bans: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    total_new_users: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    total_posts: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    total_reports: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    total_threads: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.fn('NOW')
    },
  });
}
export function down(queryInterface, Sequelize) {
  return queryInterface.dropTable('SiteStats');
}
