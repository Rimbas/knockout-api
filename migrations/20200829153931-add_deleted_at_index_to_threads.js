// @ts-check
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Threads', ['deleted_at'])
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Threads', 'deleted_at');
  }
};
