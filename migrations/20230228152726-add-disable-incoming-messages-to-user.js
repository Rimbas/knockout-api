export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'disable_incoming_messages', {
    allowNull: true,
    type: Sequelize.BOOLEAN,
  });
}

export async function down(queryInterface) {
  return queryInterface.removeColumn('Users', 'disable_incoming_messages');
}
