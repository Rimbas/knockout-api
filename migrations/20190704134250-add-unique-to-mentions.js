'use strict';

import knex from '../src/services/knex';

export async function up(queryInterface, Sequelize) {
  await knex.schema.alterTable('Mentions', function (table) {
    table.unique(['mentioned_by', 'post_id', 'mentions_user']);
  });
}
export async function down(queryInterface, Sequelize) {
  await knex.schema.alterTable('Mentions', function (table) {
    table.unique(['mentioned_by', 'post_id', 'mentions_user']);
  });
}
