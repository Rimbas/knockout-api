'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'show_online_status', {
    allowNull: true,
    type: Sequelize.BOOLEAN,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'show_online_status');
}
