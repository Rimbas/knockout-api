'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Reports', 'user_id', {
    allowNull: true,
    type: Sequelize.INTEGER,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Reports', 'user_id');
}
