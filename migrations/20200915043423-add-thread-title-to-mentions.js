'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    return queryInterface.addColumn('Mentions', 'thread_title', {
      allowNull: true,
      type: Sequelize.STRING
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Mentions', 'thread_title');
  }
};
