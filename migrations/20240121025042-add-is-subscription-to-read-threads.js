'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('ReadThreads', 'is_subscription', {
      allowNull: true,
      type: Sequelize.BOOLEAN,
    });
  },
  down: async (queryInterface) => {
    await queryInterface.removeColumn('ReadThreads', 'is_subscription');
  },
};
