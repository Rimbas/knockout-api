'use strict';

import knex from '../src/services/knex';

export async function up(queryInterface, Sequelize) {
  await knex.raw('ALTER TABLE Posts MODIFY COLUMN content LONGTEXT DEFAULT NULL NULL');
}
export async function down(queryInterface, Sequelize) {
  await knex.raw('ALTER TABLE Posts MODIFY COLUMN content JSON DEFAULT NULL NULL');
}
