'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('LogEvents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      entity: {
        allowNull: true,
        type: Sequelize.STRING
      },
      action: {
        allowNull: true,
        type: Sequelize.STRING
      },
      success: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      message: {
        allowNull: true,
        type: Sequelize.STRING
      },
      properties: {
        allowNull: true,
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('LogEvents');
  }
};
