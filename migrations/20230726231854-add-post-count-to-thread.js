'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Threads', 'post_count', {
    allowNull: true,
    type: Sequelize.INTEGER,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Threads', 'post_count');
}
