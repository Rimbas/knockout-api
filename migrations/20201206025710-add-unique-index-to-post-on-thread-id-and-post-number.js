'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Posts', ['thread_id', 'thread_post_number'], { unique: true, concurrently: true })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Posts', ['thread_id', 'thread_post_number']);
  }
};
