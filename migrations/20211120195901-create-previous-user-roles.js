'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PreviousUserRoles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Users",
        key: "id"
        }
      },
      role_id: {
        allowNull: false,
        type: Sequelize.BIGINT,
        references: {
          model: "Roles",
          key: "id"
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PreviousUserRoles');
  }
};
