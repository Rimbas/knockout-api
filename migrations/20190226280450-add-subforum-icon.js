// @ts-check
'use strict';

/**
 * @param {import('sequelize').QueryInterface} queryInterface
 * @param {string} subforumName
 */
function setDescription(queryInterface, subforumName, icon) {
  return queryInterface.sequelize.query(
    `update Subforums set icon = :icon where name = :subforumName`,
    { replacements: { subforumName, icon } }
  );
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Subforums', 'icon', {
      type: Sequelize.STRING
    });
    // Fill column
    await Promise.all([
      setDescription(
        queryInterface,
        'General Discussion',
        `https://img.icons8.com/color/96/000000/talk-female.png`
      ),
      setDescription(
        queryInterface,
        'Game Generals',
        `https://img.icons8.com/color/96/000000/nintendo-switch.png`
      ),
      setDescription(
        queryInterface,
        'Gaming',
        `https://img.icons8.com/color/96/000000/controller.png`
      ),
      setDescription(
        queryInterface,
        'Videos',
        `https://img.icons8.com/color/96/000000/documentary.png`
      ),
      setDescription(
        queryInterface,
        'Politics',
        `https://img.icons8.com/color/96/000000/reichstag.png`
      ),
      setDescription(
        queryInterface,
        'News',
        `https://img.icons8.com/color/96/000000/news.png`
      ),
      setDescription(
        queryInterface,
        'Film, Television and Music',
        `https://img.icons8.com/color/96/000000/film-reel.png`
      ),
      setDescription(
        queryInterface,
        'Hardware & Software',
        `https://img.icons8.com/color/96/000000/video-card.png`
      ),
      setDescription(
        queryInterface,
        'Meta',
        `https://img.icons8.com/color/96/000000/steak.png`
      )
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Subforums', 'icon');
  }
};
