'use strict';

import ThreadRetriever from '../src/retriever/thread';
import PostRetriever from '../src/retriever/post';

// Decrease postCount on all threads whose postCount is equal to the actual count
// of posts in the thread, plus 1.
// This is to account for the postCount incrementing on thread creation
// and the postCount incrementing on post creation. This issue is now resolved with
// the same release that this migration is introduced in, but we still need to
// fix the postCount on threads created after the release.
export async function up(queryInterface, Sequelize) {
  // Get all threads with post_count equal to the last posts thread_post_number plus 1
  // We can also limit the scope to threads created after 11/20/2023.
  const query = `
    SELECT
      *
    FROM
      Threads
    WHERE
      post_count = (
        SELECT
          COUNT(p.id)
        FROM
          Posts p
        WHERE
          p.thread_id = Threads.id
      ) + 1
    AND Threads.created_at > '2023-11-20 00:00:00'
  `
  const threads = await queryInterface.sequelize.query(query, { type: queryInterface.sequelize.QueryTypes.SELECT });

  if (!threads.length) {
    return;
  }

  const threadIds = threads.map(t => t.id);

  // Decreate all postCounts by 1
  const updateQuery = `
    UPDATE
      Threads
    SET
      post_count = post_count - 1
    WHERE
      id IN (?)
  `;
  await queryInterface.sequelize.query(updateQuery, { replacements: threadIds });

  // Decrease all thread_post_numbers by 1
  const updatePostQuery = `
    UPDATE
      Posts
    SET
      thread_post_number = thread_post_number - 1
    WHERE
      thread_id IN (?)
  `;
  await queryInterface.sequelize.query(updatePostQuery, { replacements: threadIds });

  // Invalidate all cache keys for the affected threads
  await new ThreadRetriever(threadIds).invalidate();

  // Invalidate all cache keys for the affected posts
  const postIds = await queryInterface.sequelize.query(`
    SELECT
      id
    FROM
      Posts
    WHERE
      thread_id IN (?)
  `, { replacements: threadIds, type: queryInterface.sequelize.QueryTypes.SELECT });

  await new PostRetriever(postIds).invalidate();
}

export async function down(queryInterface, Sequelize) {
  // noop
}
