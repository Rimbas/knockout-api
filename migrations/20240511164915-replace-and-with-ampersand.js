'use strict';

export async function up(queryInterface, Sequelize) {
  // Update subforum 7 name from to "Film, Television & Music"
  // Subforum 10 name to "Creativity & Art"
  // Subforum 19 name to "DIY & Hobbies"
  await queryInterface.sequelize.query(
    "UPDATE Subforums SET name = 'Film, Television & Music' WHERE id = 7;"
  );
  await queryInterface.sequelize.query(
    "UPDATE Subforums SET name = 'Creativity & Art' WHERE id = 10;"
  );
  await queryInterface.sequelize.query(
    "UPDATE Subforums SET name = 'DIY & Hobbies' WHERE id = 19;"
  );
}

export function down(queryInterface, Sequelize) {
  // noop
}
