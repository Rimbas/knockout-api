'use strict';

export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('HiddenThreads', {
    thread_id: {
      allowNull: false,
      type: Sequelize.INTEGER.UNSIGNED,
      references: {
        model: 'Threads',
        key: 'id',
      },
    },
    user_id: {
      allowNull: false,
      type: Sequelize.INTEGER,
      references: {
        model: "Users",
        key: "id"
      }
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.fn('NOW')
    },
  }).then(() => {
    return queryInterface.sequelize.query(
      'ALTER TABLE `HiddenThreads` ADD CONSTRAINT `hidden_threads_pk` PRIMARY KEY (`thread_id`, `user_id`)'
    )
    });
}
export function down(queryInterface, Sequelize) {
  return queryInterface.dropTable('HiddenThreads');
}
