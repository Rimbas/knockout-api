'use strict';

// Usergroups
// 0: Banned User
// 1: Regular User
// 2: Gold Member
// 3: Moderator
// 4: Admin
// 5: Staff
// 6: Moderator In Training

const USERGROUP_TO_ROLE_CODE_MAPPINGS = [
  'banned-user',
  'basic-user',
  'gold-user',
  'moderator',
  'admin',
  'admin',
  'moderator-in-training'
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    USERGROUP_TO_ROLE_CODE_MAPPINGS.map(async (code, usergroup) => {
      const roleId = await queryInterface.sequelize.query(
        `SELECT id FROM Roles WHERE code = :code LIMIT 1`,
        {
          type: Sequelize.QueryTypes.SELECT,
          replacements: { code: code }
        }
      ).then(roles => roles[0].id);

      await queryInterface.sequelize.query(
        'UPDATE Users SET role_id = :id WHERE usergroup = :usergroup AND role_id IS NULL',
        {
          type: Sequelize.QueryTypes.UPDATE,
          replacements: { id: roleId, usergroup }
        }
      );
    })

    // assigned limited-role roles to limited users
    const limitedRoleId = await queryInterface.sequelize.query(
      'SELECT id FROM Roles WHERE code = "limited-user" LIMIT 1', 
      {
        type: Sequelize.QueryTypes.SELECT
      }
    ).then(roles => roles[0].id);

    // all users who:
    //   1) have a post count less than 15 posts AND were created after 2020-07-01
    //   2) joined within the last week
    // are limited users
    const postCountQuery = `
      SELECT u.id as user_id
      FROM Users u
      LEFT JOIN Posts p ON p.user_id = u.id
      WHERE u.created_at > CAST('2020-07-01' AS DATE) AND u.role_id IS NULL
      GROUP BY u.id
      HAVING COUNT(p.id) < 15
    `;
  
    const conditionOneUserIds = await queryInterface.sequelize.query(postCountQuery,
      {
        type: Sequelize.QueryTypes.SELECT
      }
    ).then(users => users.map((user => user.user_id)));

    const conditionTwoUserIds = await queryInterface.sequelize.query(
      'SELECT id FROM Users WHERE created_at >= (CURDATE() - INTERVAL 7 DAY)',
      {
        type: Sequelize.QueryTypes.SELECT
      }
    ).then(users => users.map((user => user.id)));

    const limitedUserIds = conditionOneUserIds.concat(conditionTwoUserIds);

    if (limitedUserIds.length > 0) {
      return queryInterface.sequelize.query('UPDATE Users SET role_id = :id WHERE id IN (:userIds)',
        {
          type: Sequelize.QueryTypes.UPDATE,
          replacements: { id: limitedRoleId, userIds: limitedUserIds }
        }
      )
    }
    return Promise.resolve(true);
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `UPDATE Users SET role_id = NULL`,
      {
        type: Sequelize.QueryTypes.UPDATE
      }
    )
  }
};
